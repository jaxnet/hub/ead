module gitlab.com/jaxnet/hub/ead

go 1.14

require (
	github.com/davecgh/go-spew v1.1.1
	github.com/dgraph-io/ristretto v0.1.0 // indirect
	github.com/fasthttp/router v1.3.10
	github.com/go-pg/pg/v10 v10.9.0 // indirect
	github.com/gocarina/gocsv v0.0.0-20210326111627-0340a0229e98 // indirect
	github.com/golang/glog v0.0.0-20210429001901-424d2337a529 // indirect
	github.com/golang/protobuf v1.5.2 // indirect
	github.com/google/uuid v1.1.1
	github.com/jackc/pgx/v4 v4.10.1
	github.com/jessevdk/go-flags v1.5.0 // indirect
	github.com/json-iterator/go v1.1.11
	github.com/klauspost/cpuid/v2 v2.0.6 // indirect
	github.com/minio/sha256-simd v1.0.0 // indirect
	github.com/prometheus/common v0.28.0 // indirect
	github.com/prometheus/procfs v0.6.0 // indirect
	github.com/rs/zerolog v1.22.0
	github.com/valyala/fasthttp v1.22.0
	github.com/vmihailenco/msgpack/v5 v5.3.1 // indirect
	gitlab.com/jaxnet/core/indexer v0.0.0-20210518041130-22dce9d416f9
	gitlab.com/jaxnet/core/shard.core v1.6.2-0.20210507145058-b27feef6ab2d
	golang.org/x/crypto v0.0.0-20210513164829-c07d793c2f9a
	golang.org/x/xerrors v0.0.0-20200804184101-5ec99f83aff1 // indirect
	gopkg.in/check.v1 v1.0.0-20200227125254-8fa46927fb4f // indirect
	gopkg.in/natefinch/lumberjack.v2 v2.0.0
	gopkg.in/yaml.v2 v2.4.0
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b // indirect
)

replace gitlab.com/jaxnet/core/indexer => ./dep/indexer
