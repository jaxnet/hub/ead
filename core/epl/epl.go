package epl

import (
	"gitlab.com/jaxnet/core/shard.core/btcec"
	"gitlab.com/jaxnet/hub/ead/core/blockchain/shards"
	"gitlab.com/jaxnet/hub/ead/core/crypto"
	"gitlab.com/jaxnet/hub/ead/core/marshalling"
	"math/big"
	"time"
)

type ExchangeAgentProposal struct {
	ExchangeAgentID uint64
	ReservedAmount  *big.Int
	Expire          time.Time
	SequenceAnchor  uint32
	FeeFixedAmount  *big.Int
	FeePercents     float64

	Index     shards.Pair
	Signature *btcec.Signature
}

// todo: tests needed
func (proposal *ExchangeAgentProposal) FinalFee(dealAmount *big.Int) (fee *big.Int) {
	deal := &big.Float{}
	deal.SetInt(dealAmount)

	feePercents := &big.Float{}
	feePercents.SetFloat64(proposal.FeePercents)

	percent := deal.Quo(deal, (&big.Float{}).SetInt64(100))
	finalFee := percent.Mul(percent, feePercents)
	finalFee.Int(fee)
	return
}

func (proposal *ExchangeAgentProposal) Sign() (err error) {
	data, err := proposal.MarshalAllExceptSignature()
	if err != nil {
		return
	}

	signature, err := crypto.Sign(data[:])
	if err != nil {
		return
	}

	proposal.Signature = signature
	return
}

func (proposal *ExchangeAgentProposal) Verify(key *btcec.PublicKey) (ok bool, err error) {
	data, err := proposal.MarshalAllExceptSignature()
	if err != nil {
		return
	}

	return crypto.VerifyExternalPubKey(data, proposal.Signature, key), nil
}

func (proposal *ExchangeAgentProposal) MarshalBinary() (data []byte, err error) {
	encoder := marshalling.NewEncoder()

	binaryData, err := proposal.MarshalAllExceptSignature()
	if err != nil {
		return
	}

	err = encoder.PutFixedSizeDataSegment(binaryData)
	if err != nil {
		return
	}

	sigBinary := proposal.Signature.Serialize()
	err = encoder.PutVariadicDataWith2BytesHeader(sigBinary)
	if err != nil {
		return
	}

	data = encoder.CollectDataAndReleaseBuffers()
	return
}

func (proposal *ExchangeAgentProposal) UnmarshalBinary(data []byte) (err error) {
	decoder := marshalling.NewDecoder(data)

	proposal.ExchangeAgentID, err = decoder.GetUint64()
	if err != nil {
		return
	}

	proposal.ReservedAmount, err = decoder.GetBigIntWithByteHeader()
	if err != nil {
		return
	}

	proposal.Expire, err = decoder.GetTimeWithUint8Header()
	if err != nil {
		return
	}

	proposal.SequenceAnchor, err = decoder.GetUint32()
	if err != nil {
		return
	}

	proposal.FeeFixedAmount, err = decoder.GetBigIntWithByteHeader()
	if err != nil {
		return
	}

	feePercents, err := decoder.GetUint16()
	if err != nil {
		return
	}
	proposal.FeePercents = float64(feePercents / 100)

	indexBinary, err := decoder.GetDataSegment(8)
	if err != nil {
		return
	}

	err = proposal.Index.UnmarshalBinary(indexBinary)
	if err != nil {
		return
	}

	sigBinary, err := decoder.GetDataSegmentWith2BytesHeader()
	if err != nil {
		return
	}

	proposal.Signature, err = btcec.ParseSignature(sigBinary, btcec.S256())
	if err != nil {
		return
	}

	return
}

func (proposal *ExchangeAgentProposal) MarshalAllExceptSignature() (data []byte, err error) {
	encoder := marshalling.NewEncoder()

	err = encoder.PutUint64(proposal.ExchangeAgentID)
	if err != nil {
		return
	}

	err = encoder.PutBigIntWithByteHeader(proposal.ReservedAmount)
	if err != nil {
		return
	}

	err = encoder.PutTimeWithByte8Header(proposal.Expire)
	if err != nil {
		return
	}

	err = encoder.PutUint32(proposal.SequenceAnchor)
	if err != nil {
		return
	}

	err = encoder.PutBigIntWithByteHeader(proposal.FeeFixedAmount)
	if err != nil {
		return
	}

	err = encoder.PutUint16(uint16(proposal.FeePercents * 100))
	if err != nil {
		return
	}

	indexBinary, err := proposal.Index.MarshalBinary()
	if err != nil {
		return
	}

	err = encoder.PutFixedSizeDataSegment(indexBinary)
	if err != nil {
		return
	}

	data = encoder.CollectDataAndReleaseBuffers()
	return
}

// todo: sort the proposals for the efficient lookup
type ExchangeAgentsProposalsList struct {
	// Exchange Agent HashID -> proposal
	Proposals map[uint64]*ExchangeAgentProposal
}

func NewExchangeAgentsProposalsList() (list *ExchangeAgentsProposalsList) {
	list = &ExchangeAgentsProposalsList{
		Proposals: make(map[uint64]*ExchangeAgentProposal),
	}

	return
}

type ExchangeAgentProposalGetRequest struct {
	ExchangeAgentID uint64
	Signature       *btcec.Signature
}

func (proposalGetRequest *ExchangeAgentProposalGetRequest) Sign() (err error) {
	data, err := proposalGetRequest.MarshalAllExceptSignature()
	if err != nil {
		return
	}

	signature, err := crypto.Sign(data[:])
	if err != nil {
		return
	}

	proposalGetRequest.Signature = signature
	return
}

func (proposalGetRequest *ExchangeAgentProposalGetRequest) Verify(key *btcec.PublicKey) (ok bool, err error) {
	data, err := proposalGetRequest.MarshalAllExceptSignature()
	if err != nil {
		return
	}

	return crypto.VerifyExternalPubKey(data, proposalGetRequest.Signature, key), nil
}

func (proposalGetRequest *ExchangeAgentProposalGetRequest) MarshalBinary() (data []byte, err error) {
	encoder := marshalling.NewEncoder()

	binaryData, err := proposalGetRequest.MarshalAllExceptSignature()
	if err != nil {
		return
	}

	err = encoder.PutFixedSizeDataSegment(binaryData)
	if err != nil {
		return
	}

	sigBinary := proposalGetRequest.Signature.Serialize()
	err = encoder.PutVariadicDataWith2BytesHeader(sigBinary)
	if err != nil {
		return
	}

	data = encoder.CollectDataAndReleaseBuffers()
	return
}

func (proposalGetRequest *ExchangeAgentProposalGetRequest) UnmarshalBinary(data []byte) (err error) {
	decoder := marshalling.NewDecoder(data)

	proposalGetRequest.ExchangeAgentID, err = decoder.GetUint64()
	if err != nil {
		return
	}

	sigBinary, err := decoder.GetDataSegmentWith2BytesHeader()
	if err != nil {
		return
	}

	proposalGetRequest.Signature, err = btcec.ParseSignature(sigBinary, btcec.S256())
	if err != nil {
		return
	}

	return
}

func (proposalGetRequest *ExchangeAgentProposalGetRequest) MarshalAllExceptSignature() (data []byte, err error) {
	encoder := marshalling.NewEncoder()

	err = encoder.PutUint64(proposalGetRequest.ExchangeAgentID)
	if err != nil {
		return
	}

	data = encoder.CollectDataAndReleaseBuffers()
	return
}

type OwnExchangeAgentProposals struct {
	Proposals    []*ExchangeAgentProposal
	RequesterEAD uint64
}

func (response *OwnExchangeAgentProposals) MarshalBinary() (data []byte, err error) {
	encoder := marshalling.NewEncoder()

	err = encoder.PutUint32(uint32(len(response.Proposals)))
	if err != nil {
		return
	}

	for _, proposal := range response.Proposals {
		proposalBinary, err := proposal.MarshalBinary()
		if err != nil {
			return nil, err
		}

		err = encoder.PutVariadicDataWithByteHeader(proposalBinary)
		if err != nil {
			return nil, err
		}
	}

	data = encoder.CollectDataAndReleaseBuffers()
	return
}

func (response *OwnExchangeAgentProposals) UnmarshalBinary(data []byte) (err error) {
	decoder := marshalling.NewDecoder(data)

	totalRecordsSize, err := decoder.GetUint32()
	if err != nil {
		return
	}

	response.Proposals = make([]*ExchangeAgentProposal, totalRecordsSize)
	for i, _ := range response.Proposals {
		proposalBinary, err := decoder.GetDataSegmentWithByteHeader()
		if err != nil {
			return err
		}

		proposal := &ExchangeAgentProposal{}
		err = proposal.UnmarshalBinary(proposalBinary)
		if err != nil {
			return err
		}

		response.Proposals[i] = proposal
	}

	return
}
