package ec

import (
	"math"
	"math/big"
	"time"
)

var (
	i       = &big.Int{}
	OneCoin = i.SetInt64(int64(math.Pow(10, 8)))
)

const (
	// [security]
	// Minimal amount of blocks during which EAD is able to sign cross shard transactions (operational time window).
	//
	// See cross-shard transactions processing schema for more details:
	// https://miro.com/app/board/o9J_ktf5hng=/?moveToWidget=3074457351816994359&cot=12
	CrossShardOperationsBlocksRangeMinCount     = 6
	CrossShardOperationsBlocksRangeCountSoftCap = 200

	// No more than uint16 of blocks are acceptable due to the limitation of the transport protocol.
	CrossShardOperationsBlocksRangeCountHardCap = math.MaxUint16

	LockTxConfirmationsExpected       = 6
	CrossShardTxConfirmationsExpected = 6

	// Inter component communication
	InterComponentsFlowTTL = time.Second * 30
	ITTL                   = InterComponentsFlowTTL

	//
	PercentsForFeesFluctuation = 0
)
