package ec

import (
	"errors"
	"gitlab.com/jaxnet/hub/ead/core/logger"
	"os"
)

//
// Common errors.
//
var (
	ErrNilArgument      = errors.New("argument could not be nil")
	ErrValidationFailed = errors.New("validation error")
)

//
// Database-related errors.
//
var (
	ErrDBNoRecords               = errors.New("no records fetched")
	ErrDBMoreRecordsThanExpected = errors.New("more records fetched than expected")
	ErrDBSchema                  = errors.New("invalid DB schema")
)

//
// Proposals-related errors.
//
var (
	ErrInvalidShardID       = errors.New("invalid shard id specified")
	ErrInvalidReserve       = errors.New("invalid reserve specified")
	ErrInvalidFixedFee      = errors.New("fixed fee must be set as a non-empty string representation of a float number with maximal precision is 10**8")
	ErrInvalidPercentsFee   = errors.New("percents fee must be set as a non-empty string representation of a float number with maximal possible value = 100")
	ErrInvalidExpireTTL     = errors.New("expire TTL must be set a ISO-8086 date/time format, with maximal 3 days shift from current moment")
	ErrProposalIsAbsent     = errors.New("proposal is absent")
	ErrNoReserveIsAvailable = errors.New("no funds reserve is available")
	ErrInvalidAmount        = errors.New("invalid amount specified")
	ErrInvalidExchangeType  = errors.New("invalid exchange type specified")
)

//
// Transactions processing related errors.
//
var (
	ErrShardNotFound     = errors.New("specified shard has been not found. EA seems to not being registered on it")
	TransactionNotFound  = errors.New("transaction not found")
	ErrShardLimitReached = errors.New("shard's upper bound reached by the non-finished transactions")
)

func InterruptOnError(err error) {
	if err != nil {
		if logger.Log != nil {
			logger.Log.Err(err).Msg("")
		} else {
			println(err.Error())
		}

		os.Exit(-1)
	}
}

func InterruptOnNilArgument(arg interface{}) {
	if arg == nil {
		panic(arg)
	}
}

func ReportErrorInChannelIfAny(err error, ch chan error) {
	if err != nil {
		ch <- err
	}
}
