package xsync

type Flag struct {
	c chan bool
}

func NewFlag() (flag *Flag) {
	flag = &Flag{

		// This channel is used as flag, that could lock related goroutines to the moment when the flag's state changes.
		// Providing the buffer with one slot, makes this channel capable to store exactly one event at a time.
		// In case if channel is empty - no database rescans should be performed.
		// In case if there is at least one value - then the rescan has been requested,
		// and all other values that are going to be written into the channel must be abandoned,
		// because if there is a value in the channel, then the flag is already set up.
		c: make(chan bool, 1),
	}

	return
}

func (f *Flag) Up() {
	select {
	case f.c <- true:
	default:
		// No value should be added in case if upper bound is reached.
	}
}

func (f *Flag) WaitUntilUp() {
	<-f.c
}
