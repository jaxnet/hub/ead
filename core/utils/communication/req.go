package communication

type RequestContext struct {
	Context   interface{}
	Responses *ResponsesFlow
}

func NewInternalRequest(context interface{}) (r *RequestContext) {
	r = &RequestContext{
		Context:   context,
		Responses: NewResponsesFlow(),
	}
	return
}
