package communication

import "time"

type ResponsesFlow struct {
	responses chan interface{}
}

func NewResponsesFlow() (f *ResponsesFlow) {
	return &ResponsesFlow{
		responses: make(chan interface{}, 1),
	}
}

func (f *ResponsesFlow) Put(response interface{}, ttl time.Duration) (err error) {
	select {
	case f.responses <- response:
		return nil

	case <-time.After(ttl):
		return ErrResponsePassing
	}
}

func (f *ResponsesFlow) Fetch(ttl time.Duration) (response interface{}, err error) {
	select {
	case response = <-f.responses:
		return

	case <-time.After(ttl):
		err = ErrResponseFetching
		return
	}
}
