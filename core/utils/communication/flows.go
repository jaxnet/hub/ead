package communication

import (
	"errors"
	"fmt"
	"github.com/davecgh/go-spew/spew"
	"gitlab.com/jaxnet/hub/ead/core/logger"
	"time"
)

var (
	ErrRequestPassing        = errors.New("request can't be passed")
	ErrRequestFetching       = errors.New("request can't be fetched")
	ErrResponseFetching      = errors.New("response can't be fetched")
	ErrResponsePassing       = errors.New("response can't be passed")
	ErrUnexpectedRequestType = errors.New("request contains unexpected data structure")
	ErrBadRequest            = errors.New("bad request")
)

type Flow struct {
	requestsFlow chan *RequestContext
}

func NewUnbufferedFlow() (f *Flow) {
	return &Flow{
		requestsFlow: make(chan *RequestContext),
	}
}

func NewBufferedFlow(cap int) (f *Flow) {
	return &Flow{
		requestsFlow: make(chan *RequestContext, cap),
	}
}

// todo: comment
// Zero ttl means "report error instantly and do not wait"
func (f *Flow) Pass(req interface{}, ttl time.Duration) (responses *ResponsesFlow, err error) {
	r := NewInternalRequest(req)

	select {
	case f.requestsFlow <- r:
		return r.Responses, nil

	case <-time.After(ttl):
		return nil, ErrRequestPassing
	}
}

func (f *Flow) Fetch(ttl time.Duration) (request *RequestContext, err error) {
	select {
	case request = <-f.requestsFlow:
		return request, nil

	case <-time.After(ttl):
		return nil, ErrRequestFetching
	}
}

type EventsFlow struct {
	flow chan interface{}
}

func NewUnbufferedEventsFlow() (f *EventsFlow) {
	return &EventsFlow{
		flow: make(chan interface{}),
	}
}

func NewBufferedEventsFlow(cap int) (f *EventsFlow) {
	return &EventsFlow{
		flow: make(chan interface{}, cap),
	}
}

// todo: comment
// Zero ttl means "report error instantly and do not wait"
func (f *EventsFlow) Push(e interface{}, ttl time.Duration) {
	select {
	case f.flow <- e:
		return

	case <-time.After(ttl):
		eventDetails := spew.Sdump(e)
		err := fmt.Errorf("cant pass event (%w)", ErrRequestPassing)
		logger.Log.Err(err).Str("event", eventDetails).Msg("")
		return
	}
}

func (f *EventsFlow) Subscribe() <-chan interface{} {
	return f.flow
}
