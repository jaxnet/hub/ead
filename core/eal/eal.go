package eal

type ExchangeAgentsList struct {
	ExchangeAgents map[uint64]*ExchangeAgentInfo
}

func NewExchangeAgentsList() (list *ExchangeAgentsList) {
	list = &ExchangeAgentsList{
		ExchangeAgents: make(map[uint64]*ExchangeAgentInfo),
	}

	return
}
