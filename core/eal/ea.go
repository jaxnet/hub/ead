package eal

import (
	"gitlab.com/jaxnet/core/shard.core/btcec"
	"gitlab.com/jaxnet/hub/ead/core/blockchain/shards"
)

type ExchangeAgentInfo struct {
	ID uint64

	Addresses map[string][]shards.ID

	PubKey *btcec.PublicKey
}
