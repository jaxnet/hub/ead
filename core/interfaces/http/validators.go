package http

import (
	"fmt"
	"gitlab.com/jaxnet/core/shard.core/btcutil"
	"gitlab.com/jaxnet/hub/ead/core/blockchain/shards"
	"gitlab.com/jaxnet/hub/ead/core/ec"
	"gitlab.com/jaxnet/hub/ead/core/settings"
	"gitlab.com/jaxnet/hub/ead/core/utils"
	"math/big"
	"strconv"
	"strings"
	"time"
)

var (
	precision = (&big.Rat{}).SetInt64(100000000)
)

// todo: Tests needed
func validateAndNormalizeShardID(shardID string) (id shards.ID, err error) {
	var (
		invalidShardID = fmt.Errorf(
			"shard id must be a positive number greater or equal to 1: (%w)", ec.ErrInvalidShardID)

		noShardFound = fmt.Errorf(
			"no shard with specified ID found, check settings: (%w)", ec.ErrInvalidShardID)
	)

	i, err := strconv.ParseInt(shardID, 10, 32)
	if err != nil || i < 1 {
		err = invalidShardID
		return
	}

	id = shards.ID(i)
	_, shardIsPresent := settings.Conf.Blockchain.ShardsMap[id]
	if !shardIsPresent {
		err = noShardFound
		return
	}

	return
}

// todo: Tests needed
func validateAndNormalizeReservedAmount(reserve string) (normalized btcutil.Amount, err error) {
	var (
		invalidReserve = fmt.Errorf(
			"reserve must be a positive number in range (0.00000001 .. 21M]: (%w)", ec.ErrInvalidReserve)
	)

	reserve = strings.ReplaceAll(reserve, ",", ".")
	f := &big.Rat{}
	f, ok := f.SetString(reserve)
	if !ok {
		err = invalidReserve
		return
	}

	if f.Cmp((&big.Rat{}).SetFloat64(0.00000001)) < 0 {
		err = invalidReserve
		return
	}

	const maxReservePossible = 21000000
	if f.Cmp((&big.Rat{}).SetInt64(maxReservePossible)) >= 0 {
		err = invalidReserve
		return
	}

	f = f.Mul(f, precision)
	amount := f.Num().Int64()
	normalized = btcutil.Amount(amount)
	return
}

// todo: tests needed
//
// Uses the same logic as reserve validation.
func validateAndNormalizeFeeFixed(fee string) (normalized btcutil.Amount, err error) {
	var (
		invalidFee = fmt.Errorf(
			"fee must be a positive number in range (0.00000001 .. 21M]: (%w)", ec.ErrInvalidFixedFee)
	)

	fee = strings.ReplaceAll(fee, ",", ".")

	f := &big.Rat{}
	f, ok := f.SetString(fee)
	if !ok {
		err = invalidFee
		return
	}

	if f.Cmp((&big.Rat{}).SetFloat64(0.00000001)) < 0 {
		err = invalidFee
		return
	}

	if f.Cmp((&big.Rat{}).SetInt64(21000000)) > 0 {
		err = invalidFee
		return
	}

	f = f.Mul(f, precision)
	amount := f.Num().Int64()
	normalized = btcutil.Amount(amount)
	return
}

func validateAndNormalizeFeePercentage(fee string) (normalized float64, err error) {
	var (
		invalidFee = fmt.Errorf(
			"fee must be a number in range 0..100: (%w)", ec.ErrInvalidPercentsFee)
	)

	normalized, err = strconv.ParseFloat(fee, 64)
	if err != nil {
		err = invalidFee
		return
	}

	if normalized > 100 {
		err = invalidFee
		return
	}

	return
}

func validateAndNormalizeExpireTTL(expire string) (normalized time.Time, err error) {
	var (
		invalidTimestamp = fmt.Errorf(
			"expire ttl must be a UTC timestamp in ISO-8601 format "+
				"with no more than 3 days shift: (%w)", ec.ErrInvalidExpireTTL)
	)

	normalized, err = time.Parse(time.RFC3339, expire)
	if err != nil {
		err = invalidTimestamp
		return
	}

	if normalized.Before(utils.UTCNow()) {
		err = invalidTimestamp
		return
	}

	if normalized.Sub(utils.UTCNow()) > time.Hour*24*3 {
		err = invalidTimestamp
		return
	}

	return
}

func validateAmount(amount string) (normalized btcutil.Amount, err error) {
	var (
		invalidAmount = fmt.Errorf(
			"amount must be a positive number in range (1 .. 9B]: (%w)", ec.ErrInvalidAmount)
	)

	tmpAmount, err := strconv.ParseInt(amount, 10, 64)
	if err != nil {
		err = invalidAmount
		return
	}
	normalized = btcutil.Amount(tmpAmount)
	return
}

func validateExchangeType(exTypeStr string) (exType uint8, err error) {
	var (
		invalidExchangeType = fmt.Errorf(
			"exchange type must be a positive number in range [0 .. 1]: (%w)", ec.ErrInvalidExchangeType)
	)
	exTypeTmp, err := strconv.ParseUint(exTypeStr, 10, 8)
	if err != nil {
		err = invalidExchangeType
		return
	}
	if exTypeTmp != 0 && exTypeTmp != 1 {
		err = invalidExchangeType
		return
	}
	exType = uint8(exTypeTmp)
	return
}
