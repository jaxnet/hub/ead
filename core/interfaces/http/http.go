package http

import (
	"fmt"
	"gitlab.com/jaxnet/hub/ead/core/blockchain/shards"
	"gitlab.com/jaxnet/hub/ead/core/handlers/limits"
	"gitlab.com/jaxnet/hub/ead/core/logger"
	"gitlab.com/jaxnet/hub/ead/core/settings"
	"net/http"
	"strings"
)

func invalidParameterPlaceholder(parameter string) (placeholder string) {
	return fmt.Sprint("parameter `", parameter, "` is invalid: %s")
}

var (
	argReserve              = "reserve"
	argShardID              = "shard-id"
	argSourceShardID        = "source-shard-id"
	argDestinationShardID   = "destination-shard-id"
	argFeeFixed             = "fee-fixed"
	argFeePercents          = "fee-percents"
	argFeeExpireTTL         = "expire"
	argAmount               = "amount"
	argExchangeType         = "type"
	argExchangeUUID         = "uuid"
	argDestShardAddress     = "destinationShardAddress"
	argSenderPubKey         = "senderPubKey"
	argReceiverPubKey       = "receiverPubKey"
	argMultiSigRedeemScript = "multiSigRedeemScript"
	argTxHash               = "txHash"

	invalidBodyMsg                 = "invalid/corrupted request body"
	invalidReserveMsg              = invalidParameterPlaceholder(argReserve)
	invalidShardIDMsg              = invalidParameterPlaceholder(argShardID)
	invalidSourceShardIDMsg        = invalidParameterPlaceholder(argSourceShardID)
	invalidDestinationShardIDMsg   = invalidParameterPlaceholder(argDestinationShardID)
	invalidFeeFixedMsg             = invalidParameterPlaceholder(argFeeFixed)
	invalidFeePercentsMsg          = invalidParameterPlaceholder(argFeePercents)
	invalidFeeExpireTTLMsg         = invalidParameterPlaceholder(argFeeExpireTTL)
	invalidAmountMsg               = invalidParameterPlaceholder(argAmount)
	invalidExchangeTypeMsg         = invalidParameterPlaceholder(argExchangeType)
	invalidExchangeUUIDMsg         = invalidParameterPlaceholder(argExchangeUUID)
	invalidDestShardAddressMsg     = invalidParameterPlaceholder(argDestShardAddress)
	invalidSenderPubKeyMsg         = invalidParameterPlaceholder(argSenderPubKey)
	invalidReceiverPubKeyMsg       = invalidParameterPlaceholder(argReceiverPubKey)
	invalidMultiSigRedeemScriptMsg = invalidParameterPlaceholder(argMultiSigRedeemScript)
	invalidTxHashMsg               = invalidParameterPlaceholder(argTxHash)
)

var (
	handler *limits.Handler
)

func InitHHTPInterface(limits *limits.Handler) (err error) {
	handler = limits

	http.HandleFunc("/api/v1/fees/", func(w http.ResponseWriter, r *http.Request) {
		if r.Method == "POST" {
			handleFeesUpdate(w, r)

		} else {
			w.WriteHeader(http.StatusBadRequest)
			return
		}
	})
	http.HandleFunc("/api/v1/reserve/", func(w http.ResponseWriter, r *http.Request) {
		if r.Method == "POST" {
			handleReserveUpdate(w, r)

		} else {
			w.WriteHeader(http.StatusBadRequest)
			return
		}
	})
	return
}

func RunHTTPInterface() (errors <-chan error) {
	errorsFlow := make(chan error)

	logger.Log.Info().Str(
		"interface", settings.Conf.Interfaces.Http.Interface()).Msg(
		"Initializing admin HTTP interface")

	go func() {
		errorsFlow <- http.ListenAndServe(settings.Conf.Interfaces.Http.Interface(), nil)
	}()

	logger.Log.Info().Msg("HTTP interface initialization is done")
	return errorsFlow
}

func handleFeesUpdate(w http.ResponseWriter, r *http.Request) {
	err := r.ParseForm()
	if err != nil {
		reportBadRequest(w, invalidBodyMsg)
		return
	}

	sourceShardID, err := validateAndNormalizeShardID(r.Form.Get(argSourceShardID))
	if err != nil {
		reportBadRequestFromError(w, invalidSourceShardIDMsg, err)
		return
	}

	destinationShardID, err := validateAndNormalizeShardID(r.Form.Get(argDestinationShardID))
	if err != nil {
		reportBadRequestFromError(w, invalidDestinationShardIDMsg, err)
		return
	}

	feeFixed, err := validateAndNormalizeFeeFixed(r.Form.Get(argFeeFixed))
	if err != nil {
		reportBadRequestFromError(w, invalidFeeFixedMsg, err)
		return
	}

	feePercents, err := validateAndNormalizeFeePercentage(r.Form.Get(argFeePercents))
	if err != nil {
		reportBadRequestFromError(w, invalidFeePercentsMsg, err)
		return
	}

	feeExpireTTL, err := validateAndNormalizeExpireTTL(r.Form.Get(argFeeExpireTTL))
	if err != nil {
		reportBadRequestFromError(w, invalidFeeExpireTTLMsg, err)
		return
	}

	fee := limits.Fee{
		Fixed:    feeFixed,
		Percents: feePercents,
		Expired:  feeExpireTTL,
	}

	shardsIDs := shards.Pair{
		SourceShard:      sourceShardID,
		DestinationShard: destinationShardID,
	}

	err = handler.SetFee(shardsIDs, fee)
	if err != nil {
		reportError(w, err)
		return
	}

	w.WriteHeader(http.StatusOK)
}

func handleReserveUpdate(w http.ResponseWriter, r *http.Request) {
	err := r.ParseForm()
	if err != nil {
		reportBadRequest(w, invalidBodyMsg)
		return
	}

	shardID, err := validateAndNormalizeShardID(r.Form.Get("shard-id"))
	if err != nil {
		msg := fmt.Sprintf(invalidShardIDMsg, err.Error())
		reportBadRequest(w, msg)
		return
	}

	reserve, err := validateAndNormalizeReservedAmount(r.Form.Get("reserve"))
	if err != nil {
		msg := fmt.Sprintf(invalidReserveMsg, err)
		reportBadRequest(w, msg)
		return
	}

	err = handler.SetFundsLimit(shardID, reserve)
	if err != nil {
		reportError(w, err)
		return
	}

	w.WriteHeader(http.StatusOK)
}

func reportError(w http.ResponseWriter, err error) {
	message := strings.Replace(err.Error(), `"`, `\"`, -1)
	description := fmt.Sprintf(`{"error": "%s"}`, message)
	_, _ = w.Write([]byte(description))
	w.WriteHeader(http.StatusInternalServerError)
}

func reportBadRequest(w http.ResponseWriter, message string) {
	description := fmt.Sprintf(`{"error": "%s"}`, message)
	_, _ = w.Write([]byte(description))
	w.WriteHeader(http.StatusBadRequest)
}

func reportBadRequestFromError(w http.ResponseWriter, placeholder string, err error) {
	msg := fmt.Sprintf(placeholder, err.Error())
	reportBadRequest(w, msg)
}
