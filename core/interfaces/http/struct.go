package http

import (
	"fmt"
	"time"
)

type StringDataBuilder interface {
	GetStringData() string
}

type ProposalResponse struct {
	ExchangeAgentID uint64    `json:"exchangeAgentId"`
	ReservedAmount  int64     `json:"reservedAmount"`
	Expire          time.Time `json:"expire"`
	SequenceAnchor  uint32    `json:"sequenceAnchor"`
	FeeFixedAmount  int64     `json:"feeFixedAmount"`
	FeePercents     uint16    `json:"feePercents"`
	Signature       string    `json:"signature"`
}

type InitExchangeRequest struct {
	ExchangeType            uint8  `json:"type"`
	SourceShardID           uint32 `json:"sourceShardId"`
	DestinationShardID      uint32 `json:"destinationShardId"`
	SourceAmount            int64  `json:"sourceAmount"`
	DestinationAmount       int64  `json:"destinationAmount"`
	DestinationShardAddress string `json:"destinationShardAddress"`
	SenderPubKey            string `json:"senderPubKey"`
	ReceiverPubKey          string `json:"receiverPubKey"`
}

type InitExchangeResponse struct {
	Code                                     uint8  `json:"code"`
	UUID                                     string `json:"uuid"`
	SourceShardMultiSigLockWindowBlocks      uint64 `json:"sourceShardMultiSigLockWindowBlocks"`
	DestinationShardMultiSigLockWindowBlocks uint64 `json:"destinationShardMultiSigLockWindowBlocks"`
}

func (i InitExchangeResponse) GetStringData() (data string) {
	data = fmt.Sprintf("code=%d&uuid=%s&sourceShardMultiSigLockWindowBlocks=%d&destinationShardMultiSigLockWindowBlocks=%d",
		i.Code, i.UUID, i.SourceShardMultiSigLockWindowBlocks, i.DestinationShardMultiSigLockWindowBlocks)
	return
}

type ClientFundsLockTxRequest struct {
	TxUUID               string `json:"uuid"`
	MultiSigRedeemScript string `json:"multiSigRedeemScript"`
	TxHash               string `json:"txHash"`
}

type ClientFundsLockTxResponse struct {
	Code uint8 `json:"code"`
}

func (i ClientFundsLockTxResponse) GetStringData() (data string) {
	data = fmt.Sprintf("code=%d", i.Code)
	return
}

type FetchCrossShardTxResponse struct {
	Code         uint8  `json:"code"`
	CrossShardTx string `json:"crossShardTx"`
	LockTxHash   string `json:"lockTxHash"`
	LockTxOutNum uint64 `json:"lockTxOutNum"`
}

func (i FetchCrossShardTxResponse) GetStringData() (data string) {
	data = fmt.Sprintf("code=%d&crossShardTx=%s&lockTxHash=%s&lockTxOutNum=%d",
		i.Code, i.CrossShardTx, i.LockTxHash, i.LockTxOutNum)
	return
}
