package http

import (
	"crypto/sha256"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"github.com/fasthttp/router"
	"github.com/google/uuid"
	jsoniter "github.com/json-iterator/go"
	"github.com/rs/zerolog/log"
	"github.com/valyala/fasthttp"
	"gitlab.com/jaxnet/core/shard.core/btcec"
	"gitlab.com/jaxnet/core/shard.core/btcutil"
	"gitlab.com/jaxnet/core/shard.core/types/chainhash"
	"gitlab.com/jaxnet/hub/ead/core/blockchain/shards"
	"gitlab.com/jaxnet/hub/ead/core/crypto"
	"gitlab.com/jaxnet/hub/ead/core/ec"
	"gitlab.com/jaxnet/hub/ead/core/epl"
	"gitlab.com/jaxnet/hub/ead/core/handlers/proposals"
	"gitlab.com/jaxnet/hub/ead/core/handlers/transactions"
	"gitlab.com/jaxnet/hub/ead/core/logger"
	"gitlab.com/jaxnet/hub/ead/core/messages/clients_messages"
	"gitlab.com/jaxnet/hub/ead/core/settings"
	"math/big"
	"math/rand"
	"time"
)

const (
	// Inter Components Communication TTL
	iccTTL = time.Second * 3
)

type Response struct {
	Data interface{} `json:"data"`
}

type DataWithSignature struct {
	Content   interface{} `json:"content"`
	Signature string      `json:"signature"`
}

type PublicHTTPInterface struct {
	proposalsHandler *proposals.Handler
	txHandler        *transactions.Handler
	router           *router.Router
}

func InitHTTPPublicInterface(
	pHandler *proposals.Handler, exchangesHandler *transactions.Handler) (i *PublicHTTPInterface, err error) {

	i = &PublicHTTPInterface{
		proposalsHandler: pHandler,
		txHandler:        exchangesHandler,
		router:           router.New(),
	}

	group := i.router.Group("/api/v1/")
	group.GET("proposals/", i.HandleProposalsRequests)
	group.POST("exchange/init/", i.HandleExchangeInitRequests)
	group.POST("exchange/lock/", i.HandleClientFundsLockTxRequests)
	group.GET("exchange/cross-shard/", i.HandleFetchCrossShardTxRequests)

	logRouterEntries(i.router)
	return
}

func (i *PublicHTTPInterface) Run() (errors <-chan error) {
	errorsFlow := make(chan error)

	logger.Log.Info().Str(
		"interface", settings.Conf.Interfaces.Http.Interface()).Msg(
		"Initializing admin HTTP interface")

	go func() {
		errorsFlow <- fasthttp.ListenAndServe(settings.Conf.Interfaces.HttpPublic.Interface(), i.router.Handler)
	}()

	logger.Log.Info().Msg("Public HTTP interface initialization is done")
	return errorsFlow
}

func logRouterEntries(r *router.Router) {
	for method, handlers := range r.List() {
		for _, handler := range handlers {
			log.Info().Str(
				"method", method).Str(
				"handler", handler).Msg(
				"Route set")
		}
	}
}

func (i *PublicHTTPInterface) HandleProposalsRequests(ctx *fasthttp.RequestCtx) {
	switch string(ctx.Method()) {
	case "GET":
		i.handleGetProposalsRequest(ctx)
		return

	default:
		ctx.SetStatusCode(fasthttp.StatusMethodNotAllowed)
		return
	}
}

func (i *PublicHTTPInterface) HandleExchangeInitRequests(ctx *fasthttp.RequestCtx) {
	switch string(ctx.Method()) {
	case "POST":
		i.handleExchangeInitRequest(ctx)
		return

	default:
		ctx.SetStatusCode(fasthttp.StatusMethodNotAllowed)
		return
	}
}

func (i *PublicHTTPInterface) HandleClientFundsLockTxRequests(ctx *fasthttp.RequestCtx) {
	switch string(ctx.Method()) {
	case "POST":
		i.handleClientFundsLockTxRequest(ctx)
		return

	default:
		ctx.SetStatusCode(fasthttp.StatusMethodNotAllowed)
		return
	}
}

func (i *PublicHTTPInterface) HandleFetchCrossShardTxRequests(ctx *fasthttp.RequestCtx) {
	switch string(ctx.Method()) {
	case "GET":
		i.handleFetchCrossShardTxRequest(ctx)
		return

	default:
		ctx.SetStatusCode(fasthttp.StatusMethodNotAllowed)
		return
	}
}

func (i *PublicHTTPInterface) handleGetProposalsRequest(ctx *fasthttp.RequestCtx) {
	sourceShardID, err := validateAndNormalizeShardID(string(ctx.QueryArgs().Peek(argSourceShardID)))
	if err != nil {
		reportBadRequestFromErrorFast(ctx, invalidSourceShardIDMsg, err)
		return
	}

	destinationShardID, err := validateAndNormalizeShardID(string(ctx.QueryArgs().Peek(argDestinationShardID)))
	if err != nil {
		reportBadRequestFromErrorFast(ctx, invalidDestinationShardIDMsg, err)
		return
	}

	list, err := i.proposalsHandler.HandleNetworkReceivedProposalsHttpRequest(
		shards.Pair{
			SourceShard:      sourceShardID,
			DestinationShard: destinationShardID})
	if err != nil {
		list = epl.NewExchangeAgentsProposalsList() // empty
	}

	var proposalsList []ProposalResponse
	for _, proposal := range list.Proposals {
		proposalsList = append(proposalsList, ProposalResponse{
			ExchangeAgentID: proposal.ExchangeAgentID,
			ReservedAmount:  proposal.ReservedAmount.Int64(),
			Expire:          proposal.Expire,
			SequenceAnchor:  proposal.SequenceAnchor,
			FeeFixedAmount:  proposal.FeeFixedAmount.Int64(),
			FeePercents:     uint16(proposal.FeePercents * 100),
			Signature:       hex.EncodeToString(proposal.Signature.Serialize()),
		})
	}

	data, err := jsoniter.Marshal(Response{Data: proposalsList})
	if err != nil {
		HandleErrorAndHideDetails(err, ctx)
		return
	}

	_, err = ctx.Write(data)
	if err != nil {
		HandleErrorAndHideDetails(err, ctx)
		return
	}

	ctx.Response.Header.SetContentType("application/json")
}

func (i *PublicHTTPInterface) handleExchangeInitRequest(ctx *fasthttp.RequestCtx) {

	requestData := InitExchangeRequest{}
	err := json.Unmarshal(ctx.Request.Body(), &requestData)
	if err != nil {
		reportBadRequestFromErrorFast(ctx, invalidBodyMsg, err)
		return
	}

	if requestData.ExchangeType != clients_messages.RequestInitExchangeType2Sig &&
		requestData.ExchangeType != clients_messages.RequestInitExchangeType3Sig {
		err = fmt.Errorf(
			"exchange type must be a positive number in range [0 .. 1]: (%w)", ec.ErrInvalidExchangeType)
		reportBadRequestFromErrorFast(ctx, invalidExchangeTypeMsg, err)
		return
	}

	destinationShardAddress, err := btcutil.DecodeAddress(requestData.DestinationShardAddress, settings.NetParams)
	if err != nil {
		reportBadRequestFromErrorFast(ctx, invalidDestShardAddressMsg, err)
		return
	}

	senderPubKeyBytes, err := hex.DecodeString(requestData.SenderPubKey)
	if err != nil {
		reportBadRequestFromErrorFast(ctx, invalidSenderPubKeyMsg, err)
		return
	}

	senderPubKey, err := btcec.ParsePubKey(senderPubKeyBytes, btcec.S256())
	if err != nil {
		reportBadRequestFromErrorFast(ctx, invalidSenderPubKeyMsg, err)
		return
	}

	txHandlerRequest := clients_messages.RequestInitExchange{
		Type: requestData.ExchangeType,
		Index: shards.NewPairIndex(shards.ID(requestData.SourceShardID),
			shards.ID(requestData.DestinationShardID)),
		SourceAmount:            big.NewInt(requestData.SourceAmount),
		DestinationAmount:       big.NewInt(requestData.DestinationAmount),
		DestinationShardAddress: destinationShardAddress.(*btcutil.AddressPubKeyHash),
		SenderPubKey:            senderPubKey,
	}

	if requestData.ExchangeType == clients_messages.RequestInitExchangeType3Sig {
		receiverPubKeyBytes, err := hex.DecodeString(requestData.ReceiverPubKey)
		if err != nil {
			reportBadRequestFromErrorFast(ctx, invalidReceiverPubKeyMsg, err)
			return
		}

		receiverPubKey, err := btcec.ParsePubKey(receiverPubKeyBytes, btcec.S256())
		if err != nil {
			reportBadRequestFromErrorFast(ctx, invalidReceiverPubKeyMsg, err)
			return
		}

		txHandlerRequest.ReceiverPubKey = receiverPubKey
	}

	txHandlerResponses, err := i.txHandler.Flow.Pass(&txHandlerRequest, iccTTL)
	if err != nil {
		HandleErrorAndHideDetails(err, ctx)
		return
	}

	txHandlerResponse, err := txHandlerResponses.Fetch(iccTTL)
	if err != nil {
		HandleErrorAndHideDetails(err, ctx)
		return
	}

	txHandlerInitExchangeResponse := txHandlerResponse.(*clients_messages.ResponseInitExchange)
	initExchangeResponse := InitExchangeResponse{
		Code:                                     txHandlerInitExchangeResponse.Code,
		UUID:                                     txHandlerInitExchangeResponse.TxUUID.String(),
		SourceShardMultiSigLockWindowBlocks:      txHandlerInitExchangeResponse.SourceShardMultiSigLockWindowBlocks,
		DestinationShardMultiSigLockWindowBlocks: txHandlerInitExchangeResponse.DestinationShardMultiSigLockWindowBlocks,
	}

	err = writeJsonResponse1(ctx, initExchangeResponse)
	if err != nil {
		HandleErrorAndHideDetails(err, ctx)
		return
	}
}

func (i *PublicHTTPInterface) handleClientFundsLockTxRequest(ctx *fasthttp.RequestCtx) {

	requestData := ClientFundsLockTxRequest{}
	err := json.Unmarshal(ctx.Request.Body(), &requestData)
	if err != nil {
		reportBadRequestFromErrorFast(ctx, invalidBodyMsg, err)
		return
	}

	txUUID, err := uuid.Parse(requestData.TxUUID)
	if err != nil {
		reportBadRequestFromErrorFast(ctx, invalidExchangeUUIDMsg, err)
		return
	}

	multiSigRedeemScript, err := hex.DecodeString(requestData.MultiSigRedeemScript)
	if err != nil {
		reportBadRequestFromErrorFast(ctx, invalidMultiSigRedeemScriptMsg, err)
		return
	}

	txHash, err := chainhash.NewHashFromStr(requestData.TxHash)
	if err != nil {
		reportBadRequestFromErrorFast(ctx, invalidTxHashMsg, err)
		return
	}

	txHandlerRequest := clients_messages.RequestClientFundsLockTx{
		TxUUID:               txUUID,
		MultiSigRedeemScript: multiSigRedeemScript,
		TxHash:               txHash[:],
	}

	txHandlerResponses, err := i.txHandler.Flow.Pass(&txHandlerRequest, iccTTL)
	if err != nil {
		HandleErrorAndHideDetails(err, ctx)
		return
	}

	txHandlerResponse, err := txHandlerResponses.Fetch(iccTTL)
	if err != nil {
		HandleErrorAndHideDetails(err, ctx)
		return
	}

	txHandlerClientFundsLockTxResponse := txHandlerResponse.(*clients_messages.ResponseClientFundsLockTx)
	clientFundsLockTxResponse := ClientFundsLockTxResponse{
		Code: txHandlerClientFundsLockTxResponse.Code,
	}

	err = writeJsonResponse1(ctx, clientFundsLockTxResponse)
	if err != nil {
		HandleErrorAndHideDetails(err, ctx)
		return
	}
}

func (i *PublicHTTPInterface) handleFetchCrossShardTxRequest(ctx *fasthttp.RequestCtx) {
	txUUID, err := uuid.Parse(string(ctx.QueryArgs().Peek(argExchangeUUID)))
	if err != nil {
		reportBadRequestFromErrorFast(ctx, invalidExchangeUUIDMsg, err)
		return
	}

	txHandlerRequest := clients_messages.RequestFetchCrossShardTx{
		TxUUID: txUUID,
	}

	txHandlerResponses, err := i.txHandler.Flow.Pass(&txHandlerRequest, iccTTL)
	if err != nil {
		HandleErrorAndHideDetails(err, ctx)
		return
	}

	txHandlerResponse, err := txHandlerResponses.Fetch(iccTTL)
	if err != nil {
		HandleErrorAndHideDetails(err, ctx)
		return
	}

	txHandlerFetchCrossShardTxResponse := txHandlerResponse.(*clients_messages.ResponseFetchCrossShardTx)
	fetchCrossShardTxResponse := FetchCrossShardTxResponse{
		Code:         txHandlerFetchCrossShardTxResponse.Code,
		CrossShardTx: hex.EncodeToString(txHandlerFetchCrossShardTxResponse.CrossShardTx),
		LockTxHash:   txHandlerFetchCrossShardTxResponse.LockTxHash,
		LockTxOutNum: txHandlerFetchCrossShardTxResponse.LockTxOutNum,
	}

	err = writeJsonResponse1(ctx, fetchCrossShardTxResponse)
	if err != nil {
		HandleErrorAndHideDetails(err, ctx)
		return
	}
}

func reportBadRequestFromErrorFast(ctx *fasthttp.RequestCtx, placeholder string, err error) {
	msg := fmt.Sprintf(placeholder, err.Error())
	ctx.Error(msg, fasthttp.StatusBadRequest)
}

// HandleErrorAndHideDetails is used in the cases when error must be logged but no details must be shared with user.
func HandleErrorAndHideDetails(err error, ctx *fasthttp.RequestCtx) {
	errorID := rand.Int31()

	logger.Log.Err(err).Int32(
		"error-id", errorID).Msg(
		"error occurred during processing http request")

	ctx.Error(
		fmt.Sprint("Unexpected error occurred (id=", errorID, ")"),
		fasthttp.StatusInternalServerError)
}

func writeJsonResponse1(ctx *fasthttp.RequestCtx, httpResponse StringDataBuilder) (err error) {
	hash := sha256.New()
	hash.Write([]byte(httpResponse.GetStringData()))

	signature, err := crypto.Sign(hash.Sum(nil))
	if err != nil {
		return
	}

	dataResp, err := jsoniter.Marshal(
		Response{
			Data: DataWithSignature{
				Content:   httpResponse,
				Signature: hex.EncodeToString(signature.Serialize()),
			},
		})
	if err != nil {
		return
	}

	_, err = ctx.Write(dataResp)
	if err != nil {
		return
	}

	ctx.Response.Header.SetContentType("application/json")
	return
}
