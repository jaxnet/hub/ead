package tcp

import (
	"encoding"
	"gitlab.com/jaxnet/hub/ead/core/marshalling"
)

type Response interface {
	encoding.BinaryMarshaler
	encoding.BinaryUnmarshaler

	TypeID() uint8
}

type SignedResponse interface {
	Response

	SignAndAttachSignature(dataEnc *marshalling.Encoder) (content []byte, err error)
	ReadSignatureAndDecoupleData(binaryData []byte) (data []byte, err error)
}
