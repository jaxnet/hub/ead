package exchange_agents

import (
	"errors"
	"fmt"
	"gitlab.com/jaxnet/hub/ead/core/blockchain/discovery"
	"gitlab.com/jaxnet/hub/ead/core/epl"
	"gitlab.com/jaxnet/hub/ead/core/interfaces/tcp/exchange_agents/messages"
	"gitlab.com/jaxnet/hub/ead/core/marshalling"
	"net"
	"strconv"
	"strings"
	"time"
)

var (
	ErrNoOneAddressAcceptsConnection = errors.New("no one of the specified addresses accepts connections")
	ErrInvalidDiscoveryRecord        = errors.New("invalid discovery record")
)

var (
	sendingTimeout = time.Second * 5
)

type OutgoingConnectionHandler struct {
	connection net.Conn
}

func NewOutgoingConnectionHandler(addressee *discovery.DiscoveryRecord) (handler *OutgoingConnectionHandler, err error) {
	handler = &OutgoingConnectionHandler{}

	for discoveryRecord, _ := range addressee.Addresses {
		conn, err := handler.discoverEA(discoveryRecord)
		if err != nil {
			// This discoveryRecord does not accept connections.
			// Try another one, or report error on the exit.
			continue
		}

		bytesWritten, err := conn.Write([]byte{messages.DefaultProtocolVersion})
		if err != nil || bytesWritten != messages.ProtocolVersionFieldSize {
			// Protocol version could not sent.
			// Another connection should be used.
			continue
		}

		handler.connection = conn
		return handler, err
	}

	err = ErrNoOneAddressAcceptsConnection
	return
}

func (handler *OutgoingConnectionHandler) CloseAfterTimeout() {
	time.Sleep(sendingTimeout)
	_ = handler.connection.Close()
}

func (handler *OutgoingConnectionHandler) SendProposal(proposal *epl.ExchangeAgentProposal) (err error) {
	encoder := marshalling.NewEncoder()

	err = encoder.PutUint8(messages.MessageTypeProposal)
	if err != nil {
		return
	}

	data, err := proposal.MarshalBinary()
	if err != nil {
		return
	}

	err = encoder.PutVariadicDataWith2BytesHeader(data)
	if err != nil {
		return
	}

	// todo: comment
	//x := encoder.CollectDataAndReleaseBuffers()
	_, err = handler.connection.Write(encoder.CollectDataAndReleaseBuffers())
	if err != nil {
		return
	}

	return
}

func (handler *OutgoingConnectionHandler) discoverEA(ipv4DiscoveryRecord string) (conn net.Conn, err error) {
	addressesComponents := strings.Split(ipv4DiscoveryRecord, ":")

	// todo : process ipv6 address
	if len(addressesComponents) == 0 {
		err = ErrInvalidDiscoveryRecord
		return
	}

	address := addressesComponents[0]
	port := uint16(defaultPort)
	if len(addressesComponents) == 2 {
		// Discovery record contains non-standard port for EA's connections.
		// See discovery strings documentation for the details.
		portNum, err1 := strconv.ParseUint(addressesComponents[1], 10, 16)
		if err1 != nil {
			err = err1
			return
		}
		// port from discovery record is used for client's interface.
		//for ead's interface is used client's interface port + 1
		port = uint16(portNum + 1)
	}

	addressAndPort := fmt.Sprintf("%s:%d", address, port)
	conn, err = net.Dial("tcp", addressAndPort)
	return
}

func (handler *OutgoingConnectionHandler) SendGetProposalRequest(
	getProposalRequest epl.ExchangeAgentProposalGetRequest) (err error) {

	encoder := marshalling.NewEncoder()

	err = encoder.PutUint8(messages.MessageTypeGetProposals)
	if err != nil {
		return
	}

	data, err := getProposalRequest.MarshalBinary()
	if err != nil {
		return
	}

	err = encoder.PutVariadicDataWith2BytesHeader(data)
	if err != nil {
		return
	}

	// todo: comment
	//x := encoder.CollectDataAndReleaseBuffers()
	_, err = handler.connection.Write(encoder.CollectDataAndReleaseBuffers())
	if err != nil {
		return
	}

	return
}

func (handler *OutgoingConnectionHandler) SendOwnProposals(ownProposals *epl.OwnExchangeAgentProposals) (err error) {

	encoder := marshalling.NewEncoder()
	err = encoder.PutUint8(messages.MessageTypeOwnProposals)
	if err != nil {
		return
	}

	responseBinary, err := ownProposals.MarshalBinary()
	if err != nil {
		return
	}

	err = encoder.PutVariadicDataWith2BytesHeader(responseBinary)
	if err != nil {
		return
	}

	_, err = handler.connection.Write(encoder.CollectDataAndReleaseBuffers())
	if err != nil {
		return
	}

	return
}
