package exchange_agents

import (
	"github.com/davecgh/go-spew/spew"
	"gitlab.com/jaxnet/hub/ead/core/blockchain/discovery"
	"gitlab.com/jaxnet/hub/ead/core/crypto"
	"gitlab.com/jaxnet/hub/ead/core/epl"
	"gitlab.com/jaxnet/hub/ead/core/logger"
	"gitlab.com/jaxnet/hub/ead/core/settings"
	"net"
	"time"
)

const (
	defaultPort = 3000
)

type TCPInterface struct {
	// Notifications about changed transactions agents list.
	// This notifications are used to adjust the broadcasting addresses.
	incEventsAgentsAddressesUpdates chan []*discovery.DiscoveryRecord

	// Notifications about changed proposal of the current exchange agent.
	// This proposals are going to be broadcast to the rest exchange agents in this shard.
	incEventsProposalChanged chan *epl.ExchangeAgentProposal

	incEventsProposalGetRequest chan *epl.ExchangeAgentProposalGetRequest

	outgoingOwnProposals chan *epl.OwnExchangeAgentProposals

	incEventsOwnProposals chan *epl.OwnExchangeAgentProposals

	outgoingProposals chan *epl.ExchangeAgentProposal
}

func NewExchangeAgentsTCPInterface() (i *TCPInterface, err error) {
	i = &TCPInterface{
		incEventsProposalChanged:        make(chan *epl.ExchangeAgentProposal),
		incEventsProposalGetRequest:     make(chan *epl.ExchangeAgentProposalGetRequest),
		outgoingOwnProposals:            make(chan *epl.OwnExchangeAgentProposals),
		incEventsOwnProposals:           make(chan *epl.OwnExchangeAgentProposals),
		incEventsAgentsAddressesUpdates: make(chan []*discovery.DiscoveryRecord),
		outgoingProposals:               make(chan *epl.ExchangeAgentProposal),
	}

	return
}

func (i *TCPInterface) Run() (errors <-chan error) {
	errorsFlow := make(chan error)

	// todo: add gracefull stop
	go i.processProposalsBroadcasting()

	go func() {
		listener, err := net.Listen("tcp", settings.Conf.Interfaces.ExchangeAgents.Interface())
		if err != nil {
			errorsFlow <- err
			return
		}

		defer listener.Close()

		for {
			conn, err := listener.Accept()
			if err != nil {
				logger.Log.Err(err).Msg("")
				continue
			}

			//err = conn.(*net.TCPConn).SetKeepAlive(false)
			//if err != nil {
			//    continue
			//}

			handler, err := NewIncomingConnectionHandler(conn, i)
			if err != nil {
				logger.Log.Err(err).Msg("")
				continue
			}

			handler.Run()
		}
	}()

	return errorsFlow
}

func (i *TCPInterface) OnIncomingProposal() <-chan *epl.ExchangeAgentProposal {
	return i.incEventsProposalChanged
}

func (i *TCPInterface) OnIncomingProposalGetRequest() <-chan *epl.ExchangeAgentProposalGetRequest {
	return i.incEventsProposalGetRequest
}

func (i *TCPInterface) OnIncomingOwnProposal() <-chan *epl.OwnExchangeAgentProposals {
	return i.incEventsOwnProposals
}

func (i *TCPInterface) BroadcastProposal(proposal *epl.ExchangeAgentProposal) {
	// todo: enqueues only
	i.outgoingProposals <- proposal
}

func (i *TCPInterface) SetAgentsAddresses(addresses []*discovery.DiscoveryRecord) {
	i.incEventsAgentsAddressesUpdates <- addresses
}

func (i *TCPInterface) RespondOwnProposals(ownProposals *epl.OwnExchangeAgentProposals) {
	i.outgoingOwnProposals <- ownProposals
}

func (i *TCPInterface) processProposalsBroadcasting() {
	const broadcastDelay = time.Second * 1

	var (
		latestEAL      []*discovery.DiscoveryRecord
		latestProposal *epl.ExchangeAgentProposal
		lastBroadcast  = time.Now()

		isProposalsFromOtherAgentsRequested = false
		ownProposals                        *epl.OwnExchangeAgentProposals
	)

	broadcastProposalIfAddressesAreSet := func() {
		// If EAL ahs not been set yet, or no proposal has been set yet -
		// no broadcast attempt must be done.
		if latestEAL == nil || latestProposal == nil {
			return
		}

		i.broadcastProposal(latestEAL, latestProposal)

		// Mark proposal as sent
		// and prevent redundant sending attempt of the same data.
		latestProposal = nil
	}

	sendOwnProposalsToRequesterIfAddressesAreSet := func() {
		// If EAL ahs not been set yet -
		// no send attempt must be done.
		if latestEAL == nil || ownProposals == nil {
			return
		}

		i.sendOwnProposalsToRequester(latestEAL, ownProposals)

	}

	for {
		select {
		case latestEAL = <-i.incEventsAgentsAddressesUpdates:
			{
				if !isProposalsFromOtherAgentsRequested {
					i.sendRequestForProposalsToAllAgents(latestEAL)
					isProposalsFromOtherAgentsRequested = true
				}
			}

		case latestProposal = <-i.outgoingProposals:
			{
				if time.Now().After(lastBroadcast.Add(broadcastDelay)) {
					broadcastProposalIfAddressesAreSet()
				}
			}

		case ownProposals = <-i.outgoingOwnProposals:
			sendOwnProposalsToRequesterIfAddressesAreSet()

		case <-time.After(broadcastDelay):
			{
				broadcastProposalIfAddressesAreSet()
			}
		}

	}
}

func (i *TCPInterface) broadcastProposal(agents []*discovery.DiscoveryRecord, proposal *epl.ExchangeAgentProposal) {
	for _, record := range agents {
		if crypto.PubKeyMatchOurOwn(record.PubKey) {
			// Prevent sending messages to ourself.
			continue
		}

		go func(r *discovery.DiscoveryRecord) {
			handler, err := NewOutgoingConnectionHandler(r)
			if err != nil {
				logger.Log.Err(err).Msg(
					"can't setup outgoing connection handler")

				return
			}

			err = handler.SendProposal(proposal)
			if err != nil {
				logger.Log.Err(err).Str(
					"peer", spew.Sdump(r)).Str(
					"proposal", spew.Sdump(proposal)).Msg(
					"can't send proposal update to the remote peer")

				return
			}

			handler.CloseAfterTimeout()
		}(record)
	}
}

func (i *TCPInterface) sendRequestForProposalsToAllAgents(agents []*discovery.DiscoveryRecord) {
	var ownExchangeAgentID uint64

	// get ID of current EAD
	for _, rec := range agents {
		if crypto.PubKeyMatchOurOwn(rec.PubKey) {
			ownExchangeAgentID = rec.SeqNumber
			break
		}
	}

	request := epl.ExchangeAgentProposalGetRequest{
		ExchangeAgentID: ownExchangeAgentID,
	}

	err := request.Sign()
	if err != nil {
		logger.Log.Err(err).Msg(
			"can't sign Get Proposals Request")
		return
	}
	for _, record := range agents {
		if crypto.PubKeyMatchOurOwn(record.PubKey) {
			// Prevent sending messages to ourself.
			continue
		}

		go func(r *discovery.DiscoveryRecord) {
			handler, err := NewOutgoingConnectionHandler(r)
			if err != nil {
				logger.Log.Err(err).Msg(
					"Can't setup outgoing connection handler")
				return
			}

			err = handler.SendGetProposalRequest(request)
			if err != nil {
				logger.Log.Err(err).Str(
					"peer", spew.Sdump(r)).Msg(
					"can't send proposal request to the remote peer")

				return
			}
			handler.CloseAfterTimeout()
		}(record)
	}
}

func (i *TCPInterface) sendOwnProposalsToRequester(agents []*discovery.DiscoveryRecord, ownProposals *epl.OwnExchangeAgentProposals) {
	for _, record := range agents {
		if record.SeqNumber == ownProposals.RequesterEAD {
			handler, err := NewOutgoingConnectionHandler(record)
			if err != nil {
				logger.Log.Err(err).Msg(
					"can't setup outgoing connection handler")
				return
			}

			err = handler.SendOwnProposals(ownProposals)
			if err != nil {
				logger.Log.Err(err).Str(
					"peer", spew.Sdump(record)).Msg(
					"can't send proposal update to the remote peer")
				return
			}
			handler.CloseAfterTimeout()
			break
		}
	}
}
