package messages

const (
	ProtocolVersionFieldSize = 1

	FieldSize_MessageType   = 2
	FieldOffset_MessageType = 0

	FieldSize_MessageSize   = 2
	FieldOffset_MessageSize = FieldOffset_MessageType + FieldSize_MessageType
)

const (
	// 0..128 are reserved for system purpose.

	MessageTypeProposal     = 128
	MessageTypeGetProposals = 129
	MessageTypeOwnProposals = 130
)

const (
	DefaultProtocolVersion = 1
)
