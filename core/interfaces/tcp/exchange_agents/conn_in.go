package exchange_agents

import (
	"bufio"
	"errors"
	"gitlab.com/jaxnet/hub/ead/core/epl"
	"gitlab.com/jaxnet/hub/ead/core/interfaces/tcp/exchange_agents/messages"
	"gitlab.com/jaxnet/hub/ead/core/logger"
	"gitlab.com/jaxnet/hub/ead/core/marshalling"
	"io"
	"net"
)

var (
	ErrUnsupportedMessageType = errors.New("unsupported message type")
	ErrProtocolError          = errors.New("incoming data flow does not correspond to the expected protocol")
)

//
//var (
//	idleTimeout = time.Second * 10
//)

type IncomingConnectionHandler struct {
	connection net.Conn
	reader     io.Reader
	root       *TCPInterface
}

func NewIncomingConnectionHandler(connection net.Conn, i *TCPInterface) (handler *IncomingConnectionHandler, err error) {
	handler = &IncomingConnectionHandler{
		connection: connection,
		root:       i,
		reader:     bufio.NewReader(connection),
	}

	return
}

func (handler *IncomingConnectionHandler) Run() {
	defer handler.connection.Close()

	var err error
	defer func() {
		if err != nil && err.Error() != "EOF" {
			logger.Log.Err(err).Str(
				"remote-addr", handler.connection.RemoteAddr().String()).Msg(
				"Connection closed with error")
		}
	}()

	err = handler.checkVersion()
	if err != nil {
		return
	}

	for {
		err = handler.receiveMessage()
		if err != nil {
			return
		}
	}
}

func (handler *IncomingConnectionHandler) checkVersion() (err error) {
	decoder := marshalling.NewDecoderFromReader(handler.reader)
	version, err := decoder.GetUint8()
	if err != nil {
		return
	}

	if version != messages.DefaultProtocolVersion {
		err = ErrProtocolError
		return
	}

	return
}

func (handler *IncomingConnectionHandler) receiveMessage() (err error) {
	decoder := marshalling.NewDecoderFromReader(handler.reader)

	messageType, err := decoder.GetUint8()
	if err != nil {
		return
	}

	data, err := decoder.GetDataSegmentWith2BytesHeader()
	if err != nil {
		return
	}

	switch messageType {
	case messages.MessageTypeProposal:
		{
			err = handler.readValidateAndDispatchProposal(data)
		}
	case messages.MessageTypeGetProposals:
		{
			err = handler.readValidateAndDispatchGetProposal(data)
		}
	case messages.MessageTypeOwnProposals:
		{
			err = handler.readValidateAndDispatchOwnProposals(data)
		}

	default:
		err = ErrUnsupportedMessageType
	}

	return
}

func (handler *IncomingConnectionHandler) readValidateAndDispatchProposal(data []byte) (err error) {
	proposal := &epl.ExchangeAgentProposal{}
	err = proposal.UnmarshalBinary(data)
	if err != nil {
		return
	}

	// todo: add signature validation

	handler.root.incEventsProposalChanged <- proposal
	return
}

func (handler *IncomingConnectionHandler) readValidateAndDispatchGetProposal(data []byte) (err error) {
	proposalGetRequest := &epl.ExchangeAgentProposalGetRequest{}
	err = proposalGetRequest.UnmarshalBinary(data)
	if err != nil {
		return
	}

	// todo: add signature validation

	handler.root.incEventsProposalGetRequest <- proposalGetRequest
	return
}

func (handler *IncomingConnectionHandler) readValidateAndDispatchOwnProposals(data []byte) (err error) {
	ownProposalsRequest := &epl.OwnExchangeAgentProposals{}
	err = ownProposalsRequest.UnmarshalBinary(data)
	if err != nil {
		return
	}

	// todo: add signature validation

	handler.root.incEventsOwnProposals <- ownProposalsRequest
	return
}
