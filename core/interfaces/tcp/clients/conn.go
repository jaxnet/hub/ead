package clients

import (
	"bufio"
	"errors"
	"gitlab.com/jaxnet/hub/ead/core/interfaces/tcp"
	"gitlab.com/jaxnet/hub/ead/core/logger"
	"gitlab.com/jaxnet/hub/ead/core/marshalling"
	"gitlab.com/jaxnet/hub/ead/core/messages/clients_messages"
	"gitlab.com/jaxnet/hub/ead/core/settings"
	"io"
	"net"
	"time"
)

var (
	ErrUnsupportedMessageType = errors.New("unsupported message type")
	ErrProtocolError          = errors.New("incoming data flow does not correspond to the expected protocol")
	ErrInternalComponentBusy  = errors.New("internal component is not responding")
)

const (
	// Inter Components Communication TTL
	iccTTL = time.Second * 3
)

type IncomingConnectionHandler struct {
	connection net.Conn
	root       *TCPInterface
	reader     io.Reader
}

// todo: replace link to pparent struct with callback.
// todo: remove reader from the same tcp interface implementation of the eas package.
func NewIncomingConnectionHandler(connection net.Conn, i *TCPInterface) (handler *IncomingConnectionHandler, err error) {
	handler = &IncomingConnectionHandler{
		connection: connection,
		root:       i,
		reader:     bufio.NewReader(connection),
	}

	return
}

func (h *IncomingConnectionHandler) Run() {
	if settings.Conf.Debug {
		logger.Log.Debug().Str(
			"peer", h.connection.RemoteAddr().String()).Msg(
			"Incoming client connection established")
	}

	// In case if h exits with an logError - report it to log.
	// There is no way/need to report errors to the outer scope,
	// cause there is no way to handle them except to stop the h
	// (which is done here).
	var err error
	defer func() {
		if err != nil && err.Error() != "EOF" {
			logger.Log.Err(err).Str(
				"peer", h.connection.RemoteAddr().String()).Msg("")
		}
	}()

	defer h.connection.Close()
	err = h.beginCommunication()
}

// todo: all messages from the client must be signed by his private key.
func (h *IncomingConnectionHandler) beginCommunication() (err error) {
	err = h.checkVersion()
	if err != nil {
		return
	}

	if settings.Conf.Debug {
		logger.Log.Debug().Str(
			"peer", h.connection.RemoteAddr().String()).Msg(
			"Remote client protocol version checked")
	}

	for {
		err = h.receiveMessage()
		if err != nil {
			return
		}
	}
}

func (h *IncomingConnectionHandler) checkVersion() (err error) {
	decoder := marshalling.NewDecoderFromReader(h.reader)
	version, err := decoder.GetUint8()
	if err != nil {
		return
	}

	if version != clients_messages.DefaultProtocolVersion {
		err = ErrProtocolError
		return
	}

	return
}

func (h *IncomingConnectionHandler) receiveMessage() (err error) {
	decoder := marshalling.NewDecoderFromReader(h.reader)

	messageType, err := decoder.GetUint8()
	if err != nil {
		return
	}

	switch messageType {
	case clients_messages.ReqProposals:
		return h.readValidateAndDispatchProposalsRequest(decoder)

	case clients_messages.ReqInitExchange:
		return h.processExchangeInitRequest(decoder)

	case clients_messages.ReqClientFundsLockTX:
		return h.processClientLockFundsTxRequest(decoder)

	case clients_messages.ReqFetchCrossShardTx:
		return h.processFetchCrossShardTxRequest(decoder)

	//case clients_messages.ReqEADFundsLockingTX:
	//	return h.processEADLockFundsTxRequest(decoder)
	//
	//case clients_messages.ReqCrossShardTx:
	//	return h.processCrossShardTxRequest(decoder)

	default:
		err = ErrUnsupportedMessageType
	}

	return
}

func (h *IncomingConnectionHandler) sendSignedResponse(response tcp.SignedResponse) (err error) {
	encoder := marshalling.NewEncoder()
	err = encoder.PutUint8(response.TypeID())
	if err != nil {
		return
	}

	content, err := response.MarshalBinary()
	if err != nil {
		return
	}

	err = encoder.PutVariadicDataWith2BytesHeader(content)
	if err != nil {
		return
	}

	return h.collectAndSendEncodersData(encoder)
}

func (h *IncomingConnectionHandler) collectAndSendEncodersData(encoder *marshalling.Encoder) (err error) {
	data := encoder.CollectDataAndReleaseBuffers()

	// todo: enhance (partial write is possible here)
	_, err = h.connection.Write(data)
	if err != nil {
		return
	}
	return
}
