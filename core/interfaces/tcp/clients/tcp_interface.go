package clients

import (
	"gitlab.com/jaxnet/hub/ead/core/handlers/proposals"
	"gitlab.com/jaxnet/hub/ead/core/handlers/transactions"
	"gitlab.com/jaxnet/hub/ead/core/logger"
	"gitlab.com/jaxnet/hub/ead/core/settings"
	"net"
)

type TCPInterface struct {
	proposalsHandler *proposals.Handler
	txHandler        *transactions.Handler
}

func NewTCPInterface(
	proposalsHandler *proposals.Handler, exchangesHandler *transactions.Handler) (i *TCPInterface, err error) {

	i = &TCPInterface{
		proposalsHandler: proposalsHandler,
		txHandler:        exchangesHandler,
	}

	return
}

func (i *TCPInterface) Run() (errors <-chan error) {
	errorsFlow := make(chan error)

	go func() {
		netParams := settings.Conf.Interfaces.Clients
		listener, err := net.Listen("tcp", netParams.Interface())
		if err != nil {
			errorsFlow <- err
			return
		}

		logger.Log.Info().Str(
			"interface", netParams.Interface()).Msg(
			"Clients interface initialisation is done")

		for {
			conn, err := listener.Accept()
			if err != nil {
				logger.Log.Err(err).Msg("")
				continue
			}

			handler, err := NewIncomingConnectionHandler(conn, i)
			if err != nil {
				logger.Log.Err(err).Msg("")
				continue
			}

			go handler.Run()
		}
	}()

	return errorsFlow
}
