package clients

import (
	"gitlab.com/jaxnet/hub/ead/core/epl"
	"gitlab.com/jaxnet/hub/ead/core/logger"
	"gitlab.com/jaxnet/hub/ead/core/marshalling"
	"gitlab.com/jaxnet/hub/ead/core/messages/clients_messages"
	"gitlab.com/jaxnet/hub/ead/core/settings"
	"time"
)

func (h *IncomingConnectionHandler) readValidateAndDispatchProposalsRequest(decoder *marshalling.Decoder) (err error) {
	data, err := decoder.GetDataSegmentWithByteHeader()
	if err != nil {
		return
	}

	request := &clients_messages.ProposalsRequest{}
	err = request.UnmarshalBinary(data)
	if err != nil {
		return
	}

	if settings.Conf.Debug {
		logger.Log.Debug().Str(
			"peer", h.connection.RemoteAddr().String()).Msg(
			"Proposals request received")
	}

	err = h.respondWithCurrentProposalsAvailable(request)
	return
}

func (h *IncomingConnectionHandler) respondWithCurrentProposalsAvailable(req *clients_messages.ProposalsRequest) (err error) {
	list, err := h.root.proposalsHandler.HandleNetworkReceivedProposalsRequest(req)
	if err != nil {
		list = epl.NewExchangeAgentsProposalsList() // empty
	}

	encoder := marshalling.NewEncoder()
	err = encoder.PutUint8(clients_messages.RespProposals)
	if err != nil {
		return
	}

	response := clients_messages.NewProposalResponse(list)
	responseBinary, err := response.MarshalBinary()
	if err != nil {
		return
	}

	err = encoder.PutVariadicDataWith2BytesHeader(responseBinary)
	if err != nil {
		return
	}

	_, err = h.connection.Write(encoder.CollectDataAndReleaseBuffers())
	if err != nil {
		return
	}

	time.Sleep(time.Second * 10) // todo: set config
	return
}
