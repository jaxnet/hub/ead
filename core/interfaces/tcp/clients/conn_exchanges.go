package clients

import (
	"fmt"
	"gitlab.com/jaxnet/hub/ead/core/interfaces/tcp"
	"gitlab.com/jaxnet/hub/ead/core/marshalling"
	"gitlab.com/jaxnet/hub/ead/core/messages"
	"gitlab.com/jaxnet/hub/ead/core/messages/clients_messages"
)

func (h *IncomingConnectionHandler) processExchangeInitRequest(
	decoder *marshalling.Decoder) (err error) {
	return h.forwardClientRequest(&clients_messages.RequestInitExchange{}, decoder)
}

func (h *IncomingConnectionHandler) processClientLockFundsTxRequest(
	decoder *marshalling.Decoder) (err error) {
	return h.forwardClientRequest(&clients_messages.RequestClientFundsLockTx{}, decoder)
}

func (h *IncomingConnectionHandler) processFetchCrossShardTxRequest(
	decoder *marshalling.Decoder) (err error) {
	return h.forwardClientRequest(&clients_messages.RequestFetchCrossShardTx{}, decoder)
}

func (h *IncomingConnectionHandler) forwardClientRequest(
	req messages.Request, decoder *marshalling.Decoder) (err error) {

	err = decoder.UnmarshalDataSegmentWith2BytesHeader(req)
	if err != nil {
		err = fmt.Errorf("%v: %v", ErrProtocolError, err)
		return
	}

	responses, err := h.root.txHandler.Flow.Pass(req, iccTTL)
	if err != nil {
		err = fmt.Errorf("%v: %v", ErrInternalComponentBusy, err)
		return
	}

	response, err := responses.Fetch(iccTTL)
	if err != nil {
		h.reportError(err, clients_messages.ErrorCodeTemporaryUnavailable)
		return
	}

	err = h.sendSignedResponse(response.(tcp.SignedResponse))
	return
}
