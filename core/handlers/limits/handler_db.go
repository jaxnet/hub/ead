package limits

import (
	"context"
	"fmt"
	"gitlab.com/jaxnet/core/shard.core/btcutil"
	"gitlab.com/jaxnet/hub/ead/core/blockchain/shards"
	"gitlab.com/jaxnet/hub/ead/core/database"
)

func (h *Handler) writeLimitToDatabase(shardID shards.ID, limit btcutil.Amount) (err error) {
	tx, err := database.DB.Begin(context.Background())
	if err != nil {
		return
	}

	// Rollback is safe to call even if the tx is already closed, so if
	// the tx commits successfully, this is a no-op
	defer tx.Rollback(context.Background())

	row := tx.QueryRow(context.Background(),
		"INSERT INTO reserves (shard_id, reserve) VALUES ($1, $2) RETURNING id",
		shardID,
		limit)

	var recordID uint64
	err = row.Scan(&recordID)
	if err != nil {
		err = fmt.Errorf("can't write new reserve limit to the database: %w", err)
		return
	}

	err = tx.Commit(context.Background())
	return
}

func (h *Handler) readLastLimit(shardID shards.ID) (limit Limit, err error) {
	rows, err := database.DB.Query(context.Background(),
		"SELECT reserve, updated FROM reserves WHERE (shard_id = $1) ORDER BY id DESC LIMIT 1",
		shardID)

	if err != nil {
		return
	}

	if rows.Next() {
		err = rows.Scan(&limit.Amount, &limit.Created)
		if err != nil {
			err = fmt.Errorf("can't get last shard limit from database: %w", err)
			return
		}
	}

	return
}

func (h *Handler) writeLimitToCache(shardID shards.ID, limit Limit) {
	h.cachedLimits[shardID] = limit
}

func (h *Handler) writeFeesToDatabase(shards shards.Pair, fees Fee) (err error) {
	tx, err := database.DB.Begin(context.Background())
	if err != nil {
		return
	}

	// Rollback is safe to call even if the tx is already closed, so if
	// the tx commits successfully, this is a no-op
	defer tx.Rollback(context.Background())

	row := tx.QueryRow(context.Background(),
		"INSERT INTO fees (source_shard_id, destination_shard_id, fee_fixed, fee_percents, fee_expire_ttl) "+
			"VALUES ($1, $2, $3, $4, $5) RETURNING id",
		shards.SourceShard,
		shards.DestinationShard,
		fees.Fixed,
		fees.Percents,
		fees.Expired)

	var recordID uint64
	err = row.Scan(&recordID)
	if err != nil {
		err = fmt.Errorf("can't write new fees configuration to the database: %w", err)
		return
	}

	err = tx.Commit(context.Background())
	return
}

func (h *Handler) readLastFeesConfiguration(shards shards.Pair) (fee Fee, err error) {
	rows, err := database.DB.Query(context.Background(),
		"SELECT fee_fixed, fee_percents, fee_expire_ttl FROM fees "+
			"WHERE (source_shard_id = $1 AND destination_shard_id = $2) "+
			"ORDER BY id DESC LIMIT 1",
		shards.SourceShard,
		shards.DestinationShard)

	if err != nil {
		return
	}

	if rows.Next() {
		err = rows.Scan(&fee.Fixed, &fee.Percents, &fee.Expired)
		if err != nil {
			err = fmt.Errorf("can't get last fee configuration from database: %w", err)
			return
		}
	}

	return
}

func (h *Handler) writeFeesToCache(shards shards.Pair, fee Fee) {
	h.cachedFees[shards] = fee
}
