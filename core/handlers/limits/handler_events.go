package limits

import (
	"gitlab.com/jaxnet/hub/ead/core/blockchain/shards"
	"gitlab.com/jaxnet/hub/ead/core/ec"
)

func (h *Handler) notifyFeeSet(shards shards.Pair, fee Fee) {
	event := &EventFeeSet{
		Shards: shards,
		Fee:    fee,
	}

	h.Events.Push(event, ec.ITTL)
}

func (h *Handler) notifyFeeRestored(shards shards.Pair, fee Fee) {
	event := &EventFeeRestored{
		Shards: shards,
		Fee:    fee,
	}

	h.Events.Push(event, ec.ITTL)
}
