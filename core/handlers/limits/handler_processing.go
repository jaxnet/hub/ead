package limits

import (
	"fmt"
	"gitlab.com/jaxnet/hub/ead/core/ec"
	"gitlab.com/jaxnet/hub/ead/core/logger"
	"gitlab.com/jaxnet/hub/ead/core/utils/communication"
	"time"
)

func (h *Handler) processSetLimitRequest(ctx *communication.RequestContext) {
	request := ctx.Context.(*requestSetLimit)
	response := &responseSetLimit{}

	defer func() {
		err := ctx.Responses.Put(response, ec.ITTL)
		if err != nil {
			logger.Log.Err(err).Msg("can't pass set limit response for the processing")
		}
	}()

	if request.limit <= 0 {
		response.err = fmt.Errorf("limit can't be less or equal to zero (%w)", ec.ErrValidationFailed)
		return
	}

	err := h.writeLimitToDatabase(request.shardID, request.limit)
	if err != nil {
		response.err = err
		return
	}

	h.writeLimitToCache(request.shardID, Limit{request.limit, time.Now()})
	return
}

func (h *Handler) processGetLimitRequest(ctx *communication.RequestContext) {
	request := ctx.Context.(*requestGetLimit)
	response := &responseGetLimit{}

	defer func() {
		err := ctx.Responses.Put(response, ec.ITTL)
		if err != nil {
			logger.Log.Err(err).Msg("can't pass get limit response for the processing")
		}
	}()

	limit, isPresent := h.cachedLimits[request.shardID]
	if !isPresent {
		response.err = fmt.Errorf("no shard with exact ID is present (%w)", ec.ErrInvalidShardID)
		return
	}

	response.limit = limit
	return
}

func (h *Handler) processSetFeeRequest(ctx *communication.RequestContext) {
	request := ctx.Context.(*requestSetFee)
	response := &responseSetFee{}

	defer func() {
		err := ctx.Responses.Put(response, ec.ITTL)
		if err != nil {
			logger.Log.Err(err).Msg("can't pass set fee response for the processing")
		}
	}()

	if request.fee.Fixed < 0 {
		response.err = fmt.Errorf("fixed fee can't be less than 0")
		return
	}

	if request.fee.Percents < 0 {
		response.err = fmt.Errorf("percentage fee can't be less than 0")
		return
	}

	if request.fee.Percents > 100 {
		response.err = fmt.Errorf("percentage fee can't be greater than 100")
		return
	}

	err := h.writeFeesToDatabase(request.shards, request.fee)
	if err != nil {
		response.err = err
		return
	}

	h.writeFeesToCache(request.shards, request.fee)
	h.notifyFeeSet(request.shards, request.fee)
	return
}

func (h *Handler) processGetFeeRequest(ctx *communication.RequestContext) {
	request := ctx.Context.(*requestGetFee)
	response := &responseGetFee{}

	defer func() {
		err := ctx.Responses.Put(response, ec.ITTL)
		if err != nil {
			logger.Log.Err(err).Msg("can't pass get fee response for the processing")
		}
	}()

	fee, isPresent := h.cachedFees[request.shards]
	if !isPresent {
		response.err = fmt.Errorf("no fees setfor specififed shards pair (%w)", ec.ErrInvalidShardID)
		return
	}

	response.fee = fee
	return
}
