package limits

import "gitlab.com/jaxnet/hub/ead/core/blockchain/shards"

// EventFeeSet is emitted when fees configuration is set/updated via controlling interface.
// (at the moment it is HTTP admin interface).
type EventFeeSet struct {
	Shards shards.Pair
	Fee    Fee
}

// EventFeeRestored is emitted when fees configuration is restored
// from the database (typically during EAD restart).
type EventFeeRestored struct {
	Shards shards.Pair
	Fee    Fee
}
