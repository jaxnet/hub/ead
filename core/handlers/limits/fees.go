package limits

import (
	"gitlab.com/jaxnet/core/shard.core/btcutil"
	"time"
)

type Fee struct {
	Fixed    btcutil.Amount
	Percents float64
	Expired  time.Time
}
