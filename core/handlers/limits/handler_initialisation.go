package limits

import (
	"gitlab.com/jaxnet/hub/ead/core/blockchain/shards"
	"gitlab.com/jaxnet/hub/ead/core/logger"
	"gitlab.com/jaxnet/hub/ead/core/settings"
	"gitlab.com/jaxnet/hub/ead/core/utils"
)

func (h *Handler) fetchLastFees() (err error) {
	for sourceShardID, _ := range settings.Conf.Blockchain.ShardsMap {
		for destinationShardID, _ := range settings.Conf.Blockchain.ShardsMap {

			if sourceShardID == destinationShardID {
				continue
			}

			index := shards.Pair{
				SourceShard:      sourceShardID,
				DestinationShard: destinationShardID,
			}

			fee, err := h.readLastFeesConfiguration(index)
			if err != nil {
				return err
			}

			if fee.Expired.Before(utils.UTCNow()) {
				logger.Log.Debug().Int64(
					"source-shard-id", int64(index.SourceShard)).Int64(
					"destination-shard-id", int64(index.DestinationShard)).Int64(
					"fee-fixed", int64(fee.Fixed)).Float64(
					"fee-percents", fee.Percents).Msg(
					"Fee is outdated")
				continue
			}

			h.writeFeesToCache(index, fee)
			h.notifyFeeRestored(index, fee)

			if settings.Conf.Debug && (fee.Fixed != 0 || fee.Percents != 0) {
				logger.Log.Debug().Int64(
					"source-shard-id", int64(index.SourceShard)).Int64(
					"destination-shard-id", int64(index.DestinationShard)).Int64(
					"fee-fixed", int64(fee.Fixed)).Float64(
					"fee-percents", fee.Percents).Msg(
					"Restored fees configuration")
			}
		}
	}

	return
}

func (h *Handler) fetchLastLimits() (err error) {
	for shardID, _ := range settings.Conf.Blockchain.ShardsMap {
		limit, err := h.readLastLimit(shardID)
		if err != nil {
			return err
		}

		h.writeLimitToCache(shardID, limit)
		if settings.Conf.Debug && limit.Amount != 0 {
			logger.Log.Debug().Int64(
				"shard-id", int64(shardID)).Int64(
				"reserve", int64(limit.Amount)).Time(
				"created", limit.Created).Msg("Restored shard reserve limit")
		}
	}

	return
}
