package limits

import (
	"gitlab.com/jaxnet/core/shard.core/btcutil"
	"time"
)

type Limit struct {
	Amount  btcutil.Amount
	Created time.Time
}
