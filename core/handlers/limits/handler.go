package limits

import (
	"fmt"
	"gitlab.com/jaxnet/core/shard.core/btcutil"
	"gitlab.com/jaxnet/hub/ead/core/blockchain/shards"
	"gitlab.com/jaxnet/hub/ead/core/ec"
	"gitlab.com/jaxnet/hub/ead/core/logger"
	"gitlab.com/jaxnet/hub/ead/core/settings"
	"gitlab.com/jaxnet/hub/ead/core/utils"
	"gitlab.com/jaxnet/hub/ead/core/utils/communication"
	"time"
)

type Handler struct {
	// Communication channel for transferring events out of handler.
	Events *communication.EventsFlow

	// Internal communication flow for the internal loop.
	flow *communication.Flow

	cachedLimits map[shards.ID]Limit
	cachedFees   map[shards.Pair]Fee
}

func New() (h *Handler, err error) {
	h = &Handler{
		Events:       communication.NewBufferedEventsFlow(16),
		flow:         communication.NewBufferedFlow(16),
		cachedLimits: make(map[shards.ID]Limit),
		cachedFees:   make(map[shards.Pair]Fee),
	}

	err = h.fetchLastLimits()
	if err != nil {
		return
	}

	err = h.fetchLastFees()
	if err != nil {
		return
	}

	return
}

func (h *Handler) Run() (errorsFlow <-chan error) {
	internalErrorsFlow := make(chan error)
	defer func() {
		errorsFlow = internalErrorsFlow
	}()

	go func() {
		const roundWaitTime = time.Second * 5

		for {
			request, err := h.flow.Fetch(roundWaitTime)
			if err != nil {
				// No next request is present.
				// This should not be treated as an error.
				continue
			}

			switch request.Context.(type) {
			case *requestSetLimit:
				h.processSetLimitRequest(request)

			case *requestGetLimit:
				h.processGetLimitRequest(request)

			case *requestSetFee:
				h.processSetFeeRequest(request)

			case *requestGetFee:
				h.processGetFeeRequest(request)

			default:
				internalErrorsFlow <- fmt.Errorf(
					"unexpected request occured during limitis handler flow processing (%w)",
					communication.ErrUnexpectedRequestType)
			}
		}
	}()

	go func() {
		time.Sleep(time.Second * 30)
		for {
			for key, fee := range h.cachedFees {
				if fee.Expired.Before(utils.UTCNow()) {
					logger.Log.Debug().Uint32(
						"source-shard-id", uint32(key.SourceShard)).Uint32(
						"destination-shard-id", uint32(key.DestinationShard)).Int64(
						"fee-fixed", int64(fee.Fixed)).Float64(
						"fee-percents", fee.Percents).Msg(
						"Fee became outdated")
					delete(h.cachedFees, key)
				}
			}
			time.Sleep(time.Minute)
		}
	}()

	return
}

func (h *Handler) SetFundsLimit(shardID shards.ID, limit btcutil.Amount) (err error) {
	req := &requestSetLimit{
		shardID: shardID,
		limit:   limit,
	}

	responses, err := h.flow.Pass(req, ec.ITTL)
	if err != nil {
		err = fmt.Errorf("can't pass set limit request for processing (%w)", err)
		return
	}

	response, err := responses.Fetch(ec.ITTL)
	if err != nil {
		err = fmt.Errorf("can't fetch set limit response (%w)", err)
		return
	}

	resp := response.(*responseSetLimit)

	err = resp.err
	if err == nil && settings.Conf.Debug {
		logger.Log.Debug().Uint32(
			"shard-id", uint32(req.shardID)).Int64(
			"limit", int64(req.limit)).Msg(
			"New reserve set")
	}

	return
}

func (h *Handler) FundsLimit(shardID shards.ID) (limit Limit, err error) {
	req := &requestGetLimit{
		shardID: shardID,
	}

	responses, err := h.flow.Pass(req, ec.ITTL)
	if err != nil {
		err = fmt.Errorf("can't pass get limit request for processing (%w)", err)
		return
	}

	response, err := responses.Fetch(ec.ITTL)
	if err != nil {
		err = fmt.Errorf("can't fetch get limit response (%w)", err)
		return
	}

	resp := response.(*responseGetLimit)
	err = resp.err
	limit = resp.limit
	return
}

func (h *Handler) SetFee(shards shards.Pair, fee Fee) (err error) {
	req := &requestSetFee{
		shards: shards,
		fee:    fee,
	}

	responses, err := h.flow.Pass(req, ec.ITTL)
	if err != nil {
		err = fmt.Errorf("can't pass set fee request for processing (%w)", err)
		return
	}

	response, err := responses.Fetch(ec.ITTL)
	if err != nil {
		err = fmt.Errorf("can't fetch set fee response (%w)", err)
		return
	}

	resp := response.(*responseSetFee)

	err = resp.err
	if err == nil && settings.Conf.Debug {
		logger.Log.Debug().Uint32(
			"source-shard-id", uint32(req.shards.SourceShard)).Uint32(
			"destination-shard-id", uint32(req.shards.DestinationShard)).Int64(
			"fee-fixed", int64(req.fee.Fixed)).Float64(
			"fee-percents", req.fee.Percents).Msg(
			"New fees configuration set")
	}
	return
}

func (h *Handler) Fee(shards shards.Pair) (fee Fee, err error) {
	req := &requestGetFee{
		shards: shards,
	}

	responses, err := h.flow.Pass(req, ec.ITTL)
	if err != nil {
		err = fmt.Errorf("can't pass get fee request for processing (%w)", err)
		return
	}

	response, err := responses.Fetch(ec.ITTL)
	if err != nil {
		err = fmt.Errorf("can't fetch get fee response (%w)", err)
		return
	}

	resp := response.(*responseGetFee)
	err = resp.err
	fee = resp.fee
	return
}
