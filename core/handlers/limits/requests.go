package limits

import (
	"gitlab.com/jaxnet/core/shard.core/btcutil"
	"gitlab.com/jaxnet/hub/ead/core/blockchain/shards"
)

type requestSetLimit struct {
	shardID shards.ID
	limit   btcutil.Amount
}

type responseSetLimit struct {
	err error
}

type requestGetLimit struct {
	shardID shards.ID
}

type responseGetLimit struct {
	limit Limit
	err   error
}

type requestSetFee struct {
	shards shards.Pair
	fee    Fee
}

type responseSetFee struct {
	err error
}

type requestGetFee struct {
	shards shards.Pair
}

type responseGetFee struct {
	fee Fee
	err error
}
