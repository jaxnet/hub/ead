package blocks

import (
	"fmt"
	"gitlab.com/jaxnet/hub/ead/core/blockchain"
	"gitlab.com/jaxnet/hub/ead/core/blockchain/shards"
	"gitlab.com/jaxnet/hub/ead/core/ec"
	"gitlab.com/jaxnet/hub/ead/core/logger"
	"gitlab.com/jaxnet/hub/ead/core/settings"
	"time"
)

type BlockData struct {
	CurrentBlockNumber uint64
	TimeUpdated        time.Time
}

type Handler struct {
	blockchain         *blockchain.Connector
	cachedBlockNumbers map[shards.ID]BlockData
}

func New(chainConnector *blockchain.Connector) (handler *Handler, err error) {

	handler = &Handler{
		blockchain:         chainConnector,
		cachedBlockNumbers: make(map[shards.ID]BlockData),
	}

	beaconShardConnector, err := handler.getShardConnector(0)
	if err != nil {
		return
	}
	beaconCurrentBlockNumber, err := beaconShardConnector.CurrentBlockNumber()
	if err != nil {
		return
	}
	handler.cachedBlockNumbers[0] = BlockData{
		CurrentBlockNumber: beaconCurrentBlockNumber,
		TimeUpdated:        time.Now(),
	}
	for shardID, _ := range settings.Conf.Blockchain.ShardsMap {
		shardConnector, err := handler.getShardConnector(shardID)
		if err != nil {
			return nil, err
		}
		shardCurrentBlockNumber, err := shardConnector.CurrentBlockNumber()
		if err != nil {
			return nil, err
		}
		handler.cachedBlockNumbers[shardID] = BlockData{
			CurrentBlockNumber: shardCurrentBlockNumber,
			TimeUpdated:        time.Now(),
		}
	}

	return
}

func (h *Handler) Run() (flow <-chan error) {
	errorsFlow := make(chan error)

	go func() {
		var err error
		defer ec.ReportErrorInChannelIfAny(err, errorsFlow)

		for {
			h.getActualBlockNumbers()
			time.Sleep(time.Second * 15)
		}
	}()

	return errorsFlow
}

func (h *Handler) GetCurrentBlockNumber(shardID shards.ID) (currentBlockNumber uint64, err error) {
	currentBlockData, isPresent := h.cachedBlockNumbers[shardID]
	if !isPresent {
		// This kind of errors must be prevented on a transactions validation stage,
		// but here the additional check is also present as a runtime checks for cases,
		// when EAD's configuration has been changed on the fly.
		err = fmt.Errorf("shard is not supported by the EAD: %w", ec.ErrInvalidShardID)
		return
	}
	if currentBlockData.TimeUpdated.Add(blockDataTTL()).Before(time.Now()) {
		err = fmt.Errorf("current block data is outdated")
		return
	}
	currentBlockNumber = currentBlockData.CurrentBlockNumber
	return
}

func (h *Handler) getActualBlockNumbers() {
	for shardID, value := range h.cachedBlockNumbers {
		shardConnector, err := h.getShardConnector(shardID)
		if err != nil {
			logger.Log.Error().Err(err).Uint32("shard-id", uint32(shardID)).
				Msg("Blocks handler: can't get shard connector")
			continue
		}
		shardCurrentBlockNumber, err := shardConnector.CurrentBlockNumber()
		if err != nil {
			logger.Log.Error().Err(err).Uint32("shard-id", uint32(shardID)).
				Msg("Blocks handler: can't get current block number")
			continue
		}
		value.CurrentBlockNumber = shardCurrentBlockNumber
		value.TimeUpdated = time.Now()
		h.cachedBlockNumbers[shardID] = value
	}
}

func (h *Handler) getShardConnector(id shards.ID) (
	c *blockchain.ShardConnector, err error) {

	c, isPresent := h.blockchain.ShardsConnectors[id]
	if !isPresent {
		// This kind of errors must be prevented on a transactions validation stage,
		// but here the additional check is also present as a runtime checks for cases,
		// when EAD's configuration has been changed on the fly.
		err = fmt.Errorf("shard is not supported by the EAD: %w", ec.ErrInvalidShardID)
		return
	}

	return
}

func blockDataTTL() (ttl time.Duration) {
	ttl = time.Duration(time.Second * 45)
	return
}
