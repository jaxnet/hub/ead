package agents

import (
	"errors"
	"fmt"
	"gitlab.com/jaxnet/core/shard.core/btcec"
	"gitlab.com/jaxnet/hub/ead/core/blockchain"
	"gitlab.com/jaxnet/hub/ead/core/blockchain/discovery"
	"gitlab.com/jaxnet/hub/ead/core/blockchain/shards"
	"gitlab.com/jaxnet/hub/ead/core/crypto"
	"gitlab.com/jaxnet/hub/ead/core/eal"
	"gitlab.com/jaxnet/hub/ead/core/ec"
	"gitlab.com/jaxnet/hub/ead/core/logger"
	"sync"
	"time"
)

type Handler struct {
	blockchain *blockchain.Connector

	// shard id -> exchange agents list
	// todo: [post PoC] When EAD would be moved to signal/slot pattern -
	//		 this map can be updated back to regular one (for performance and code complexity reasons).
	exchangeAgents sync.Map

	// Shard ID -> Exchange Agent Registration ID
	// Contains ID os the shards in which current exchange agent has a registration.
	// This field is expected to be updated each time when exchange agents list has been updated.
	// todo: [post PoC] When EAD would be moved to signal/slot pattern -
	//		 this map can be updated back to regular one (for performance and code complexity reasons).
	shardsIDsWhereCurrentEAIsPresent sync.Map

	outEventsAgentsConnectionInfoChanged chan []*discovery.DiscoveryRecord
}

func NewEALHandler(connector *blockchain.Connector) (handler *Handler, err error) {
	ec.InterruptOnNilArgument(connector)

	handler = &Handler{
		blockchain: connector,

		//Note: exchangeAgents is initialized in fetchAndApplyExchangeAgentsListChanges().
		//No need to be redundantly initialized here.

		// Note: shardsIDsWhereCurrentEAIsPresent is initialized in fetchAndApplyExchangeAgentsListChanges().
		// No need to be redundantly initialized here.

		outEventsAgentsConnectionInfoChanged: make(chan []*discovery.DiscoveryRecord, 16),
	}

	return
}

func (handler *Handler) Run() (flow <-chan error) {
	errorsFlow := make(chan error)

	go func() {
		for {
			err := handler.fetchAndApplyExchangeAgentsListChanges()
			if err != nil {
				logger.Log.Error().Err(err)
				errorsFlow <- err
				return
			}

			// todo : increase this delay to one day
			time.Sleep(blockchain.ShardBlockGenerationTimeout)
		}
	}()

	return errorsFlow
}

func (handler *Handler) OnEALChanged() <-chan []*discovery.DiscoveryRecord {
	return handler.outEventsAgentsConnectionInfoChanged
}

func (handler *Handler) CurrentEAIsPresentInShard(shard shards.ID) bool {
	_, isPresent := handler.shardsIDsWhereCurrentEAIsPresent.Load(shard)
	if !isPresent {
		return false
	}

	return true
}

func (handler *Handler) EAIsPresentInShard(shard shards.ID, eaRegID uint64) bool {
	easList, isPresent := handler.exchangeAgents.Load(shard)
	if !isPresent {
		return false
	}

	eaInfo, isPresent := easList.(*eal.ExchangeAgentsList).ExchangeAgents[eaRegID]
	if !isPresent {
		return false
	}

	if eaInfo.ID != eaRegID {
		return false
	}

	return true
}

func (handler *Handler) EASigIsValid(shard shards.ID, eaRegID uint64, sig *btcec.Signature, data []byte) bool {
	easList, isPresent := handler.exchangeAgents.Load(shard)
	if !isPresent {
		return false
	}

	eaInfo, isPresent := easList.(*eal.ExchangeAgentsList).ExchangeAgents[eaRegID]
	if !isPresent {
		return false
	}

	return crypto.VerifyExternalPubKey(data, sig, eaInfo.PubKey)
}

func (handler *Handler) RegNumberInShard(id shards.ID) (regNumber uint64, err error) {
	n, isPresent := handler.shardsIDsWhereCurrentEAIsPresent.Load(id)
	if !isPresent {
		err = errors.New("current exchange agent has no registration in shard specified")
		return
	}

	regNumber = n.(uint64)
	return
}

func (handler *Handler) fetchAndApplyExchangeAgentsListChanges() (err error) {

	// WARN:
	// In real-world blockchain the agent could (and probably would)
	// be registered MORE than once: there is no way to update information,
	// so the new registration is needed and partial update is also possible.
	//
	// This method MUST ensure that it has processed THE LAST ONE record,
	// (potentially collected from the parts of other registration transactions in case is partial update is allowed)
	//
	// todo: [post PoC] Implement proper registration logic.
	// todo: [post PoC] Implement / Improve tests for EAD registration discovery.

	// Amount of shards in which current EAD is registered.
	totalShardsRegistered := 0

	dropPreviousContext := func() {
		handler.exchangeAgents.Range(func(key, value interface{}) bool {
			handler.exchangeAgents.Delete(key)
			return true
		})

		handler.shardsIDsWhereCurrentEAIsPresent.Range(func(key, value interface{}) bool {
			handler.shardsIDsWhereCurrentEAIsPresent.Delete(key)
			return true
		})
	}

	updateContext := func() {
		eaRegRecords, err := handler.blockchain.GetExchangeAgentsRegistrationRecords()
		if err != nil {
			return
		}

		for _, record := range eaRegRecords {
			accessInfoRecord := &eal.ExchangeAgentInfo{
				ID:        record.SeqNumber,
				Addresses: record.Addresses,
				PubKey:    record.PubKey,
			}

			for _, operatingShards := range record.Addresses {
				for _, shardID := range operatingShards {
					agentsList, isPresent := handler.exchangeAgents.Load(shardID)
					if !isPresent {
						agentsList = eal.NewExchangeAgentsList()
						handler.exchangeAgents.Store(shardID, agentsList)
					}

					agentsList.(*eal.ExchangeAgentsList).ExchangeAgents[record.SeqNumber] = accessInfoRecord

					if crypto.PubKeyMatchOurOwn(record.PubKey) {
						handler.shardsIDsWhereCurrentEAIsPresent.Store(shardID, record.SeqNumber)
						totalShardsRegistered++
					}
				}
			}
		}

		if totalShardsRegistered > 0 {
			handler.outEventsAgentsConnectionInfoChanged <- eaRegRecords

		} else {
			err = fmt.Errorf("EAD is not registered in any shard: %w", err)

			// todo: [post PoC]: Provide further instructions.
			// 		 It would be nice to provide user with some instruction here what to do next to solve this problem,
			// 		 how to register in the beacon chain and how long to wait until next try.
			return
		}
	}

	dropPreviousContext()
	updateContext()
	return
}
