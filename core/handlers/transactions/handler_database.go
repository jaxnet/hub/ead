package transactions

import (
	"fmt"
	"gitlab.com/jaxnet/core/shard.core/btcutil"
	"gitlab.com/jaxnet/hub/ead/core/blockchain/shards"
	"gitlab.com/jaxnet/hub/ead/core/database"
	"gitlab.com/jaxnet/hub/ead/core/handlers/transactions/transactions"
	"time"
)

// totalFundsFrozen returns amount of funds that are frozen by the operations, that are not finished yet.
func (h *Handler) totalFundsFrozen(shardID shards.ID) (amount btcutil.Amount, err error) {

	// todo: think about inclusion of fees here too.

	// LastTXState is expected to store the ID of the last state,
	// in which transaction could fall being normally processed.
	//
	// AND!
	//
	// It is also expected, that all other states IDs AFTER this one
	// are indicating the final state of the transaction, but with error (error code),
	// and no other intermediary states are possible in range (LastState .. 255)
	row := database.QueryRow(
		"SELECT coalesce(sum(destination_amount), 0) "+
			"FROM transactions "+
			"WHERE destination_shard_id = $1 AND state < $2;",
		shardID, transactions.LastTXState)

	err = row.Scan(&amount)
	if err != nil {
		err = fmt.Errorf("can't calculate total amount of non-finished transactions: %w", err)
		return
	}

	return
}

// totalFundsIssuedFromShard returns total amount of funds
// that has been moved from the shard for all the time.
func (h *Handler) totalFundsIssuedFromShard(shardID shards.ID, reserveCreated time.Time) (
	amount btcutil.Amount, err error) {

	// todo: think about inclusion of fees here too.

	// LastTXState is expected to store the ID of the last state,
	// in which transaction could fall being normally processed.
	//
	// AND!
	//
	// It is also expected, that all other states IDs AFTER this one
	// are indicating the final state of the transaction, but with error (error code),
	// and no other intermediary states are possible in range (LastState .. 255)
	row := database.QueryRow(
		"SELECT coalesce(sum(destination_amount), 0) "+
			"FROM transactions "+
			"WHERE destination_shard_id = $1 AND updated > $2 AND state = $3;", // WARN: Exact state here! (only successful operations)
		shardID, reserveCreated, transactions.LastTXState)

	err = row.Scan(&amount)
	if err != nil {
		err = fmt.Errorf("can't calculate total amount of funds issued from shard: %w", err)
		return
	}

	return
}

// totalFundsAcceptedIntoShard returns total amount of funds
// that has been received by the shard for all the time.
func (h *Handler) totalFundsAcceptedIntoShard(shardID shards.ID, reserveCreated time.Time) (
	amount btcutil.Amount, err error) {

	// todo: think about inclusion of fees here too.

	// LastTXState is expected to store the ID of the last state,
	// in which transaction could fall being normally processed.
	//
	// AND!
	//
	// It is also expected, that all other states IDs AFTER this one
	// are indicating the final state of the transaction, but with error (error code),
	// and no other intermediary states are possible in range (LastState .. 255)
	row := database.QueryRow(
		"SELECT coalesce(sum(source_amount), 0) "+
			"FROM transactions "+
			"WHERE source_shard_id = $1 AND updated > $2 AND state = $3;", // WARN: Exact state here! (only successful operations)
		shardID, reserveCreated, transactions.LastTXState)

	err = row.Scan(&amount)
	if err != nil {
		err = fmt.Errorf("can't calculate total amount of funds issued from shard: %w", err)
		return
	}

	return
}
