package transactions

import (
	"encoding/json"
	"errors"
	"fmt"
	"gitlab.com/jaxnet/core/shard.core/btcutil"
	"gitlab.com/jaxnet/hub/ead/core/blockchain"
	"gitlab.com/jaxnet/hub/ead/core/blockchain/shards"
	"gitlab.com/jaxnet/hub/ead/core/ec"
	"gitlab.com/jaxnet/hub/ead/core/handlers/blocks"
	"gitlab.com/jaxnet/hub/ead/core/handlers/limits"
	"gitlab.com/jaxnet/hub/ead/core/handlers/transactions/transactions"
	"gitlab.com/jaxnet/hub/ead/core/logger"
	"gitlab.com/jaxnet/hub/ead/core/messages/clients_messages"
	"gitlab.com/jaxnet/hub/ead/core/utils/communication"
	"io/ioutil"
	"math/big"
	"net/http"
	"sync"
	"time"
)

var (
	ErrAmountLessThanMinimalTransferable = errors.New("passed amount is less than minimally transferable amount at this moment")
)

var (
	zero = &big.Int{}
)

type txBoundError struct {
	tx  *transactions.Transaction
	err error
}

type txBoundUpdate struct {
	tx *transactions.Transaction
}

// todo: move to separate file
type txSet struct {
	// Client funds locking group.
	initialised       map[uint64]*transactions.Transaction
	clientFundsLocked map[uint64]*transactions.Transaction

	// EAD funds locking group.
	eadLockMultiSigCreated  map[uint64]*transactions.Transaction
	eadFundsLockingTxIssued map[uint64]*transactions.Transaction
	eadFundsLockingTxMined  map[uint64]*transactions.Transaction

	// Cross shard TX group
	crossShardTxPrepared map[uint64]*transactions.Transaction
	crossShardTxIssued   map[uint64]*transactions.Transaction

	// Refunding lock group
	lockFundsShouldBeReturned map[uint64]*transactions.Transaction
}

func (set *txSet) Count() int {
	return len(set.initialised) +
		len(set.clientFundsLocked) +
		len(set.eadLockMultiSigCreated) +
		len(set.eadFundsLockingTxIssued) +
		len(set.eadFundsLockingTxMined) +
		len(set.crossShardTxPrepared) +
		len(set.crossShardTxIssued) +
		len(set.lockFundsShouldBeReturned)
}

func (set *txSet) ForEach(handler func(tx *transactions.Transaction)) {
	for _, tx := range set.initialised {
		handler(tx)
	}

	for _, tx := range set.clientFundsLocked {
		handler(tx)
	}

	for _, tx := range set.eadLockMultiSigCreated {
		handler(tx)
	}

	for _, tx := range set.eadFundsLockingTxIssued {
		handler(tx)
	}

	for _, tx := range set.eadFundsLockingTxMined {
		handler(tx)
	}

	for _, tx := range set.crossShardTxPrepared {
		handler(tx)
	}

	for _, tx := range set.crossShardTxIssued {
		handler(tx)
	}

	for _, tx := range set.lockFundsShouldBeReturned {
		handler(tx)
	}
}

// todo: move reserves processing here
// todo: ensure schema on the startup
type Handler struct {
	Flow *communication.Flow

	limitsHandler *limits.Handler
	blockchain    *blockchain.Connector
	blocksHandler *blocks.Handler
}

func New(limitsHandler *limits.Handler, chainConnector *blockchain.Connector, blocksHandler *blocks.Handler) (
	handler *Handler, err error) {

	handler = &Handler{
		Flow:          communication.NewBufferedFlow(16),
		limitsHandler: limitsHandler,
		blockchain:    chainConnector,
		blocksHandler: blocksHandler,
	}

	return
}

func (h *Handler) Run() (flow <-chan error) {
	errorsFlow := make(chan error)

	go func() {
		var err error
		defer ec.ReportErrorInChannelIfAny(err, errorsFlow)

		logger.Log.Info().Msg("Transactions processing has been started")
		defer func() {
			logger.Log.Info().Msg("Transactions processing has been finished")
		}()

		for {
			err = h.processTransactions()
			if err != nil {
				logger.Log.Err(err).Msg(
					"Error occurred during processing of internal transactions handler loop " +
						"(process transactions section). Processing has not been stopped.")
			}

			err = h.processFlow()
			if err != nil {
				logger.Log.Err(err).Msg(
					"Error occurred during processing of internal transactions handler loop " +
						"(process flow section). Processing has not been stopped.")
			}

			// todo: add exit point to this method
		}
	}()

	return errorsFlow
}

func (h *Handler) processTransactions() (err error) {
	// todo: [scale point] [data race] [tx collisions possibility]
	// 		 In case if there are more than one EAD launched for txs processing,
	//		 this mechanism would fail, because only one EAD at a time would set the flag up,
	//		 and the rest would not react in any way.
	//       The other problem is a race condition between EADs, that must be eliminated,
	//		 or there is a serious risk for txs collisions.

	txs, err := h.fetchPendingTransactions()
	if err != nil {
		return
	}

	if txs.Count() == 0 {
		// No more pending txs.
		return
	}

	// Some txs could fail during processing.
	// To collect this failures and not to try to save the state of the failed txs,
	// but still t be able to process txs in parallel - tx->errors mapping is used.
	// By default it is expected, that every transaction could fail (worst case).
	txBoundErrors := make(chan txBoundError, txs.Count())

	// Some txs would change their data during the execution.
	// This channel collects this type of transactions for further updates on the DB level.
	txBoundUpdates := make(chan txBoundUpdate, txs.Count())

	// Wait group must be initialized to wait until ALL txs would be completed.
	wg := sync.WaitGroup{}
	wg.Add(txs.Count())

	h.processTransactionsAsynchronously(txs, &wg, txBoundErrors, txBoundUpdates)

	// Wait until all txs would be processed.
	// Each transaction is performed asynchronously.
	wg.Wait()

	// In case if there are any errors - all of them must be reported,
	// But NO txs processing interruption must happen,
	// because some txs (that are executed correctly)
	// have changed their states, and need to be written to the disk.
	failedTransactions := make(map[uint64]error)
	for {
		if len(txBoundErrors) == 0 {
			break
		}

		txErr := <-txBoundErrors
		failedTransactions[txErr.tx.ID] = txErr.err
	}

	updatedTransactions := make(map[uint64]*transactions.Transaction)
	for {
		if len(txBoundUpdates) == 0 {
			break
		}

		txBoundUpdate := <-txBoundUpdates
		updatedTransactions[txBoundUpdate.tx.ID] = txBoundUpdate.tx
	}

	h.updateProcessedTransactionsExceptFailed(txs, failedTransactions, updatedTransactions)

	// There are txs that has been processed and potentially moved to another stage.
	//h.flagDBShouldBeRescanned.Up()
	return
}

func (h *Handler) processFlow() (err error) {
	timeStart := time.Now()
	for {
		request, err := h.Flow.Fetch(time.Second * 1)
		if err != nil {
			// No next request is present.
			// This should bot be reported as error to the outer scope.
			return nil
		}

		switch request.Context.(type) {

		case *clients_messages.RequestInitExchange:
			err := h.processInitExchangeRequest(request)
			if err != nil {
				return err
			}

		case *clients_messages.RequestClientFundsLockTx:
			err = h.processClientLockFundsRequest(request)
			if err != nil {
				return err
			}

		case *clients_messages.RequestFetchCrossShardTx:
			err = h.processFetchCrossShardRequest(request)
			if err != nil {
				return err
			}

		default:
			return communication.ErrUnexpectedRequestType
		}
		// stop loop after 5s
		if time.Now().After(timeStart.Add(time.Second * 5)) {
			break
		}
	}
	return
}

func (h *Handler) AddressBalance(shardID shards.ID, address string) (balance btcutil.Amount, err error) {
	shardConnector, err := h.getShardConnector(shardID)
	if err != nil {
		return
	}
	requestUrl := fmt.Sprintf("%s/api/v1/indexes/utxos/balance/?address=%s",
		shardConnector.UTXOIndexerInterface, address)

	resp, err := http.Get(requestUrl)
	if err != nil {
		return
	}
	if resp.Status != "200 OK" {
		err = errors.New("Wrong response from utxo indexer")
		return
	}

	bodyBytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return
	}

	type BalanceResponse struct {
		Balance int64 `json:"balance"`
	}
	var balanceResponse BalanceResponse
	err = json.Unmarshal(bodyBytes, &balanceResponse)
	if err != nil {
		return
	}
	balance = btcutil.Amount(balanceResponse.Balance)
	return
}
