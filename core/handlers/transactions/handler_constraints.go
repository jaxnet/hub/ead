package transactions

import (
	"gitlab.com/jaxnet/core/shard.core/btcutil"
	"gitlab.com/jaxnet/core/shard.core/types/chaincfg"
	"gitlab.com/jaxnet/hub/ead/core/crypto"
	"math/big"
)

// Returns minimal amount in a source shard that could be transferred to the destination shard.
// This function prevents  attempts to send (create transactions, that could not be executed) of a very small amounts, that are even smaller than miners fee.
func (h *Handler) minimalTransferableAmount() (amount *big.Int) {
	// todo: fetch current miners fee, add 5%.

	// WARN: Mock implementation here.
	amount = &big.Int{}
	amount, _ = amount.SetString("1", 10)
	return
}

// Returns the address which holds the funds that was collected from the clients_messages during the exchange operations.
func (h *Handler) incomingFundsDestinationAddress() (address btcutil.Address, err error) {
	serializedPubKey := crypto.PubKey().SerializeCompressed()

	// todo: &chaincfg.MainNetParams identifies the bitcoin network.
	//		 We must redeclare them for our own.
	address, err = btcutil.NewAddressPubKey(serializedPubKey, &chaincfg.MainNetParams)
	return
}

// Returns the amount of the funds that could be sent to the destination shard.
func (h *Handler) calculateDestinationAmount(sourceAmount *big.Int) (amount *big.Int, err error) {
	// todo: destination amount must be calculated taking into account he final fees.
	return sourceAmount, nil
}
