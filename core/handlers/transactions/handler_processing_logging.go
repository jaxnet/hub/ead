package transactions

import (
	"github.com/rs/zerolog"
	"gitlab.com/jaxnet/hub/ead/core/handlers/transactions/transactions"
	"gitlab.com/jaxnet/hub/ead/core/logger"
)

func LogErr(tx *transactions.Transaction, err error) *zerolog.Event {
	return logger.Log.Err(err).Uint64("tx-id", tx.ID)
}

func LogInfo(tx *transactions.Transaction) *zerolog.Event {
	return logger.Log.Info().Uint64("tx-id", tx.ID)
}

func LogDebug(tx *transactions.Transaction) *zerolog.Event {
	return logger.Log.Debug().Uint64("tx-id", tx.ID)
}
