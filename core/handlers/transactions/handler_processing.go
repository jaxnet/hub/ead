package transactions

import "C"
import (
	"bytes"
	"context"
	"encoding/hex"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/google/uuid"
	"gitlab.com/jaxnet/core/shard.core/btcutil"
	"gitlab.com/jaxnet/core/shard.core/cmd/tx-gatling/txutils"
	"gitlab.com/jaxnet/core/shard.core/types/chainhash"
	"gitlab.com/jaxnet/core/shard.core/types/wire"
	"gitlab.com/jaxnet/hub/ead/core/blockchain"
	"gitlab.com/jaxnet/hub/ead/core/blockchain/shards"
	"gitlab.com/jaxnet/hub/ead/core/crypto"
	"gitlab.com/jaxnet/hub/ead/core/database"
	"gitlab.com/jaxnet/hub/ead/core/ec"
	txs "gitlab.com/jaxnet/hub/ead/core/handlers/transactions/transactions"
	"gitlab.com/jaxnet/hub/ead/core/logger"
	"gitlab.com/jaxnet/hub/ead/core/settings"
	"io/ioutil"
	"net/http"
	"net/url"
	"strconv"
	"sync"
	"time"
)

var (
	// todo: move it to the errors package
	ErrInvalidTransaction           = errors.New("invalid internal transaction")
	ErrInvalidTransactionState      = errors.New("invalid transaction state")
	ErrInvalidBlockchainTransaction = errors.New("blockchain transaction does not pass validation")
	ErrRuntimeError                 = errors.New("runtime error")
)

// todo: move this method to the database package.
//
// initExchangeTransaction creates new transaction-record in the database.
// This method is expected to not to be used directly,
// but through the init2SigExchangeTransaction or init3SigExchangeTransaction methods.
func (h *Handler) initExchangeTransaction(index shards.Pair, sourceAmount, destinationAmount btcutil.Amount,
	destinationShardDestinationAddress btcutil.AddressPubKeyHash, schema int,
	senderPubKey, receiverPubKey []byte) (uid uuid.UUID, sourceShardLockWindow uint64,
	destinationShardLockWindow uint64, err error) {

	limitReached, err := h.isFundsLimitReached(index, destinationAmount)
	if err != nil {
		return
	}

	if limitReached {
		msg := "cant start new exchange transaction: no funds left in source shard (%w)"
		err = fmt.Errorf(msg, ec.ErrShardLimitReached)
		return
	}

	// todo: think about fees in destination shard.

	// It is assumed, that EAD operates with the same source address (pub key) in each shard.
	// So the source address for all operations is the same - current public key address.
	sourceShardDestinationAddress, err := crypto.Address()
	if err != nil {
		return
	}

	// todo: There is a very-vary small probability of the UUID collision.
	//       It is not a great problem on this stage, but ideally it must be covered.
	//		 PostgreSQL would report a unique-index error (cause schema has a corresponding check),
	//		 so the only thing that is must to be done is to check the corresponding error
	//		 after the first insert attempt and try once more.

	uid, err = uuid.NewRandom()
	if err != nil {
		return
	}

	senderPubKeyHexOrNULL := "NULL"
	if senderPubKey != nil {
		hexRepresentation := hex.EncodeToString(senderPubKey)
		senderPubKeyHexOrNULL = fmt.Sprint("decode('", hexRepresentation, "', 'hex')")

	}

	receiverPubKeyHexOrNULL := "NULL"
	if receiverPubKey != nil {
		hexRepresentation := hex.EncodeToString(receiverPubKey)
		receiverPubKeyHexOrNULL = fmt.Sprint("decode('", hexRepresentation, "', 'hex')")
	}

	sourceShardLockWindow, sourceShardMaxBlockHeight, destinationShardLockWindow,
		destinationShardMaxBlockHeight, maxBlockHeightForCstxSigning, err :=
		h.getLockWindowValues(index.SourceShard, index.DestinationShard)
	if err != nil {
		return
	}

	// Transaction body and initial transaction state must be created atomically.
	dbTX, err := database.DB.Begin(context.Background())
	if err != nil {
		return
	}

	// Rollback is safe to call even if the tx is already closed, so if
	// the tx commits successfully, this is a no-op
	defer dbTX.Rollback(context.Background())

	query := fmt.Sprint(
		"INSERT INTO transactions (",
		"uuid, ",
		"source_amount, ",
		"destination_amount, ",
		"source_shard_id, ",
		"destination_shard_id, ",
		"source_shard_destination_address, ",
		"destination_shard_destination_address, ",
		"op_schema, ",
		"client_pub_key, ",
		"receiver_pub_key, ",
		"source_shard_lock_window, ",
		"source_shard_max_block_height, ",
		"destination_shard_lock_window, ",
		"destination_shard_max_block_height, ",
		"max_block_height_for_cstx_signing) ",
		"VALUES (",

		fmt.Sprint("'", uid.String(), "'"), ", ",
		int64(sourceAmount), ", ",
		int64(destinationAmount), ", ",
		index.SourceShard, ", ",
		index.DestinationShard, ", ",
		fmt.Sprint("'", sourceShardDestinationAddress.String(), "'"), ", ",
		fmt.Sprint("'", destinationShardDestinationAddress.String(), "'"), ", ",
		schema, ", ",
		senderPubKeyHexOrNULL, ", ",
		receiverPubKeyHexOrNULL, ", ",
		sourceShardLockWindow, ", ",
		sourceShardMaxBlockHeight, ", ",
		destinationShardLockWindow, ", ",
		destinationShardMaxBlockHeight, ", ",
		maxBlockHeightForCstxSigning,
		") ",
		"RETURNING id")

	row := dbTX.QueryRow(context.Background(), query)

	var txID uint64
	err = row.Scan(&txID)
	if err != nil {

		// todo: insufficient reserve is reported here.
		// 		 it must be checked and processed properly,
		//		 otherwise clients_messages would not be able to know what exactly has happened.
		return
	}

	err = dbTX.Commit(context.Background())
	if err != nil {
		return
	}

	return
}

func (h *Handler) init2SigExchangeTransaction(index shards.Pair, sourceAmount, destinationAmount btcutil.Amount,
	destinationShardDestinationAddress btcutil.AddressPubKeyHash, senderPubKey []byte) (
	uid uuid.UUID, sourceShardLockWindow uint64, destinationShardLockWindow uint64, err error) {
	return h.initExchangeTransaction(
		index, sourceAmount, destinationAmount,
		destinationShardDestinationAddress, txs.SchemaSenderEADSender,
		senderPubKey, nil)
}

func (h *Handler) init3SigExchangeTransaction(index shards.Pair, sourceAmount, destinationAmount btcutil.Amount,
	destinationShardDestinationAddress btcutil.AddressPubKeyHash, senderPubKey, receiverPubKey []byte) (
	uid uuid.UUID, sourceShardLockWindow uint64, destinationLockWindow uint64, err error) {
	return h.initExchangeTransaction(
		index, sourceAmount, destinationAmount,
		destinationShardDestinationAddress, txs.SchemaSenderEADReceiver,
		senderPubKey, receiverPubKey)
}

func (h *Handler) fetchPendingTransactions() (transactions txSet, err error) {
	transactions = txSet{}
	filter := "(delayed_until < now() OR delayed_until IS NULL)"

	transactions.initialised, err =
		database.FetchTransactions(fmt.Sprint(filter, " AND state=", txs.Initialised))
	if err != nil {
		return
	}

	transactions.clientFundsLocked, err =
		database.FetchTransactions(fmt.Sprint(filter, " AND state=", txs.ClientFundsLocked))
	if err != nil {
		return
	}

	transactions.eadLockMultiSigCreated, err =
		database.FetchTransactions(fmt.Sprint(filter, " AND state=", txs.EADLockMultiSigCreated))
	if err != nil {
		return
	}

	transactions.eadFundsLockingTxIssued, err =
		database.FetchTransactions(fmt.Sprint(filter, " AND state=", txs.EAFundsLockingTxIssued))
	if err != nil {
		return
	}

	transactions.eadFundsLockingTxMined, err =
		database.FetchTransactions(fmt.Sprint(filter, " AND state=", txs.EAFundsLockingTXMined))
	if err != nil {
		return
	}

	transactions.crossShardTxPrepared, err =
		database.FetchTransactions(fmt.Sprint(filter, " AND state=", txs.CrossShardTxPrepared))
	if err != nil {
		return
	}

	transactions.lockFundsShouldBeReturned, err =
		database.FetchTransactions(fmt.Sprint(filter, " AND state=", txs.LockFundsReturning))
	if err != nil {
		return
	}

	// todo: cut this code
	//transactions.crossShardTxIssued, err =
	//	database.FetchTransactions(fmt.Sprint(filter, " AND state=", txs.CrossShardTxIssued))
	//if err != nil {
	//	return
	//}

	return
}

func (h *Handler) processTransactionsAsynchronously(
	transactions txSet, wg *sync.WaitGroup,
	errors chan txBoundError, updates chan txBoundUpdate) {
	// todo: add handling of the transactions that has crossed deadline (reject with error).

	// Client funds locking group.
	h.processInitialisedTXsAsynchronously(
		transactions.initialised, wg, errors, updates)

	h.processClientFundsLockedTXsAsynchronously(
		transactions.clientFundsLocked, wg, errors, updates)

	// EAD funds locking TXs group.
	h.processEAFundsLockingTxPreparedTXsAsynchronously(
		transactions.eadLockMultiSigCreated, wg, errors, updates)

	h.processEAFundsLockingTxIssuedTXsAsynchronously(
		transactions.eadFundsLockingTxIssued, wg, errors, updates)

	h.processEADFundsLockingTxMinedTXsAsynchronously(
		transactions.eadFundsLockingTxMined, wg, errors, updates)

	// Cross shard TXs group.
	h.processCrossShardTXsAsynchronously(
		transactions.crossShardTxPrepared, wg, errors, updates)

	// Refunding lock group
	h.processLockFundsRefundingAsynchronously(
		transactions.lockFundsShouldBeReturned, wg, errors, updates)

	// todo: remove me
	//h.processCrossShardTXsIssuedAsynchronously(
	//	transactions.crossShardTxIssued, wg, errors, updates)
}

func (h *Handler) updateProcessedTransactionsExceptFailed(
	transactions txSet, failedTransactions map[uint64]error, updatedTransactions map[uint64]*txs.Transaction) {

	trySaveTxIfSuccessAndHasUpdate := func(tx *txs.Transaction) {
		_, txHasFailed := failedTransactions[tx.ID]
		_, txHasUpdate := updatedTransactions[tx.ID]

		///Testing:Injection:ForbidUpdate

		if txHasFailed || txHasUpdate {
			err := h.updateTransaction(tx)
			if err != nil {
				logger.Log.Err(err).Msg("Can't save new state of the transaction")
			}
		}
	}

	transactions.ForEach(trySaveTxIfSuccessAndHasUpdate)
}

// todo: comment each one case when transaction is postponed (why?)

//func (h *Handler) processFailedTransactions(transactions txSet, failedTransactions map[uint64]error) {
//	const (
//		defaultPostponeTimeout = time.Second * 20
//		maxPostponeAttemptsCount = 10
//	)
//
//	rejectTx := func(tx *Transaction) {
//
//	}
//
//	tryPostponeTx := func(tx *Transaction, timeout time.Duration) {
//		// todo: check how many times transaction has been postponed in the past.
//		//		 if more than max postpone attempts - reject with error.
//	}
//}
func (h *Handler) processInitialisedTXsAsynchronously(
	transactions map[uint64]*txs.Transaction, wg *sync.WaitGroup,
	errorsFlow chan txBoundError, updates chan txBoundUpdate) {

	processTx := func(tx *txs.Transaction) (err error) {
		// todo: postpone, txError, dataUpdated and checking txState could be moved to common functor structure.
		// 		 this would make code shorter and a bit easier to maintain.
		dataUpdated := func() (err error) {
			updates <- txBoundUpdate{tx: tx}
			return nil
		}

		txError := func(err error) (e error) {
			return h.txBoundError(tx, errorsFlow, err)
		}

		postpone := func() (err error) {
			const postponeIntervalSeconds = 5
			h.postponeTransaction(tx, postponeIntervalSeconds)
			return dataUpdated()

			// todo: check how many times transaction has been postponed in the past.
			// 		 if more than max postpone attempts - reject with error.
		}

		txElapsed := func(tx *txs.Transaction) (isElapsed bool, err error) {
			currentBlockNumber, err := h.blocksHandler.GetCurrentBlockNumber(tx.SourceShardID)
			if err != nil {
				return
			}

			if currentBlockNumber > tx.MaxBlockHeightForCstxSigning {
				isElapsed = true
			}
			return
		}

		checkLockAmountAndDestinationFunc := func(shardConnector *blockchain.ShardConnector, lockTxHash *chainhash.Hash,
			expectedAmount btcutil.Amount, expectedAddress string) (check bool, err error) {
			transaction, err := shardConnector.GetRawTransaction(lockTxHash)
			if err != nil {
				return false, err
			}
			check = transaction.MsgTx().TxOut[0].Value == int64(expectedAmount)
			if !check {
				return
			}

			decodedOutput, err := txutils.DecodeScript(transaction.MsgTx().TxOut[0].PkScript, settings.NetParams)
			if err != nil {
				return
			}

			check = decodedOutput.Addresses[0] == expectedAddress
			return
		}

		checkIfLockDidntUseYetFunc := func(shardConnector *blockchain.ShardConnector, lockTxHash *chainhash.Hash,
			multiSigAddress string) (isUsed bool, err error) {

			requestUrl := fmt.Sprintf("%s/api/v1/indexes/addresses/transactions/with-input/?address=%s&tx_input=%s&tx_input_idx=%d",
				shardConnector.AddressesTxsIndexerInterface, multiSigAddress, lockTxHash.String(), 0)

			resp, err := http.Get(requestUrl)
			if err != nil {
				return
			}
			if resp.Status != "200 OK" {
				err = errors.New("Wrong response from addresses txs indexer")
				return
			}

			bodyBytes, err := ioutil.ReadAll(resp.Body)
			if err != nil {
				return
			}

			var addressesTxs []string
			err = json.Unmarshal(bodyBytes, &addressesTxs)
			if err != nil {
				return
			}

			isUsed = len(addressesTxs) > 0

			return
		}

		if tx.State != txs.Initialised {
			err = fmt.Errorf("expected TX state is 'Initialised': %w", ErrInvalidTransactionState)
			return txError(err)
		}

		isTxElapsed, err := txElapsed(tx)
		if err != nil {
			return txError(err)
		}
		if isTxElapsed {
			err = fmt.Errorf("Tx become elapsed: %w", ErrInvalidTransaction)
			return txError(err)
		}

		if tx.ClientFundLockTxHash == nil {
			return postpone()
		}

		txHash, err := chainhash.NewHash(tx.ClientFundLockTxHash)
		if err != nil {
			return txError(fmt.Errorf(
				"can't decode client funds lock tx hash: %v", ErrInvalidTransaction))
		}

		sourceShard, err := h.getSourceShardConnector(tx)
		if err != nil {
			return txError(err)
		}

		if tx.ClientFundLockTx == nil {
			blockchainTx, err := sourceShard.GetRawTransaction(txHash)
			if err != nil {
				// todo: test if transaction would be automatically closed by the deadline handler.
				// 		 otherwise it must be closed with the error here.
				return postpone()
			}

			if !blockchainTx.Hash().IsEqual(txHash) {
				return txError(fmt.Errorf(
					"hash of the received blockchain tx does not correspond "+
						"to the expected one (received from the client): %v", ErrInvalidBlockchainTransaction))
			}

			// todo: [security] https://jaxnetwork.atlassian.net/browse/HUB-49
			// 	 	 Ensure EAD validates Sender->EAD funds locking TX before going to lock it's own funds.

			blockchainTxBinary := &bytes.Buffer{}
			err = blockchainTx.MsgTx().Serialize(blockchainTxBinary)
			if err != nil {
				return txError(fmt.Errorf(
					"can't encode fetched blockchain tx into binary form: %v", ErrRuntimeError))
			}

			// All is OK.
			// Blockchain transaction has passed the validation.
			tx.ClientFundLockTx = blockchainTxBinary.Bytes()
		}

		_, isMined, err := sourceShard.IsTxMined(txHash, ec.LockTxConfirmationsExpected, true)
		if err != nil {
			logger.Log.Err(err).Uint64(
				"txID", tx.ID).Msg(
				"Can't fetch transaction state from the blockchain. Postponed.")

			return postpone()
		}

		if isMined == false {
			return postpone()
		}

		lockAmountAndDestinationAddressCorrect, err := checkLockAmountAndDestinationFunc(sourceShard, txHash,
			tx.SourceAmount, tx.SourceShardMultiSigAddress.String())
		if err != nil {
			return txError(err)
		}
		if !lockAmountAndDestinationAddressCorrect {
			return txError(errors.New("Lock tx amount or destination is not refer to exhange"))
		}

		isUsed, err := checkIfLockDidntUseYetFunc(sourceShard, txHash, tx.SourceShardMultiSigAddress.String())
		if err != nil {
			return txError(err)
		}
		if isUsed {
			return txError(errors.New("Lock transaction is already used"))
		}

		err = tx.SetState(txs.ClientFundsLocked)
		if err != nil {
			return txError(err)
		}

		logger.Log.Debug().Uint64("txID", tx.ID).Msg("Client locked funds")
		logger.Log.Debug().Str("client-funds-locking-tx", txHash.String()).Msg("")
		return dataUpdated()
	}

	h.transactionsHandlerWrapper(processTx, transactions, wg, errorsFlow)
}

// processClientFundsLockedTXsAsynchronously prepares funds locking TX and stores it in the the database.
// This is needed to prevent cases, when TX is issued to the network,
// but no tx copy is saved to the database (e.g. due to EAD crash),
// so no TX state could be fetched/checked in future.
//
// This method does not checks any balance of the EADs wallet,
// cause it is expected that cross shard tx could not reach this state due to the reservations check,
// hat is done BEFORE any new cross shard operation is created.
// So this method only prepares money to be sent, by creating corresponding transaction.
func (h *Handler) processClientFundsLockedTXsAsynchronously(
	transactions map[uint64]*txs.Transaction, wg *sync.WaitGroup,
	errorsFlow chan txBoundError, updates chan txBoundUpdate) {

	processTx := func(tx *txs.Transaction) (err error) {
		txError := func(err error) (e error) {
			return h.txBoundError(tx, errorsFlow, err)
		}

		dataUpdated := func() (err error) {
			updates <- txBoundUpdate{tx: tx}
			return nil
		}

		if tx.State != txs.ClientFundsLocked {
			err = fmt.Errorf("expected TX state is 'ClientFundsLocked': %w", ErrInvalidTransactionState)
			return txError(err)
		}

		destinationShard, err := h.getDestinationShardConnector(tx)
		if err != nil {
			return txError(err)
		}

		var (
			multiSig        *crypto.MultiSigAddress
			multiSigAddress *btcutil.AddressScriptHash
		)
		if tx.Schema == txs.SchemaSenderEADSender {
			multiSig, multiSigAddress, err = destinationShard.Prepare2SigFundsLockMultiSig(
				tx.ClientPubKey, int32(tx.DestinationShardLockWindow))
			if err != nil {
				return txError(err)
			}

		} else if tx.Schema == txs.SchemaSenderEADReceiver {
			multiSig, multiSigAddress, err = destinationShard.Prepare3SigFundsLockMultiSig(
				tx.ClientPubKey, tx.ReceiverPubKey, int32(tx.DestinationShardLockWindow))
			if err != nil {
				return txError(err)
			}
		}

		tx.DestinationShardMultiSigAddress = multiSigAddress
		tx.DestinationShardMultiSigRedeemScript = multiSig.RawRedeemScript
		logger.Log.Debug().Str("multisig-address", multiSigAddress.String()).Str(
			"multisig-redeem-script", multiSig.RedeemScript).Uint64(
			"refund-deferring-period", tx.DestinationShardLockWindow).Msg(
			"Multi sig in destination shard created")

		err = tx.SetState(txs.EADLockMultiSigCreated)
		if err != nil {
			return txError(err)
		}

		return dataUpdated()
	}

	h.transactionsHandlerWrapper(processTx, transactions, wg, errorsFlow)
}

// processClientFundsLockedTXsAsynchronously prepares funds locking TX and stores it in the the database.
// This is needed to prevent cases, when TX is issued to the network,
// but no tx copy is saved to the database (e.g. due to EAD crash),
// so no TX state could be fetched/checked in future.
//
// This method does not checks any balance of the EADs wallet,
// cause it is expected that cross shard tx could not reach this state due to the reservations check,
// hat is done BEFORE any new cross shard operation is created.
// So this method only prepares money to be sent, by creating corresponding transaction.
func (h *Handler) processEAFundsLockingTxPreparedTXsAsynchronously(
	transactions map[uint64]*txs.Transaction, wg *sync.WaitGroup,
	errorsFlow chan txBoundError, updates chan txBoundUpdate) {

	processTx := func(tx *txs.Transaction) (err error) {
		dataUpdated := func() (err error) {
			updates <- txBoundUpdate{tx: tx}
			return nil
		}

		postpone := func() (err error) {
			const postponeIntervalSeconds = 5
			h.postponeTransaction(tx, postponeIntervalSeconds)
			return dataUpdated()

			// todo: check how many times transaction has been postponed in the past.
			// 		 if more than max postpone attempts - reject with error.
		}

		postponeWithError := func(e error) (err error) {
			LogErr(tx, err).Err(e).Msg("")
			return postpone()
		}

		txElapsed := func(tx *txs.Transaction) (isElapsed bool, err error) {
			currentBlockNumber, err := h.blocksHandler.GetCurrentBlockNumber(tx.SourceShardID)
			if err != nil {
				return
			}

			if currentBlockNumber > tx.MaxBlockHeightForCstxSigning {
				isElapsed = true
			}
			return
		}

		txError := func(err error) (e error) {
			return h.txBoundError(tx, errorsFlow, err)
		}

		lockFundsInDestinationShard := func(tx *txs.Transaction) (err error) {
			// todo : add to the request
			//allowHighFees := true // todo: [post PoC] move this to settings

			///Testing:Injection:LockFundsInDestinationShardError

			data := url.Values{
				"shard-id":          {strconv.Itoa(int(tx.DestinationShardID))},
				"uuid":              {tx.UUID.String()},
				"amount":            {strconv.Itoa(int(tx.DestinationAmount))},
				"address":           {tx.DestinationShardMultiSigAddress.String()},
				"max-blocks-height": {strconv.Itoa(int(tx.DestinationShardMaxBlockHeight))},
			}
			resp, err := http.PostForm(settings.Conf.PaymentsHandler.URL+"/api/v1/payments/", data)
			if err != nil {
				return
			}
			if resp.Status != "200 OK" {
				err = errors.New("Wrong handler response " + resp.Status)
				return
			}
			logger.Log.Info().Msg("Payment was delivered")
			return
		}

		if tx.State != txs.EADLockMultiSigCreated {
			err = fmt.Errorf("expected TX state is 'ClientFundsLocked': %w", ErrInvalidTransactionState)
			return txError(err)
		}

		isTxElapsed, err := txElapsed(tx)
		if err != nil {
			return postponeWithError(err)
		}
		if isTxElapsed {
			logger.Log.Info().Msg("Tx become elapsed. Start refunding")
			err = tx.SetState(txs.LockFundsReturning)
			if err != nil {
				return postponeWithError(err)
			}
			return dataUpdated()
		}

		// Lock funds in EAD -> Sender / Receiver locking multi. sig.
		err = lockFundsInDestinationShard(tx)
		if err != nil {
			return postponeWithError(err)
		}

		err = tx.SetState(txs.EAFundsLockingTxIssued)
		if err != nil {
			return postponeWithError(err)
		}

		return dataUpdated()
	}

	h.transactionsHandlerWrapper(processTx, transactions, wg, errorsFlow)
}

type PaymentStateResponse struct {
	State    uint8  `json:"state"`
	TxHash   []byte `json:"tx-hash"`
	TxOutNum uint64 `json:"tx-out-num"`
}

func (h *Handler) processEAFundsLockingTxIssuedTXsAsynchronously(
	transactions map[uint64]*txs.Transaction, wg *sync.WaitGroup,
	errorsFlow chan txBoundError, updates chan txBoundUpdate) {

	processTx := func(tx *txs.Transaction) (err error) {
		dataUpdated := func() (err error) {
			updates <- txBoundUpdate{tx: tx}
			return nil
		}

		txError := func(err error) (e error) {
			return h.txBoundError(tx, errorsFlow, err)
		}

		postpone := func() (err error) {
			const postponeIntervalSeconds = 5
			h.postponeTransaction(tx, postponeIntervalSeconds)
			return dataUpdated()

			// todo: check how many times transaction has been postponed in the past.
			// 		 if more than max postpone attempts - reject with error.
		}

		postponeWithError := func(e error) (err error) {
			LogErr(tx, err).Err(e).Msg("")
			return postpone()
		}

		txElapsed := func(tx *txs.Transaction) (isElapsed bool, err error) {
			currentBlockNumber, err := h.blocksHandler.GetCurrentBlockNumber(tx.SourceShardID)
			if err != nil {
				return
			}

			if currentBlockNumber > tx.MaxBlockHeightForCstxSigning {
				isElapsed = true
			}
			return
		}

		fundsAreLocked := func(tx *txs.Transaction) (ok bool, lockTxHash []byte, lockTxOutNum uint64, err error) {

			///Testing:Injection:FundsAreLockedError

			data := url.Values{
				"uuid": {tx.UUID.String()},
			}
			resp, err := http.PostForm(settings.Conf.PaymentsHandler.URL+"/api/v1/payments/state/", data)
			if err != nil {
				logger.Log.Info().Msg("Can't send payment state request")
				return
			}
			if resp.Status != "200 OK" {
				err = errors.New("Wrong response from payments handler")
				return
			}

			var res PaymentStateResponse
			err = json.NewDecoder(resp.Body).Decode(&res)
			if err != nil {
				return
			}
			logger.Log.Debug().Msg("Handler's transaction state " + strconv.Itoa(int(res.State)))

			switch res.State {
			case 3:
				lockTxHash = res.TxHash[:]
				lockTxOutNum = res.TxOutNum
				ok = true
			case 4:
				ok = true
			default:
				ok = false
			}
			return
		}

		if tx.State != txs.EAFundsLockingTxIssued {
			err = fmt.Errorf("expected TX state is 'EAFundsLockingTxIssued': %w", ErrInvalidTransactionState)
			return postponeWithError(err)
		}

		isTxElapsed, err := txElapsed(tx)
		if err != nil {
			return postponeWithError(err)
		}
		if isTxElapsed {
			logger.Log.Info().Msg("Tx become elapsed. Start refunding")
			err = tx.SetState(txs.LockFundsReturning)
			if err != nil {
				return postponeWithError(err)
			}
			return dataUpdated()
		}

		areFundsLocked, lockTxHash, lockTxOutNum, err := fundsAreLocked(tx)
		if err != nil {
			return postponeWithError(err)
		}

		if areFundsLocked {
			if len(lockTxHash) == 0 {
				return txError(errors.New("Payment becomes outdated"))
			}
			tx.EAFundsLockTxHash = lockTxHash
			tx.EAFundsLockTxOutNum = lockTxOutNum
			err = tx.SetState(txs.EAFundsLockingTXMined)
			if err != nil {
				return postponeWithError(err)
			}
			return dataUpdated()
		}

		return postpone()
	}

	h.transactionsHandlerWrapper(processTx, transactions, wg, errorsFlow)
}

func (h *Handler) processEADFundsLockingTxMinedTXsAsynchronously(
	transactions map[uint64]*txs.Transaction, wg *sync.WaitGroup,
	errorsFlow chan txBoundError, updates chan txBoundUpdate) {

	processTx := func(tx *txs.Transaction) (err error) {
		dataUpdated := func() (err error) {
			updates <- txBoundUpdate{tx: tx}
			return nil
		}

		// On this stage transaction can't be finished with an error. It should be to the refunding stage.
		txError := func(err error) (e error) {
			logger.Log.Error().Err(err).Msg(
				"During processing transaction an error occurred. Try process it on next iteration")
			if tx.CrossShardTx != nil {
				_ = tx.SetState(txs.CrossShardTxPrepared)
				return dataUpdated()
			} else {
				_ = tx.SetState(txs.LockFundsReturning)
				return dataUpdated()
			}
		}

		txElapsed := func(tx *txs.Transaction) (isElapsed bool, err error) {
			currentBlockNumber, err := h.blocksHandler.GetCurrentBlockNumber(tx.SourceShardID)
			if err != nil {
				return
			}

			if currentBlockNumber > tx.MaxBlockHeightForCstxSigning {
				isElapsed = true
			}
			return
		}

		// todo: make this guard common for each one processing function.
		if tx.State != txs.EAFundsLockingTXMined {
			err = fmt.Errorf("expected TX state is 'EAFundsLockingTXMined': %w", ErrInvalidTransactionState)
			err = tx.SetState(txs.LockFundsReturning)
			return dataUpdated()
		}

		if tx.CrossShardTx != nil {
			// seems cross shard ts was build on previous iteration. go to next state
			return txError(errors.New("CrossShardTx should be nil on this stage"))
		}

		isTxElapsed, err := txElapsed(tx)
		if err != nil {
			return txError(err)
		}

		if isTxElapsed {
			return txError(errors.New("Transaction becomes elapsed"))
		}

		crossShardTx, err := h.blockchain.PrepareCrossShardTX(tx)
		if err != nil {
			return txError(err)
		}

		crossShardTXBinary, err := crossShardTx.MarshalBinary()
		if err != nil {
			return txError(err)
		}

		tx.CrossShardTx = crossShardTXBinary
		err = tx.SetState(txs.CrossShardTxPrepared)
		if err != nil {
			return txError(err)
		}

		return dataUpdated()
	}

	h.transactionsHandlerWrapper(processTx, transactions, wg, errorsFlow)
}

func (h *Handler) processCrossShardTXsAsynchronously(
	transactions map[uint64]*txs.Transaction, wg *sync.WaitGroup,
	errorsFlow chan txBoundError, updates chan txBoundUpdate) {

	processTx := func(tx *txs.Transaction) (err error) {

		dataUpdated := func() (err error) {
			updates <- txBoundUpdate{tx: tx}
			return nil
		}

		//txError := func(err error) (e error) {
		//	return h.txBoundError(tx, errorsFlow, err)
		//}

		postpone := func() (err error) {
			const postponeIntervalSeconds = 5
			h.postponeTransaction(tx, postponeIntervalSeconds)
			return dataUpdated()

			// todo: check how many times transaction has been postponed in the past.
			// 		 if more than max postpone attempts - reject with error.
		}

		postponeWithInfo := func(msg string) (err error) {
			LogErr(tx, err).Msg(msg)
			return postpone()
		}

		postponeWithError := func(e error) (err error) {
			LogErr(tx, err).Err(e).Msg("")
			return postpone()
		}

		txElapsed := func(tx *txs.Transaction) (isElapsed bool, err error) {
			currentBlockNumber, err := h.blocksHandler.GetCurrentBlockNumber(tx.SourceShardID)
			if err != nil {
				return
			}

			if currentBlockNumber > tx.MaxBlockHeightForCstxSigning {
				isElapsed = true
			}
			return
		}

		pickTxFromLockAddress := func(shardConnector *blockchain.ShardConnector, address string,
			inputTxHashBytes []byte, inputTxInNum uint64) (
			isFound bool, blockChainTX *wire.MsgTx, err error) {

			inputTxHash, err := chainhash.NewHash(inputTxHashBytes)
			if err != nil {
				return
			}

			requestUrl := fmt.Sprintf("%s/api/v1/indexes/addresses/transactions/with-input/?address=%s&tx_input=%s&tx_input_idx=%d",
				shardConnector.AddressesTxsIndexerInterface, address, inputTxHash.String(), inputTxInNum)

			resp, err := http.Get(requestUrl)
			if err != nil {
				return
			}
			if resp.Status != "200 OK" {
				err = errors.New("Wrong response from addreses txs indexer")
				return
			}

			bodyBytes, err := ioutil.ReadAll(resp.Body)
			if err != nil {
				return
			}

			var txHashes []string
			err = json.Unmarshal(bodyBytes, &txHashes)
			if err != nil {
				return
			}

			if len(txHashes) == 0 {
				isFound = false
				logger.Log.Debug().Msg("Not found tx with specified input")
				return
			}

			if len(txHashes) > 1 {
				logger.Log.Debug().Strs("tx-hashes", txHashes).Msg("There are more than one CSTX found")
			}

			txHash, err := chainhash.NewHashFromStr(txHashes[0])
			if err != nil {
				return false, nil, err
			}
			tx, err := shardConnector.GetRawTransaction(txHash)
			if err != nil {
				return
			}

			blockChainTX = tx.MsgTx()
			isFound = true
			return
		}

		getTXState := func(shardConnector *blockchain.ShardConnector, tx *wire.MsgTx) (isFound, isMined bool, err error) {

			txHash := tx.TxHash()

			// WARN: Ot is very important check mem. pool here!
			// 		 Otherwise there could be the case, when TX hax being copied from the some shard into another shard,
			// 		 still not included even in first block, EAD on the next iteration if this function
			//		 does not sees this copied transaction in chain, and tries to copy it once again,
			//		 after which receives false negative error.
			isFound, isMined, err = shardConnector.IsTxMined(&txHash, ec.CrossShardTxConfirmationsExpected, true)
			return
		}

		if tx.State != txs.CrossShardTxPrepared {
			err = fmt.Errorf("expected TX state is 'CrossShardTxPrepared': %w", ErrInvalidTransactionState)
			return postponeWithError(err)
		}

		sourceShard, destinationShard, err := h.getSourceAndDestinationShardsConnectors(tx)
		if err != nil {
			return postponeWithError(err)
		}

		var (
			sourceAddress      = tx.SourceShardMultiSigAddress.String()
			destinationAddress = tx.DestinationShardMultiSigAddress.String()
		)

		isTxElapsed, err := txElapsed(tx)
		if err != nil {
			return postponeWithError(err)
		}
		if isTxElapsed {
			logger.Log.Info().Msg("Tx become elapsed. Start refunding")
			err = tx.SetState(txs.LockFundsReturning)
			if err != nil {
				return postponeWithError(err)
			}
			return dataUpdated()
		}

		isSourceShardTxFound, sourceShardTx, err := pickTxFromLockAddress(
			sourceShard, sourceAddress, tx.ClientFundLockTxHash, 0)
		if err != nil {
			return postponeWithError(err)
		}
		if isSourceShardTxFound {
			LogDebug(tx).Str("hash", sourceShardTx.TxHash().String()).Msg("Source shard TX found")
		}

		isDestinationShardTxFound, destinationShardTx, err := pickTxFromLockAddress(
			destinationShard, destinationAddress, tx.EAFundsLockTxHash, tx.EAFundsLockTxOutNum)
		if err != nil {
			return postponeWithError(err)
		}
		if isDestinationShardTxFound {
			LogDebug(tx).Str("hash", destinationShardTx.TxHash().String()).Msg("Destination shard TX found")
		}

		sourceTxFound := false
		sourceTxMined := false
		if isSourceShardTxFound {
			sourceTxFound, sourceTxMined, err = getTXState(sourceShard, sourceShardTx)
			if err != nil {
				return postponeWithError(err)
			}
		}

		destinationTxFound := false
		destinationTxMined := false
		if isDestinationShardTxFound {
			destinationTxFound, destinationTxMined, err = getTXState(destinationShard, destinationShardTx)
			if err != nil {
				return postponeWithError(err)
			}
		}

		if !sourceTxFound && !destinationTxFound {
			msg := "No source shard TX nor destination shard TX ha been found in corresponding shards. " +
				"Seems like no Sender nor Receiver has published cross shard TXs yet. Waiting..."
			return postponeWithInfo(msg)
		}

		if sourceTxFound && destinationTxFound {
			if !sourceTxMined && !destinationTxMined {
				msg := "Source shard TX and destination shard TX are found in corresponding chains, " +
					"but both are not mined yet. Waiting..."
				return postponeWithInfo(msg)
			}

			if sourceTxMined && destinationTxMined {
				msg := "Cross Shard TX mined in both shards. Exchange operation finished."
				LogInfo(tx).Msg(msg)

				err = tx.SetState(txs.CrossShardTxMined)
				if err != nil {
					return postponeWithError(err)
				}

				return dataUpdated()
			}

			if sourceTxMined {
				msg := "Source shard TX mined. Waiting for destination shard to be mined too."
				return postponeWithInfo(msg)

			}

			if destinationTxMined {
				msg := "Destination shard TX mined. Waiting for source shard to be mined too."
				return postponeWithInfo(msg)
			}
		}

		if sourceTxFound {
			if sourceTxMined {
				_, err = destinationShard.IssueCrossShardTx(sourceShardTx)
				if err != nil {
					err = fmt.Errorf("can't copy source shard transaction to the destination shard: %w", err)
					return postponeWithError(err)
				}

				msg := "Cross shard TX copied from source shard into the destination shard. Waiting to be mined."
				return postponeWithInfo(msg)
			} else {
				msg := "Source shard TX not mined yet. Waiting..."
				return postponeWithInfo(msg)
			}

		} else if destinationTxFound {
			if destinationTxMined {
				_, err = sourceShard.IssueCrossShardTx(destinationShardTx)
				if err != nil {
					err = fmt.Errorf("can't copy destination shard transaction to the sources shard: %w", err)
					return postponeWithError(err)
				}

				msg := "Cross shard TX copied from destination shard into the source shard. Waiting to be mined."
				return postponeWithInfo(msg)
			} else {
				msg := "Destination shard TX not mined yet. Waiting..."
				return postponeWithInfo(msg)
			}
		}
		return
	}

	h.transactionsHandlerWrapper(processTx, transactions, wg, errorsFlow)
}

func (h *Handler) processLockFundsRefundingAsynchronously(
	transactions map[uint64]*txs.Transaction, wg *sync.WaitGroup,
	errorsFlow chan txBoundError, updates chan txBoundUpdate) {

	processTx := func(tx *txs.Transaction) (err error) {

		dataUpdated := func() (err error) {
			updates <- txBoundUpdate{tx: tx}
			return nil
		}

		//txError := func(err error) (e error) {
		//	return h.txBoundError(tx, errorsFlow, err)
		//}

		postpone := func() (err error) {
			const postponeIntervalSeconds = 5
			h.postponeTransaction(tx, postponeIntervalSeconds)
			return dataUpdated()

			// todo: check how many times transaction has been postponed in the past.
			// 		 if more than max postpone attempts - reject with error.
		}

		postponeWithInfo := func(msg string) (err error) {
			LogErr(tx, err).Msg(msg)
			return postpone()
		}

		postponeWithError := func(e error) (err error) {
			LogErr(tx, err).Err(e).Msg("")
			return postpone()
		}

		if tx.State != txs.LockFundsReturning {
			err = fmt.Errorf("expected TX state is 'CrossShardTxPrepared': %w", ErrInvalidTransactionState)
			return postponeWithError(err)
		}

		_, destinationShard, err := h.getSourceAndDestinationShardsConnectors(tx)
		if err != nil {
			return postponeWithError(err)
		}

		checkIfLockWindowElapsed := func(shardID shards.ID, lockWindow uint64) (isElapsed bool, err error) {
			currentBlockNumber, err := h.blocksHandler.GetCurrentBlockNumber(shardID)
			if err != nil {
				return
			}

			isElapsed = currentBlockNumber > lockWindow
			return
		}

		buildRefundingTx := func(shardID shards.ID, multiSigAddress *btcutil.AddressScriptHash,
			multiSigRedeemScript []byte, lockTx *wire.MsgTx, outputTxNum uint64) (
			isFound bool, refundingTX *wire.MsgTx, err error) {

			for idx, curBlockChainTxOut := range lockTx.TxOut {
				logger.Log.Debug().Int64("lock-tx-out-amount", curBlockChainTxOut.Value).
					Int("lock-tx-out-idx", idx).Msg("buildRefundingTx")
				if uint64(idx) == outputTxNum {
					connector, err := h.getShardConnector(shardID)
					if err != nil {
						return false, nil, err
					}
					refundingTX, err = connector.BuildRefundingTx(multiSigAddress,
						multiSigRedeemScript, curBlockChainTxOut, lockTx.TxHash().String())
					if err != nil {
						return false, nil, err
					}
					isFound = true
					return isFound, refundingTX, nil
				}
			}

			logger.Log.Debug().Msg("Not found tx with specified input")
			isFound = false
			return
		}

		getTXState := func(shardID shards.ID, tx *wire.MsgTx) (isFound, isMined bool, err error) {
			connector, err := h.getShardConnector(shardID)
			if err != nil {
				return
			}

			txHash := tx.TxHash()

			// WARN: Ot is very important check mem. pool here!
			// 		 Otherwise there could be the case, when TX hax being copied from the some shard into another shard,
			// 		 still not included even in first block, EAD on the next iteration if this function
			//		 does not sees this copied transaction in chain, and tries to copy it once again,
			//		 after which receives false negative error.
			var confirmationsExpected int64 = 6 // todo: load it from settings.
			isFound, isMined, err = connector.IsTxMined(&txHash, confirmationsExpected, true)
			return
		}

		lockWindowElapsed, err := checkIfLockWindowElapsed(tx.DestinationShardID, tx.DestinationShardMaxBlockHeight)
		if err != nil {
			return postponeWithError(err)
		}

		if !lockWindowElapsed {
			msg := "Destination shard lock window doesn't elapsed yet. Waiting..."
			return postponeWithInfo(msg)
		}

		lockTxHash, err := chainhash.NewHash(tx.EAFundsLockTxHash)
		if err != nil {
			return postponeWithError(err)
		}
		logger.Log.Debug().Str("lock-tx-hash", lockTxHash.String()).Msg("Get EAD lock tx hash")

		lockTx, err := destinationShard.GetRawTransaction(lockTxHash)
		if err != nil {
			return postponeWithError(err)
		}

		isFoundLockTx, refundingTx, err := buildRefundingTx(tx.DestinationShardID, tx.DestinationShardMultiSigAddress,
			tx.DestinationShardMultiSigRedeemScript, lockTx.MsgTx(), tx.EAFundsLockTxOutNum)
		if err != nil {
			return postponeWithError(err)
		}
		if !isFoundLockTx {
			msg := "Can't find destination shard lock tx. Waiting..."
			return postponeWithInfo(msg)
		}

		LogDebug(tx).Str("hash", refundingTx.TxHash().String()).Msg("Refunding TX built")
		refundingTxFound, refundingTxMined, err := getTXState(tx.DestinationShardID, refundingTx)
		if err != nil {
			return postponeWithError(err)
		}

		if !refundingTxFound {
			txHash, err := destinationShard.IssuedRefundingTx(refundingTx)
			if err != nil {
				return postponeWithError(err)
			}
			msg := fmt.Sprintf("Refunding TX %s published. Waiting to be mined.", txHash.String())
			return postponeWithInfo(msg)
		}

		if !refundingTxMined {
			msg := "Refunding TX is found in corresponding chain, but is not mined yet. Waiting..."
			return postponeWithInfo(msg)
		}

		msg := "Refunding TX mined. Exchange operation finished."
		LogInfo(tx).Msg(msg)

		err = tx.SetState(txs.LockFundsReturned)
		if err != nil {
			return postponeWithError(err)
		}

		return dataUpdated()
	}

	h.transactionsHandlerWrapper(processTx, transactions, wg, errorsFlow)
}

// todo: add comment
func (h *Handler) transactionsHandlerWrapper(handler func(tx *txs.Transaction) (err error),
	transactions map[uint64]*txs.Transaction, wg *sync.WaitGroup, errors chan txBoundError) {
	markTXDoneInAnyCase := func() {
		wg.Done()
	}

	processTx := func(tx *txs.Transaction) {
		defer markTXDoneInAnyCase()

		if settings.Conf.Debug {
			logger.Log.Debug().Uint64(
				"id", tx.ID).Msg("Exchange TX execution started")
		}
		err := handler(tx)

		if err != nil {
			errors <- txBoundError{
				tx:  tx,
				err: err,
			}

			logger.Log.Err(err).Uint64(
				"id", tx.ID).Uint8("state", tx.State).Msg("")
		}

		if settings.Conf.Debug {
			logger.Log.Debug().Uint64(
				"id", tx.ID).Uint8(
				"state", tx.State).Msg(
				"Exchange TX execution finished")
		}
	}

	// todo: [low priority] [improvement] [performance]
	// 		 Add goroutines pool here, cause transaction amount could be potentially very large.
	for _, tx := range transactions {

		// todo: Add workers pool here.
		// 		 Transactions could be executed in parallel most of the time.
		//		 The only complex thing here is a simultaneous access to the structure,
		//		 that represents blockchain (blockchain communicator)
		//
		//	 	 It is possible to create several this structures per pool and to distribute transactions
		//		 across this pool of workers, so no data race would occur.
		processTx(tx)
	}

	return

}

// todo: move this method to the database level.
func (h *Handler) updateTransaction(tx *txs.Transaction) (err error) {
	sourceAmount := fmt.Sprint(int64(tx.SourceAmount))
	destinationAmount := fmt.Sprint(int64(tx.DestinationAmount))

	sourceShardMultiSigAddressOrNull := "NULL"
	if tx.SourceShardMultiSigAddress != nil {
		sourceShardMultiSigAddressOrNull = tx.SourceShardMultiSigAddress.String()
	}

	sourceShardMultiSigRedeemScriptOrNull := "NULL"
	if tx.SourceShardMultiSigRedeemScript != nil {
		hexRepresentation := hex.EncodeToString(tx.SourceShardMultiSigRedeemScript)
		sourceShardMultiSigRedeemScriptOrNull = fmt.Sprint("decode('", hexRepresentation, "', 'hex')")
	}

	destinationShardMultiSigAddressOrNull := "NULL"
	if tx.DestinationShardMultiSigAddress != nil {
		destinationShardMultiSigAddressOrNull = tx.DestinationShardMultiSigAddress.String()
	}

	destinationShardMultiSigRedeemScriptOrNull := "NULL"
	if tx.DestinationShardMultiSigRedeemScript != nil {
		hexRepresentation := hex.EncodeToString(tx.DestinationShardMultiSigRedeemScript)
		destinationShardMultiSigRedeemScriptOrNull = fmt.Sprint("decode('", hexRepresentation, "', 'hex')")
	}

	errorDescriptionOrNULL := "NULL"
	if tx.ErrorDescription != nil {
		errorDescriptionOrNULL = fmt.Sprint("'", *tx.ErrorDescription, "'")
	}

	delayedUntilOrNULL := "NULL"
	if tx.DelayedUntil != nil {
		delayedUntilOrNULL = fmt.Sprint("'", tx.DelayedUntil.Format(time.RFC3339), "'")
	}

	clientPubKeyOrNULL := "NULL"
	if tx.ClientPubKey != nil {
		hexRepresentation := hex.EncodeToString(tx.ClientPubKey)
		clientPubKeyOrNULL = fmt.Sprint("decode('", hexRepresentation, "', 'hex')")
	}

	clientFundsLockingTXHashOrNULL := "NULL"
	if tx.ClientFundLockTxHash != nil {
		hexRepresentation := hex.EncodeToString(tx.ClientFundLockTxHash)
		clientFundsLockingTXHashOrNULL = fmt.Sprint("decode('", hexRepresentation, "', 'hex')")
	}

	clientFundsLockingTXOrNULL := "NULL"
	if tx.ClientFundLockTx != nil {
		hexRepresentation := hex.EncodeToString(tx.ClientFundLockTx)
		clientFundsLockingTXOrNULL = fmt.Sprint("decode('", hexRepresentation, "', 'hex')")
	}

	eaFundsLockingTXOrNULL := "NULL"
	if tx.EAFundsLockTx != nil {
		hexRepresentation := hex.EncodeToString(tx.EAFundsLockTx)
		eaFundsLockingTXOrNULL = fmt.Sprint("decode('", hexRepresentation, "', 'hex')")
	}

	eaFundsLockingTXHashOrNULL := "NULL"
	if tx.EAFundsLockTxHash != nil {
		hexRepresentation := hex.EncodeToString(tx.EAFundsLockTxHash)
		eaFundsLockingTXHashOrNULL = fmt.Sprint("decode('", hexRepresentation, "', 'hex')")
	}

	crossShardTXOrNULL := "NULL"
	if tx.CrossShardTx != nil {
		hexRepresentation := hex.EncodeToString(tx.CrossShardTx)
		crossShardTXOrNULL = fmt.Sprint("decode('", hexRepresentation, "', 'hex')")
	}

	query := fmt.Sprint(
		"UPDATE transactions "+
			"SET "+
			"source_amount=", sourceAmount, ", ",
		"destination_amount=", destinationAmount, ", ",
		"source_shard_destination_address='", tx.SourceShardDestinationAddress.String(), "', ",
		"destination_shard_destination_address='", tx.DestinationShardDestinationAddress.String(), "', ",
		"source_shard_multi_sig_address='", sourceShardMultiSigAddressOrNull, "', ",
		"source_shard_multi_sig_redeem_script=", sourceShardMultiSigRedeemScriptOrNull, ", ",
		"destination_shard_multi_sig_address='", destinationShardMultiSigAddressOrNull, "', ",
		"destination_shard_multi_sig_redeem_script=", destinationShardMultiSigRedeemScriptOrNull, ", ",
		"state=", tx.State, ", ",
		"error_description=", errorDescriptionOrNULL, ", ",
		"delayed_until=", delayedUntilOrNULL, ", ",
		"client_pub_key=", clientPubKeyOrNULL, ", ",
		"client_funds_locking_tx_hash=", clientFundsLockingTXHashOrNULL, ", ",
		"client_funds_locking_tx=", clientFundsLockingTXOrNULL, ", ",
		"ea_funds_locking_tx_hash=", eaFundsLockingTXHashOrNULL, ", ",
		"ea_funds_locking_tx=", eaFundsLockingTXOrNULL, ", ",
		"ea_funds_locking_tx_out_num=", tx.EAFundsLockTxOutNum, ", ",
		"cross_shard_tx=", crossShardTXOrNULL, ", ",
		"source_shard_lock_window=", tx.SourceShardLockWindow, ", ",
		"source_shard_max_block_height=", tx.SourceShardMaxBlockHeight, ", ",
		"destination_shard_lock_window=", tx.DestinationShardLockWindow, ", ",
		"destination_shard_max_block_height=", tx.DestinationShardMaxBlockHeight, ", ",
		"max_block_height_for_cstx_signing=", tx.MaxBlockHeightForCstxSigning, ", ",
		"updated='", tx.Updated.Format(time.RFC3339), "' ",
		"WHERE id=", tx.ID)

	_, err = database.DB.Exec(context.Background(), query)
	return
}

func (h *Handler) checkTransactionByClientFundsLockingTxHash(txHash []byte) (find bool, err error) {
	hexRepresentation := hex.EncodeToString(txHash)
	txHashStr := fmt.Sprint("decode('", hexRepresentation, "', 'hex')")
	query := fmt.Sprint("SELECT 1 FROM transactions WHERE client_funds_locking_tx_hash = " + txHashStr)

	rows, err := database.DB.Query(context.Background(), query)

	if err != nil {
		return
	}

	find = rows.Next()
	return
}

func (h *Handler) postponeTransaction(tx *txs.Transaction, seconds int) {
	frame := time.Second * time.Duration(seconds)
	deadline := h.UTCNow().Add(frame)

	tx.DelayedUntil = &deadline
	logger.Log.Debug().Uint64(
		"id", tx.ID).Time(
		"utc-wakeup-time", deadline).Msg("Exchange TX postponed")
}

func (h *Handler) UTCNow() time.Time {
	return time.Now().UTC()
}

// getSourceShardConnector is a syntactic sugar for getShardConnector.
func (h *Handler) getSourceShardConnector(tx *txs.Transaction) (*blockchain.ShardConnector, error) {
	return h.getShardConnector(tx.SourceShardID)
}

// getDestinationShardConnector is a syntactic sugar for getShardConnector.
func (h *Handler) getDestinationShardConnector(tx *txs.Transaction) (*blockchain.ShardConnector, error) {
	return h.getShardConnector(tx.DestinationShardID)
}

// getSourceAndDestinationShardsConnectors is a syntactic sugar for getShardConnector.
func (h *Handler) getSourceAndDestinationShardsConnectors(tx *txs.Transaction) (
	sourceShard, destinationShard *blockchain.ShardConnector, err error) {
	sourceShard, err = h.getSourceShardConnector(tx)
	if err != nil {
		return
	}

	destinationShard, err = h.getDestinationShardConnector(tx)
	if err != nil {
		return
	}

	return
}

// getShardConnector returns RPC connector for specified shard.
func (h *Handler) getShardConnector(id shards.ID) (
	c *blockchain.ShardConnector, err error) {

	c, isPresent := h.blockchain.ShardsConnectors[id]
	if !isPresent {
		// This kind of errors must be prevented on a transactions validation stage,
		// but here the additional check is also present as a runtime checks for cases,
		// when EAD's configuration has been changed on the fly.
		err = fmt.Errorf("shard is not supported by the EAD: %w", ec.ErrInvalidShardID)
		return
	}

	return
}

func (h *Handler) getLockWindowValues(sourceShardId shards.ID, destinationShardId shards.ID) (
	sourceShardLockWindow uint64, sourceShardMaxBlockHeight uint64,
	destinationShardLockWindow uint64, destinationShardMaxBlockHeight uint64,
	maxBlockHeightForCstxSigning uint64, err error) {

	sourceShardCurrentBlockNumber, err := h.blocksHandler.GetCurrentBlockNumber(sourceShardId)
	if err != nil {
		return
	}

	destShardCurrentBlockNumber, err := h.blocksHandler.GetCurrentBlockNumber(destinationShardId)
	if err != nil {
		return
	}

	// todo : set correct values
	sourceShardLockWindow = 80
	sourceShardMaxBlockHeight = sourceShardCurrentBlockNumber + sourceShardLockWindow
	destinationShardLockWindow = 40
	destinationShardMaxBlockHeight = destShardCurrentBlockNumber + destinationShardLockWindow
	maxBlockHeightForCstxSigning = sourceShardCurrentBlockNumber + 60
	return
}
