package transactions

import (
	"gitlab.com/jaxnet/hub/ead/core/handlers/transactions/transactions"
	"gitlab.com/jaxnet/hub/ead/core/logger"
)

func (h *Handler) txBoundError(tx *transactions.Transaction, errors chan txBoundError, err error) (e error) {

	// Passing error for further processing.
	select {
	case errors <- txBoundError{
		tx:  tx,
		err: err,
	}:

	default:
		logger.Log.Error().Err(err).Uint64(
			"txID", tx.ID).Msg(
			"Can't report transaction bound error: destination channel is full.")
	}

	tx.State = transactions.Error
	errorDescription := err.Error()
	tx.ErrorDescription = &errorDescription
	return e
}
