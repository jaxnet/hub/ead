package transactions

import (
	"bytes"
	"encoding/hex"
	"errors"
	"fmt"
	"gitlab.com/jaxnet/core/shard.core/btcec"
	"gitlab.com/jaxnet/core/shard.core/btcutil"
	"gitlab.com/jaxnet/core/shard.core/cmd/tx-gatling/txutils"
	"gitlab.com/jaxnet/hub/ead/core/crypto"
	"gitlab.com/jaxnet/hub/ead/core/handlers/limits"
	"gitlab.com/jaxnet/hub/ead/core/handlers/transactions/transactions"
	"gitlab.com/jaxnet/hub/ead/core/messages/clients_messages"
	"gitlab.com/jaxnet/hub/ead/core/settings"
	"gitlab.com/jaxnet/hub/ead/core/utils/communication"
	"math/big"
	"strings"
)

var (
	ErrValidation           = errors.New("validation error")
	ErrTxDataRewriteAttempt = fmt.Errorf("transaction data can't be rewritten: %v", ErrValidation)
)

func (h *Handler) validateInitExchangeRequest(req *clients_messages.RequestInitExchange) (err error) {
	if req.SourceAmount.Cmp(zero) != 1 {
		err = fmt.Errorf("can't initialize exchange operation with amount set to 0: %v", communication.ErrBadRequest)
		return
	}

	if req.Index.SourceShard == 0 || req.Index.DestinationShard == 0 {
		err = fmt.Errorf("EAD does not work with beacon chain (%w)", communication.ErrBadRequest)
		return
	}

	// Index validation.
	// Source and destination shards must be defined in settings.
	// Otherwise, EAD does not work with specified shards and request must be rejected.
	_, isSourceShardPresent := settings.Conf.Blockchain.ShardsMap[req.Index.SourceShard]
	_, isDestinationShardPresent := settings.Conf.Blockchain.ShardsMap[req.Index.DestinationShard]

	if !isSourceShardPresent || !isDestinationShardPresent {
		err = fmt.Errorf("EAD does not work with requested shards pair (%w)", communication.ErrBadRequest)
		return
	}

	fee, err := h.limitsHandler.Fee(req.Index)
	if err != nil {
		return
	}
	feeAmount := calculateProposalFeeAmount(req.SourceAmount, fee)
	expectedDestinationAmount := feeAmount.Sub(req.SourceAmount, feeAmount)
	if expectedDestinationAmount.Cmp(req.DestinationAmount) != 0 {
		err = fmt.Errorf("destination amount does not considered to fee: %v", communication.ErrBadRequest)
		return
	}
	return
}

func (h *Handler) validateClientLockFundsTxRequest(
	tx *transactions.Transaction, req *clients_messages.RequestClientFundsLockTx) (
	sourceShardMultiSigAddress *btcutil.AddressScriptHash, err error) {

	//
	// Data integrity check.
	// In case if request would be received several times - no data rewrite is allowed,
	// in case if some data is already populated and stored into DB.
	if tx.ClientFundLockTxHash != nil || tx.ClientFundLockTx != nil {
		err = fmt.Errorf(
			"exchange transaction is already populated with client funds lock tx. no rewrite is poosible: %v",
			ErrTxDataRewriteAttempt)
		return
	}

	//
	// Basic fields validation.
	//
	if req.TxHash == nil {
		err = fmt.Errorf("TX hash could not be empty: %v", ErrValidation)
		return
	}

	if len(req.TxHash) != 32 {
		err = fmt.Errorf("invalid TX hash: %v", ErrValidation)
		return
	}

	lockAlreadyPresent, err := h.checkTransactionByClientFundsLockingTxHash(req.TxHash)
	if err != nil {
		return
	}

	if lockAlreadyPresent {
		err = fmt.Errorf("client lock TX already present: %v", ErrValidation)
		return
	}

	// Note: Transaction presence in blockchain should not be checked,
	//       cause it could be still not mined, ant it is OK.

	if req.MultiSigRedeemScript == nil {
		err = fmt.Errorf("redeem script could not be empty: %v", ErrValidation)
		return
	}

	var multiSig *crypto.MultiSigAddress
	if tx.Schema == transactions.SchemaSenderEADSender {
		multiSig, err = h.validate2of2MultiSigAddress(tx, req)
		if err != nil {
			return
		}

	} else if tx.Schema == transactions.SchemaSenderEADReceiver {
		multiSig, err = h.validate3of3MultiSigAddress(tx, req)
		if err != nil {
			return
		}

	} else {
		err = errors.New(fmt.Sprint("invalid transaction schema type occurred: ", tx.Schema))
		return
	}

	sourceShardMultiSigAddress, err = btcutil.NewAddressScriptHash(multiSig.RawRedeemScript, settings.NetParams)
	return
}

func (h *Handler) validate2of2MultiSigAddress(
	tx *transactions.Transaction, req *clients_messages.RequestClientFundsLockTx) (multiSig *crypto.MultiSigAddress, err error) {

	if req.MultiSigRedeemScript == nil {
		err = fmt.Errorf("redeem script could not be empty: %v", ErrValidation)
		return
	}

	//
	// Fetching client's pub key from the multi sig.
	//
	originalMultiSig, err := txutils.DecodeScript(req.MultiSigRedeemScript, settings.NetParams)
	if err != nil {
		return
	}

	asmComponents := strings.Split(originalMultiSig.Asm, " ")

	// By default, redeem script of 2/2 multi sig address would be of the next format:
	// 2 HEX_PUB_KEY1 HEX_PUB_KEY2 2 OP_CHECKMULTISIG
	if len(asmComponents) != 14 {
		err = fmt.Errorf("redeem script must contain exactly 14 tokens: %v", ErrValidation)
		return
	}

	decodePubKey := func(hexRepresentation string) (key *btcec.PublicKey, err error) {
		pubKeyBinary, err := hex.DecodeString(hexRepresentation)
		if err != nil {
			err = fmt.Errorf("redeem script contains invalid pub key: %v", ErrValidation)
			return
		}

		key, err = btcec.ParsePubKey(pubKeyBinary, btcec.S256())
		if err != nil {
			err = fmt.Errorf("redeem script contains invalid pub key: %v, %v", err, ErrValidation)
			return
		}

		return
	}

	_, err = decodePubKey(asmComponents[5])
	if err != nil {
		return
	}

	_, err = decodePubKey(asmComponents[6])
	if err != nil {
		return
	}

	//
	// There are 2 ways to validate multi. sig address:
	// 1. to decompile redeem script and to check each one component for correctness, or
	// 2. to create another one instance of multi sig. using EXPECTED (trusted) parameters and then
	//	  check that redeem scripts of both instances are the same.
	//
	// Using first way, there is a need to check that pub keys are present and correspond to the expected ones.
	// Then, there is a need to check that multi. sig address format is 2/2 and then
	// check that required op. code is present.
	//
	// All this checks are automatically covered by the way number 2, so it is preferred.
	//
	clientAddress, err := btcutil.NewAddressPubKey(tx.ClientPubKey, settings.NetParams)
	if err != nil {
		err = fmt.Errorf("can't convert client's pub. key into address: %v", ErrValidation)
		return
	}

	eadAddress, err := btcutil.NewAddressPubKey(crypto.PubKey().SerializeUncompressed(), settings.NetParams)
	if err != nil {
		err = fmt.Errorf("can't convert ead's pub. key into address: %v", ErrValidation)
		return
	}

	signers := []string{
		clientAddress.String(),
		eadAddress.String(),
	}

	multiSig, err = crypto.MakeMultiSigLockScript(signers, len(signers), int32(tx.SourceShardLockWindow), settings.NetParams)
	if err != nil {
		err = fmt.Errorf("can't generate multisig address: %v, %v", err, ErrValidation)
		return
	}

	if bytes.Compare(multiSig.RawRedeemScript, req.MultiSigRedeemScript) != 0 {
		err = fmt.Errorf("invalid original multisig address: %v", ErrValidation)
		return
	}

	return
}

func (h *Handler) validate3of3MultiSigAddress(
	tx *transactions.Transaction, req *clients_messages.RequestClientFundsLockTx) (multiSig *crypto.MultiSigAddress, err error) {

	if req.MultiSigRedeemScript == nil {
		err = fmt.Errorf("redeem script could not be empty: %v", ErrValidation)
		return
	}

	//
	// Fetching client's pub key from the multi sig.
	//
	originalMultiSig, err := txutils.DecodeScript(req.MultiSigRedeemScript, settings.NetParams)
	if err != nil {
		return
	}

	asmComponents := strings.Split(originalMultiSig.Asm, " ")

	// By default, redeem script of 2/2 multi sig address would be of the next format:
	// 3 HEX_PUB_KEY1 HEX_PUB_KEY2 HEX_PUB_KEY3 3 OP_CHECKMULTISIG
	if len(asmComponents) != 15 {
		err = fmt.Errorf("redeem script must contain exactly 15 tokens: %v", ErrValidation)
		return
	}

	decodePubKey := func(hexRepresentation string) (key *btcec.PublicKey, err error) {
		pubKeyBinary, err := hex.DecodeString(hexRepresentation)
		if err != nil {
			err = fmt.Errorf("redeem script contains invalid pub key: %v", ErrValidation)
			return
		}

		key, err = btcec.ParsePubKey(pubKeyBinary, btcec.S256())
		if err != nil {
			err = fmt.Errorf("redeem script contains invalid pub key: %v, %v", err, ErrValidation)
			return
		}

		return
	}

	_, err = decodePubKey(asmComponents[5])
	if err != nil {
		return
	}

	_, err = decodePubKey(asmComponents[6])
	if err != nil {
		return
	}

	_, err = decodePubKey(asmComponents[7])
	if err != nil {
		return
	}

	//
	// There are 2 ways to validate multi. sig address:
	// 1. to decompile redeem script and to check each one component for correctness, or
	// 2. to create another one instance of multi sig. using EXPECTED (trusted) parameters and then
	//	  check that redeem scripts of both instances are the same.
	//
	// Using first way, there is a need to check that pub keys are present and correspond to the expected ones.
	// Then, there is a need to check that multi. sig address format is 2/2 and then
	// check that required op. code is present.
	//
	// All this checks are automatically covered by the way number 2, so it is preferred.
	//

	clientAddress, err := btcutil.NewAddressPubKey(tx.ClientPubKey, settings.NetParams)
	if err != nil {
		err = fmt.Errorf("can't convert client's pub. key into address: %v", ErrValidation)
		return
	}

	receiverAddress, err := btcutil.NewAddressPubKey(tx.ReceiverPubKey, settings.NetParams)
	if err != nil {
		err = fmt.Errorf("can't convert receiver's pub. key into address: %v", ErrValidation)
		return
	}

	eadAddress, err := btcutil.NewAddressPubKey(crypto.PubKey().SerializeUncompressed(), settings.NetParams)
	if err != nil {
		err = fmt.Errorf("can't convert ead's pub. key into address: %v", ErrValidation)
		return
	}

	signers := []string{
		clientAddress.String(),
		receiverAddress.String(),
		eadAddress.String(),
	}

	multiSig, err = crypto.MakeMultiSigLockScript(signers, len(signers), int32(tx.SourceShardLockWindow), settings.NetParams)
	if err != nil {
		err = fmt.Errorf("can't generate multisig address: %v, %v", err, ErrValidation)
		return
	}

	if bytes.Compare(multiSig.RawRedeemScript, req.MultiSigRedeemScript) != 0 {
		err = fmt.Errorf("invalid original multisig address: %v", ErrValidation)
		return
	}

	return
}

func calculateProposalFeeAmount(requiredAmount *big.Int, fee limits.Fee) *big.Int {
	precision := (&big.Rat{}).SetInt64(100000000)

	feePercentBig := (&big.Rat{}).Mul((&big.Rat{}).SetInt(requiredAmount), (&big.Rat{}).SetFloat64(fee.Percents))
	feePercentBigInt := feePercentBig.Mul(feePercentBig, precision).Num()
	feePercentBigInt = feePercentBigInt.Add(feePercentBigInt, (&big.Int{}).SetUint64(uint64(fee.Fixed)))
	return feePercentBigInt
}
