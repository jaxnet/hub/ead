package transactions

import (
	"errors"
	"fmt"
	"github.com/google/uuid"
	"gitlab.com/jaxnet/core/shard.core/btcutil"
	"gitlab.com/jaxnet/core/shard.core/types/chainhash"
	"gitlab.com/jaxnet/hub/ead/core/crypto"
	"gitlab.com/jaxnet/hub/ead/core/database"
	"gitlab.com/jaxnet/hub/ead/core/ec"
	"gitlab.com/jaxnet/hub/ead/core/handlers/transactions/transactions"
	"gitlab.com/jaxnet/hub/ead/core/logger"
	msg "gitlab.com/jaxnet/hub/ead/core/messages/clients_messages"
	"gitlab.com/jaxnet/hub/ead/core/settings"
	"gitlab.com/jaxnet/hub/ead/core/utils/communication"
)

// todo: migrate communication to the newly developed netstack.
func (h *Handler) processInitExchangeRequest(req *communication.RequestContext) (err error) {
	response := &msg.ResponseInitExchange{}
	defer func() {
		_ = req.Responses.Put(response, ec.ITTL)
	}()

	cr, ok := req.Context.(*msg.RequestInitExchange)
	if !ok {
		err = communication.ErrUnexpectedRequestType
		response.Code = msg.Error
		return
	}

	err = h.validateInitExchangeRequest(cr)
	if err != nil {
		if errors.Is(err, ErrValidation) {
			response.Code = msg.InvalidRequest
			return

		} else {
			response.Code = msg.Error
			return
		}
	}

	// Informing client about the required amount of blocks
	// to be set as a lock time for the multi. sig.
	//
	// No additional validation of shards presence is needed here:
	// the fact that EAD works in specified pair of shards has been validated on previous step.
	//sourceShardConfig := settings.Conf.Blockchain.ShardsMap[cr.Index.SourceShard]
	//destinationShardConfig := settings.Conf.Blockchain.ShardsMap[cr.Index.DestinationShard]

	// WARN: Source shard lock time is twice as more than destination shard lock time.
	// This is needed to ensure, that EAD is able to grab its money from the lock with client
	// even in case if receiver (or sender in S-EA-S schema) is signing transaction right
	// when the locks in destination shards are going to be open (special kind of attack).
	//
	// Details: https://gitlab.com/jaxnet/hub/ead/-/issues/52
	// Details: https://gitlab.com/jaxnet/hub/ead/-/issues/42
	// Details: https://gitlab.com/jaxnet/hub/ead/-/issues/8

	// Initialising the operation.
	// todo: replace big.Int by btcutil.Amount
	sourceAmount := btcutil.Amount(cr.SourceAmount.Int64())
	destinationAmount := btcutil.Amount(cr.DestinationAmount.Int64())

	eadAddress, err := crypto.Address()
	if err != nil {
		response.Code = msg.Error
		return
	}
	balance, err := h.AddressBalance(cr.Index.DestinationShard, eadAddress.String())
	if err != nil {
		response.Code = msg.Error
		return
	}
	logger.Log.Debug().Uint32("shard-id", uint32(cr.Index.DestinationShard)).Int64(
		"balance", int64(balance)).Msg("EAD balance")
	if balance < destinationAmount {
		logger.Log.Debug().Msg("EAD balance less than requested exchange amount")
		response.Code = msg.Error
		return
	}

	var txUUID uuid.UUID
	var sourceShardLockWindow, destinationShardLockWindow uint64
	if cr.Type == msg.RequestInitExchangeType2Sig {
		txUUID, sourceShardLockWindow, destinationShardLockWindow, err = h.init2SigExchangeTransaction(
			cr.Index, sourceAmount, destinationAmount, *cr.DestinationShardAddress,
			cr.SenderPubKey.SerializeUncompressed())

	} else if cr.Type == msg.RequestInitExchangeType3Sig {
		txUUID, sourceShardLockWindow, destinationShardLockWindow, err = h.init3SigExchangeTransaction(
			cr.Index, sourceAmount, destinationAmount, *cr.DestinationShardAddress,
			cr.SenderPubKey.SerializeUncompressed(),
			cr.ReceiverPubKey.SerializeUncompressed())
	} else {
		// Invalid operation type.
		response.Code = msg.Error
		return
	}

	if err != nil {
		if settings.Conf.Debug {
			logger.Log.Err(err).Int64(
				"source-amount", cr.SourceAmount.Int64()).Int64(
				"destination-amount", cr.DestinationAmount.Int64()).Msg(
				"Request for exchange operation rejected with error")
		}

		response.Code = msg.Error
		return
	}

	if settings.Conf.Debug {
		logger.Log.Debug().Int64(
			"source-amount", cr.SourceAmount.Int64()).Int64(
			"destination-amount", cr.DestinationAmount.Int64()).Str(
			"exchange-tx-uuid", response.TxUUID.String()).Uint64(
			"source-shard-lock-window", sourceShardLockWindow).Uint64(
			"destination-shard-lock-window", destinationShardLockWindow).Msg(
			"Request for exchange operation approved, tx execution started")
	}

	response.Code = msg.OK
	response.TxUUID = txUUID
	// ToDo: Here EAD is only provides recommendations for the client.
	//		 But it is very important to have the same validation
	//		 on the stage where the client's lock is going to be checked.
	response.SourceShardMultiSigLockWindowBlocks = sourceShardLockWindow
	response.DestinationShardMultiSigLockWindowBlocks = destinationShardLockWindow
	return
}

func (h *Handler) processClientLockFundsRequest(req *communication.RequestContext) (err error) {
	response := &msg.ResponseClientFundsLockTx{}
	defer func() {
		_ = req.Responses.Put(response, ec.ITTL)
	}()

	request, ok := req.Context.(*msg.RequestClientFundsLockTx)
	if !ok {
		response.Code = msg.Error
		err = communication.ErrUnexpectedRequestType
		return
	}

	filter := fmt.Sprint("uuid='", request.TxUUID.String(), "'", " AND state=", transactions.Initialised)
	tx, err := database.FetchOneTransaction(filter)
	if err != nil {
		if errors.Is(err, ec.ErrDBNoRecords) {
			response.Code = msg.NoCorrespondingTransaction
			return

		} else {
			return err
		}
	}

	var sourceShardMultiSigAddress *btcutil.AddressScriptHash
	sourceShardMultiSigAddress, err = h.validateClientLockFundsTxRequest(tx, request)
	if err != nil {
		if errors.Is(err, ErrValidation) {
			response.Code = msg.InvalidRequest
			return

		} else {
			response.Code = msg.Error
			return
		}
	}

	// It is possible, that transaction has been already picked for the execution,
	// but was delayed due to the absence of the client funds lock transaction.
	// Now it is present, so the transaction should be picked for the next round execution ASAP.
	tx.DelayedUntil = nil

	tx.SourceShardMultiSigAddress = sourceShardMultiSigAddress
	tx.SourceShardMultiSigRedeemScript = request.MultiSigRedeemScript
	tx.ClientFundLockTxHash = request.TxHash[:]
	err = h.updateTransaction(tx)

	if err != nil {
		response.Code = msg.Error
		return
	}

	response.Code = msg.OK
	return
}

func (h *Handler) processFetchCrossShardRequest(req *communication.RequestContext) (err error) {
	response := &msg.ResponseFetchCrossShardTx{}
	defer func() {
		_ = req.Responses.Put(response, ec.ITTL)
	}()

	request, ok := req.Context.(*msg.RequestFetchCrossShardTx)
	if !ok {
		response.Code = msg.Error
		err = communication.ErrUnexpectedRequestType
		return
	}

	filter := fmt.Sprint("uuid='", request.TxUUID.String(), "'")
	tx, err := database.FetchOneTransaction(filter)
	if err != nil {
		response.Code = msg.ErrCodeInternalError
		return
	}

	if tx.State == transactions.Error || tx.State == transactions.LockFundsReturning ||
		tx.State == transactions.LockFundsReturned {
		response.Code = msg.TransactionAborted
		return
	}

	if tx.State == transactions.CrossShardTxPrepared {
		response.Code = msg.OK
		response.CrossShardTx = tx.CrossShardTx
		eadLockFundsTxHash, err := chainhash.NewHash(tx.EAFundsLockTxHash)
		if err != nil {
			response.Code = msg.ErrCodeInternalError
			return err
		}
		response.LockTxHash = eadLockFundsTxHash.String()
		response.LockTxOutNum = tx.EAFundsLockTxOutNum
		return nil
	}

	response.Code = msg.InProgress
	return
}
