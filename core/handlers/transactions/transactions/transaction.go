package transactions

import (
	"errors"
	"fmt"
	"github.com/google/uuid"
	"gitlab.com/jaxnet/core/shard.core/btcutil"
	"gitlab.com/jaxnet/hub/ead/core/blockchain/shards"
	"gitlab.com/jaxnet/hub/ead/core/ec"
	"gitlab.com/jaxnet/hub/ead/core/settings"
	"strings"
	"time"
)

var (
	ErrInvalidTxState = errors.New("invalid tx state")
	ErrInvalidTxData  = errors.New("invalid tx data")
)

const (
	SchemaSenderEADSender   = 0
	SchemaSenderEADReceiver = 1
)

const (
	Initialised            = 0
	ClientFundsLocked      = 1
	EADLockMultiSigCreated = 2
	EAFundsLockingTxIssued = 3
	EAFundsLockingTXMined  = 4
	CrossShardTxPrepared   = 5

	// WARN!
	// [security] [balances health]
	//
	// Final transaction states.
	// All intermediary states of the transaction MUST be placed BEFORE the final transaction state.
	// The only exception here - error codes, which are also indicating the END of transaction's execution,
	// but with the error (error code)
	//
	//
	// todo: tests are needed for this important constraint.
	//
	// Pls, see "(h *Handler) totalFundsFrozen" for more context why it is so important.
	// ---------------------------
	CrossShardTxMined  = 6
	LockFundsReturning = 7
	LockFundsReturned  = 8
	Error              = 255

	LastTXState = CrossShardTxMined
)

var (
	TxStatesGraph = map[uint8][]uint8{
		Initialised:            {ClientFundsLocked, Error},
		ClientFundsLocked:      {EADLockMultiSigCreated, Error},
		EADLockMultiSigCreated: {EAFundsLockingTxIssued, LockFundsReturning, Error},
		EAFundsLockingTxIssued: {EAFundsLockingTXMined, LockFundsReturning, Error},
		EAFundsLockingTXMined:  {CrossShardTxPrepared, LockFundsReturning, Error},
		CrossShardTxPrepared:   {CrossShardTxMined, LockFundsReturning, Error},
		CrossShardTxMined:      {},
		LockFundsReturning:     {LockFundsReturned, Error},
		LockFundsReturned:      {},
		Error:                  {},
	}
)

type Transaction struct {
	ID                                   uint64
	UUID                                 uuid.UUID
	State                                uint8
	Schema                               uint8
	SourceAmount                         btcutil.Amount
	DestinationAmount                    btcutil.Amount
	SourceShardID                        shards.ID
	DestinationShardID                   shards.ID
	SourceShardDestinationAddress        btcutil.Address
	DestinationShardDestinationAddress   btcutil.Address
	SourceShardMultiSigAddress           *btcutil.AddressScriptHash // Could be nil
	DestinationShardMultiSigAddress      *btcutil.AddressScriptHash // Could be nil
	SourceShardMultiSigRedeemScript      []byte
	DestinationShardMultiSigRedeemScript []byte
	Updated                              *time.Time // Could be nil.
	DelayedUntil                         *time.Time // Could be nil.
	ClientPubKey                         []byte     // Could be nil.
	ReceiverPubKey                       []byte     // Could be nil.
	ClientFundLockTxHash                 []byte     // todo: remove this field as redundant.
	ClientFundLockTx                     []byte     // Could be nil.
	EAFundsLockTxHash                    []byte     // Could be nil.
	EAFundsLockTx                        []byte     // Could be nil.
	EAFundsLockTxOutNum                  uint64     // Could be nil
	CrossShardTx                         []byte     // Could be nil.
	SourceShardLockWindow                uint64
	SourceShardMaxBlockHeight            uint64
	DestinationShardLockWindow           uint64
	DestinationShardMaxBlockHeight       uint64
	MaxBlockHeightForCstxSigning         uint64
	ErrorDescription                     *string // Could be nil.
}

func FromDBData(
	id int64,
	binaryUUID [16]byte,
	sourceAmount btcutil.Amount,
	destinationAmount btcutil.Amount,
	sourceShardID shards.ID,
	destinationShardID shards.ID,
	sourceShardDestinationAddress string,
	destinationShardDestinationAddress string,
	sourceShardMultiSigAddress *string,
	destinationShardMultiSigAddress *string,
	sourceShardMultiSigRedeemScript []byte,
	destinationShardMultiSigRedeemScript []byte,
	state int16,
	schema int16,
	errorDescription *string,
	updated *time.Time,
	delayedUntil *time.Time,
	clientPubKey []byte,
	receiverPubKey []byte,
	clientFundsLockingTxHash []byte,
	clientFundsLockingTx []byte,
	eaFundsLockingTxHash []byte,
	eaFundsLockingTxOutNum uint64,
	crossShardTx []byte,
	sourceShardLockWindow uint64,
	sourceShardMaxBlockHeight uint64,
	destinationShardLockWindow uint64,
	destinationShardMaxBlockHeight uint64,
	maxBlockHeightForCstxSigning uint64) (tx *Transaction, err error) {

	tx = &Transaction{}
	tx.ID, err = parseAndValidateID(id)
	if err != nil {
		return
	}

	tx.UUID, err = parseAndValidateUUID(binaryUUID)
	if err != nil {
		return
	}

	tx.SourceAmount, err = parseAndValidateAmount(sourceAmount)
	if err != nil {
		return
	}

	tx.DestinationAmount, err = parseAndValidateAmount(destinationAmount)
	if err != nil {
		return
	}

	tx.SourceShardID = sourceShardID
	tx.DestinationShardID = destinationShardID

	tx.SourceShardDestinationAddress, err = parseBlockchainAddress(sourceShardDestinationAddress)
	if err != nil {
		return
	}

	tx.DestinationShardDestinationAddress, err = parseBlockchainAddress(destinationShardDestinationAddress)
	if err != nil {
		return
	}

	tx.SourceShardMultiSigAddress, err = parseMultiSigBlockchainAddress(sourceShardMultiSigAddress)
	if err != nil {
		return
	}

	tx.DestinationShardMultiSigAddress, err = parseMultiSigBlockchainAddress(destinationShardMultiSigAddress)
	if err != nil {
		return
	}

	tx.State, err = parseAndValidateState(state)
	if err != nil {
		return
	}

	tx.Schema, err = parseAndValidateSchema(schema)
	if err != nil {
		return
	}

	tx.ErrorDescription, err = parseAndValidateErrorDescription(errorDescription)
	if err != nil {
		return
	}

	tx.Updated, err = parseAndValidateTime(updated, false)
	if err != nil {
		return
	}

	tx.DelayedUntil, err = parseAndValidateTime(delayedUntil, true)
	if err != nil {
		return
	}

	tx.SourceShardMultiSigRedeemScript = sourceShardMultiSigRedeemScript
	tx.DestinationShardMultiSigRedeemScript = destinationShardMultiSigRedeemScript

	tx.ClientPubKey = clientPubKey
	tx.ReceiverPubKey = receiverPubKey
	tx.ClientFundLockTxHash = clientFundsLockingTxHash
	tx.ClientFundLockTx = clientFundsLockingTx
	tx.EAFundsLockTxHash = eaFundsLockingTxHash
	tx.EAFundsLockTxOutNum = eaFundsLockingTxOutNum
	tx.CrossShardTx = crossShardTx

	tx.SourceShardLockWindow = sourceShardLockWindow
	tx.SourceShardMaxBlockHeight = sourceShardMaxBlockHeight
	tx.DestinationShardLockWindow = destinationShardLockWindow
	tx.DestinationShardMaxBlockHeight = destinationShardMaxBlockHeight
	tx.MaxBlockHeightForCstxSigning = maxBlockHeightForCstxSigning
	return
}

func (tx *Transaction) SetState(nextState uint8) (err error) {

	///Testing:Injection:SetStateError

	nextValidStates, isPresent := TxStatesGraph[tx.State]
	if !isPresent {
		err = fmt.Errorf(
			"current state is final, no further state changing is allowed: %v",
			ErrInvalidTxState)
		return
	}

	for _, state := range nextValidStates {
		if state == nextState {
			tx.State = nextState
			return
		}
	}

	err = fmt.Errorf(
		"inapropriate sate change prevented, valid states are the next: %v: %v",
		nextValidStates, ErrInvalidTxState)

	return
}

func parseAndValidateID(data int64) (id uint64, err error) {
	if data < 0 {
		err = ErrInvalidTxData
	}

	id = uint64(data)
	return
}

func parseAndValidateUUID(binary [16]byte) (uid uuid.UUID, err error) {
	err = uid.UnmarshalBinary(binary[:])
	if err != nil {
		err = ErrInvalidTxData
		return
	}

	return
}

func parseAndValidateAmount(amount btcutil.Amount) (a btcutil.Amount, err error) {
	if amount < 0 {
		err = ErrInvalidTxData
		return
	}

	a = amount
	return
}

func parseBlockchainAddress(a string) (address btcutil.Address, err error) {
	address, err = btcutil.DecodeAddress(a, settings.NetParams)
	if err != nil {
		err = ErrInvalidTxData
		return
	}

	return
}

func parseMultiSigBlockchainAddress(a *string) (address *btcutil.AddressScriptHash, err error) {
	if a == nil || *a == "" || strings.ToUpper(*a) == "NULL" {
		return // empty address
	}

	addr, err := btcutil.DecodeAddress(*a, settings.NetParams)
	if err != nil {
		err = ErrInvalidTxData
		return
	}

	tmp, ok := addr.(*btcutil.AddressScriptHash)
	if !ok {
		err = ErrInvalidTxData
		return
	}

	address = tmp
	return
}

func parseAndValidateState(state int16) (stateCode uint8, err error) {
	stateCode = uint8(state)

	for availableState, _ := range TxStatesGraph {
		if stateCode == availableState {
			return
		}
	}

	err = fmt.Errorf(
		"unexpected tx state occured. Valid states are: %v (%v, %v)",
		TxStatesGraph, ErrInvalidTxData, ErrInvalidTxState)
	return
}

func parseAndValidateSchema(schema int16) (schemaCode uint8, err error) {
	schemaCode = uint8(schema)
	if schema != SchemaSenderEADReceiver &&
		schema != SchemaSenderEADSender {
		err = fmt.Errorf("unexpected tx schema occured: %w", ErrInvalidTxData)
	}

	return
}

func parseAndValidateTime(t *time.Time, nullIsOK bool) (updated *time.Time, err error) {
	if !nullIsOK && t == nil {
		err = fmt.Errorf("time is required to be not null: %v", ec.ErrNilArgument)
		return
	}

	// todo: add validation (if any)
	updated = t
	return
}

func parseAndValidateErrorDescription(description *string) (parsedDescription *string, err error) {
	if description == nil || *description == "" || strings.ToUpper(*description) == "NULL" {
		return // empty err
	}

	parsedDescription = description
	return
}
