package transactions

import (
	"fmt"
	"gitlab.com/jaxnet/hub/ead/core/blockchain/shards"
	"gitlab.com/jaxnet/hub/ead/core/ec"
	"gitlab.com/jaxnet/hub/ead/core/handlers/limits"
)

// fundsLimitTakingFeesIntoAccount returns hard limit of funds available in a shard,
// but also takes into account, that some amount of this funds would be spent for miners fees.
func (h *Handler) fundsLimitTakingFeesIntoAccount(shard shards.ID) (limit limits.Limit, err error) {
	limit, err = h.limitsHandler.FundsLimit(shard)
	if err != nil {
		err = fmt.Errorf("can't fetch funds limit of the shard: %w", err)
		return
	}

	// It is expected that percentsForFeesFluctuation is enough buffer before upper bound of funds limit.
	// This guard helps to prevent cases, when transactions are using all the funds available in shard for their body,
	// and there is no funds left for paying mining fees.
	limit.Amount = limit.Amount - (limit.Amount / 100 * ec.PercentsForFeesFluctuation)
	return
}
