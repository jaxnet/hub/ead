package transactions

import (
	"gitlab.com/jaxnet/core/shard.core/btcutil"
	"gitlab.com/jaxnet/hub/ead/core/blockchain/shards"
	"gitlab.com/jaxnet/hub/ead/core/logger"
)

// isFundsLimitReached returns true, if total used amount by other operations plus expected amount
// exceeds the amount of free funds in shard.
func (h *Handler) isFundsLimitReached(index shards.Pair, expectedAmount btcutil.Amount) (limitReached bool, err error) {
	limit, err := h.fundsLimitTakingFeesIntoAccount(index.DestinationShard)
	if err != nil {
		return
	}

	totalFrozen, err := h.totalFundsFrozen(index.DestinationShard)
	if err != nil {
		return
	}

	totalIssued, err := h.totalFundsIssuedFromShard(index.DestinationShard, limit.Created)
	if err != nil {
		return
	}

	totalReceived, err := h.totalFundsAcceptedIntoShard(index.DestinationShard, limit.Created)
	if err != nil {
		return
	}

	availableFunds := limit.Amount - totalFrozen - totalIssued + totalReceived

	limitReached = expectedAmount > availableFunds
	if limitReached {
		logger.Log.Warn().Int64(
			"limit", int64(limit.Amount)).Int64(
			"total-frozen", int64(totalFrozen)).Int64(
			"total-issued", int64(totalIssued)).Int64(
			"total-received", int64(totalReceived)).Int64(
			"available-funds", int64(availableFunds)).Msg(
			"Exchange request hit shard limit of funds")
	}

	return
}
