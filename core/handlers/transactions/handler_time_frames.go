package transactions

import "time"

const (
	blockGenerationTimeFrame   = time.Second * 15
	secureFundsMovingTimeFrame = blockGenerationTimeFrame * 100
)

// This sections describes time frames for transactions stages.
// Each one transaction stage has it's own time frame during which it must pass to the next stage, or to be cancelled.
var (
	ClientFundsLockingStageTimeFrame = secureFundsMovingTimeFrame * 2
)
