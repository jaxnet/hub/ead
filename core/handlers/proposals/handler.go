package proposals

import (
	"errors"
	"github.com/davecgh/go-spew/spew"
	"gitlab.com/jaxnet/core/shard.core/btcutil"
	"gitlab.com/jaxnet/hub/ead/core/blockchain/shards"
	"gitlab.com/jaxnet/hub/ead/core/ec"
	"gitlab.com/jaxnet/hub/ead/core/epl"
	"gitlab.com/jaxnet/hub/ead/core/handlers/agents"
	"gitlab.com/jaxnet/hub/ead/core/handlers/limits"
	"gitlab.com/jaxnet/hub/ead/core/logger"
	"gitlab.com/jaxnet/hub/ead/core/messages/clients_messages"
	"gitlab.com/jaxnet/hub/ead/core/settings"
	"math/big"
	"sync"
	"time"
)

var (
	zero = &big.Int{}
)

// ToDo: Load cached proposals on a startup.
//		 Proposals could be cached for some time in persistent storage,
//       so that when daemon would be restarted - proposals registry would populated with some data.
//       This would allow EA to respond to client's requests right after the startup.

type Handler struct {
	ealHandler    *agents.Handler
	limitsHandler *limits.Handler

	epls       map[shards.Pair]*epl.ExchangeAgentsProposalsList
	eplRWMutex sync.RWMutex

	//reserves        map[shards.ID]*big.Int
	//reservesRWMutex sync.RWMutex

	outEventsProposalChanged chan *epl.ExchangeAgentProposal

	outEventsOwnProposals chan *epl.OwnExchangeAgentProposals
}

func NewEPLHandler(ealHandler *agents.Handler, limitsHandler *limits.Handler) (handler *Handler, err error) {
	ec.InterruptOnNilArgument(ealHandler)

	handler = &Handler{
		epls: make(map[shards.Pair]*epl.ExchangeAgentsProposalsList),

		ealHandler:    ealHandler,
		limitsHandler: limitsHandler,

		outEventsProposalChanged: make(chan *epl.ExchangeAgentProposal, 16),
		outEventsOwnProposals:    make(chan *epl.OwnExchangeAgentProposals, 16),
	}

	return
}

func (handler *Handler) Run() (flow <-chan error) {
	errorsFlow := make(chan error)

	// todo: add implementation here

	return errorsFlow
}

//func (handler *Handler) SetReservedAmount(shardID, reserve string) (err error) {
//	handler.eplRWMutex.Lock()
//	defer handler.eplRWMutex.Unlock()
//
//	normalizedShardID, err := handler.validateAndNormalizeShardID(shardID)
//	if err != nil {
//		return
//	}
//
//	normalizedReserve, err := handler.validateAndNormalizeReservedAmount(reserve)
//	if err != nil {
//		return
//	}
//
//	err = handler.updateReservedAmount(normalizedShardID, normalizedReserve)
//	return
//}

//func (handler *Handler) SetFee(sourceShardID, destinationShardID, fixedFee, percentsFee, expire string) (err error) {
//	handler.eplRWMutex.Lock()
//	defer handler.eplRWMutex.Unlock()
//
//	normalizedSourceShardID, err := handler.validateAndNormalizeShardID(sourceShardID)
//	if err != nil {
//		return
//	}
//
//	normalizedDestinationShardID, err := handler.validateAndNormalizeShardID(destinationShardID)
//	if err != nil {
//		return
//	}
//
//	normalizedFixedFee, err := handler.validateAndNormalizeFixedFee(fixedFee)
//	if err != nil {
//		return
//	}
//
//	normalizedPercentsFee, err := handler.validateAndNormalizePercentsFee(percentsFee)
//	if err != nil {
//		return
//	}
//
//	normalizeExpireTimestamp, err := handler.validateAndNormalizeExpireTTL(expire)
//	if err != nil {
//		return
//	}
//
//	err = handler.GenerateNextProposal(
//		shards.NewPairIndex(normalizedSourceShardID, normalizedDestinationShardID),
//		normalizedFixedFee, normalizedPercentsFee, normalizeExpireTimestamp)
//	return
//}

func (handler *Handler) HandleNetworkReceivedProposal(proposal *epl.ExchangeAgentProposal) {
	handler.eplRWMutex.Lock()
	defer handler.eplRWMutex.Unlock()

	isValid, err := handler.validateProposal(proposal)
	if err != nil {
		return
	}

	if !isValid {
		err = ErrInvalidProposal
		return
	}

	handler.setProposal(proposal.Index, proposal)
	return
}

func (handler *Handler) HandleNetworkReceivedProposalsRequestFromEAD(proposalGetRequest *epl.ExchangeAgentProposalGetRequest) {
	handler.eplRWMutex.Lock()
	defer handler.eplRWMutex.Unlock()

	isValid, err := handler.validateProposalGetRequest(proposalGetRequest)
	if err != nil {
		return
	}

	if !isValid {
		err = ErrInvalidProposal
		return
	}

	err = handler.sendOwnProposalsToEAD(proposalGetRequest.ExchangeAgentID)
	return
}

func (handler *Handler) HandleNetworkReceiveOwnProposalsFromEAD(incomingEADProposals *epl.OwnExchangeAgentProposals) {
	handler.eplRWMutex.Lock()
	defer handler.eplRWMutex.Unlock()

	for _, proposal := range incomingEADProposals.Proposals {
		isValid, err := handler.validateProposal(proposal)
		if err != nil {
			logger.Log.Debug().Msg("Can't validate proposal")
			continue
		}

		if !isValid {
			logger.Log.Debug().Msg("Proposal is invalid")
			continue
		}

		handler.setProposal(proposal.Index, proposal)
	}
	return
}

func (handler *Handler) HandleNetworkReceivedProposalsRequest(req *clients_messages.ProposalsRequest) (list *epl.ExchangeAgentsProposalsList, err error) {
	handler.eplRWMutex.Lock()
	defer handler.eplRWMutex.Unlock()

	// todo: [security]
	//
	// WARN!
	// Potential security issue here.
	// Code here uses the method handler.getProposalsList to get current list of proposals available.
	// In case if this list would not be found - the error would be generated in current method,
	// which would be converted into the empty response on the upper level (client communication).
	//
	// There is another method handler.getProposalsListOrCreate, which creates empty list
	// in case if there is no one on the moment of accessing it by the index (pair of shards IDs),
	// but THIS METHOD MUST NOT BE USED HERE, because it doing so the attacker is provided with possibility
	// to request huge amount of the fake indexes (fake pairs of shards) and drain the memory of the daemon.
	list, err = handler.getProposalsList(req.Index)
	return
}

func (handler *Handler) HandleNetworkReceivedProposalsHttpRequest(pair shards.Pair) (list *epl.ExchangeAgentsProposalsList, err error) {
	handler.eplRWMutex.Lock()
	defer handler.eplRWMutex.Unlock()

	// todo: [security]
	//
	// WARN!
	// Potential security issue here.
	// Code here uses the method handler.getProposalsList to get current list of proposals available.
	// In case if this list would not be found - the error would be generated in current method,
	// which would be converted into the empty response on the upper level (client communication).
	//
	// There is another method handler.getProposalsListOrCreate, which creates empty list
	// in case if there is no one on the moment of accessing it by the index (pair of shards IDs),
	// but THIS METHOD MUST NOT BE USED HERE, because it doing so the attacker is provided with possibility
	// to request huge amount of the fake indexes (fake pairs of shards) and drain the memory of the daemon.
	list, err = handler.getProposalsList(pair)
	return
}

func (handler *Handler) OnProposalChange() <-chan *epl.ExchangeAgentProposal {
	return handler.outEventsProposalChanged
}

func (handler *Handler) OnOwnProposalsRequest() <-chan *epl.OwnExchangeAgentProposals {
	return handler.outEventsOwnProposals
}

func (handler *Handler) GenerateNextProposal(
	index shards.Pair, fixedFee btcutil.Amount, percentsFee float64, expireTTL time.Time) (err error) {

	limitAvailable, err := handler.limitsHandler.FundsLimit(index.DestinationShard)
	if err != nil {
		return
	}

	if limitAvailable.Amount == 0 {
		err = ec.ErrNoReserveIsAvailable
		return
	}

	eaRegNumberInCurrentShard, err := handler.ealHandler.RegNumberInShard(index.SourceShard)
	if err != nil {
		return
	}

	var nextProposalSequenceAnchor uint32 = 0
	currentProposal, proposalErr := handler.getOwnProposal(index, eaRegNumberInCurrentShard)
	if proposalErr == nil {
		nextProposalSequenceAnchor = currentProposal.SequenceAnchor + 1

	}

	// todo: current proposals architecture operates with big.Int in the time
	// 		 when the rest codebase operates with int64 for amount representation.
	// 	     Proposals design must be synchronised with the rest codebase.
	// 		 https://gitlab.com/jaxnet/hub/ead/-/issues/47

	nextProposal := &epl.ExchangeAgentProposal{
		ExchangeAgentID: eaRegNumberInCurrentShard,
		ReservedAmount:  (&big.Int{}).SetInt64(int64(limitAvailable.Amount)), // todo: fixme.
		Expire:          expireTTL,
		SequenceAnchor:  nextProposalSequenceAnchor,
		FeeFixedAmount:  (&big.Int{}).SetInt64(int64(fixedFee)), // todo: fixme.
		FeePercents:     percentsFee,
		Index:           index,
	}

	err = nextProposal.Sign()
	if err != nil {
		return
	}

	handler.setProposal(index, nextProposal)

	handler.outEventsProposalChanged <- nextProposal
	return
}

func (handler *Handler) getOwnProposal(index shards.Pair, regNumber uint64) (proposal *epl.ExchangeAgentProposal, err error) {
	ec.InterruptOnNilArgument(index)

	list := handler.getProposalsListOrCreate(index)
	proposal, isPresent := list.Proposals[regNumber]
	if !isPresent {
		err = ec.ErrProposalIsAbsent
		return
	}

	return
}

func (handler *Handler) setProposal(index shards.Pair, proposal *epl.ExchangeAgentProposal) {
	ec.InterruptOnNilArgument(index)
	ec.InterruptOnNilArgument(proposal)

	list := handler.getProposalsListOrCreate(index)
	list.Proposals[proposal.ExchangeAgentID] = proposal

	if settings.Conf.Debug {
		spew.Config.DisablePointerAddresses = true
		spew.Config.DisableCapacities = true

		logger.Log.Debug().Str(
			"proposal", spew.Sdump(proposal)).Msg(
			"Applied proposal")
	}
}

func (handler *Handler) getProposalsList(index shards.Pair) (list *epl.ExchangeAgentsProposalsList, err error) {
	ec.InterruptOnNilArgument(index)

	list, isPresent := handler.epls[index]
	if !isPresent {
		err = errors.New("no EPLs set ofr the specified pair of shards")
		return
	}

	return
}

func (handler *Handler) getProposalsListOrCreate(index shards.Pair) (list *epl.ExchangeAgentsProposalsList) {
	ec.InterruptOnNilArgument(index)

	list, err := handler.getProposalsList(index)
	if err != nil {
		list = epl.NewExchangeAgentsProposalsList()
		handler.epls[index] = list
	}

	return
}

//func (handler *Handler) getReservedAmount(shardID *shards.ID) (reserve *big.Int, err error) {
//	r, isPresent := handler.reserves[*shardID]
//	if !isPresent {
//		err = errors.New("no corresponding reserve found to the specified shard")
//		return
//	}
//
//	// Return copy of the reserve, and not the direct pointer.
//	reserve = &big.Int{}
//	reserve.Set(r)
//	return
//}

//func (handler *Handler) updateReservedAmount(shardID shards.ID, reserve *big.Int) (err error) {
//	// todo: Add validation step.
//	//		 Reserved amount must be less or equal to the rest of funds on the EA's balance.
//
//	handler.reservesRWMutex.Lock()
//	{
//		handler.reserves[shardID] = reserve
//	}
//	handler.reservesRWMutex.Unlock()
//
//	// todo: Shrink current proposal in case if new reserve is less than one,
//	//		 that is declared in last generated proposal.
//
//	return
//}

func (handler *Handler) sendOwnProposalsToEAD(exchangeAgentID uint64) (err error) {

	ownEpls := &epl.OwnExchangeAgentProposals{RequesterEAD: exchangeAgentID}

	for key, _ := range handler.epls {
		eaRegNumberInCurrentShard, err := handler.ealHandler.RegNumberInShard(key.SourceShard)
		if err != nil {
			continue
		}

		currentProposal, proposalErr := handler.getOwnProposal(key, eaRegNumberInCurrentShard)
		if proposalErr != nil {
			continue
		}
		ownEpls.Proposals = append(ownEpls.Proposals, currentProposal)
	}

	if len(ownEpls.Proposals) == 0 {
		return
	}

	handler.outEventsOwnProposals <- ownEpls

	return
}
