package proposals

import (
	"gitlab.com/jaxnet/hub/ead/core/blockchain/shards"
	"gitlab.com/jaxnet/hub/ead/core/ec"
	"math/big"
	"strconv"
	"strings"
	"time"
)

var (
	tmp             = &big.Float{}
	precision, _, _ = tmp.Parse("100000000", 10)
)

func (handler *Handler) validateAndNormalizeShardID(sid string) (id shards.ID, err error) {
	if sid == "" {
		err = ec.ErrInvalidShardID
		return
	}

	i, err := strconv.ParseInt(sid, 10, 32)
	if err != nil {
		return
	}
	id = shards.ID(i)

	currentEAIsPresentInReceivedShard := handler.ealHandler.CurrentEAIsPresentInShard(id)
	if !currentEAIsPresentInReceivedShard {
		err = ec.ErrShardNotFound
		return
	}

	return
}

func (handler *Handler) validateAndNormalizeFixedFee(fee string) (normalized *big.Int, err error) {
	if fee == "" {
		err = ec.ErrInvalidFixedFee
		return
	}
	fee = strings.ReplaceAll(fee, ",", ".")

	f := &big.Float{}
	f, ok := f.SetString(fee)
	if !ok {
		err = ec.ErrInvalidFixedFee
		return
	}

	f = f.Mul(f, precision)
	normalized = &big.Int{}
	normalized, _ = f.Int(normalized)
	return
}

func (handler *Handler) validateAndNormalizePercentsFee(fee string) (normalized float64, err error) {
	if fee == "" {
		err = ec.ErrInvalidPercentsFee
		return
	}

	normalized, err = strconv.ParseFloat(fee, 64)
	if err != nil {
		err = ec.ErrInvalidPercentsFee
		return
	}

	if normalized > 100 {
		err = ec.ErrInvalidPercentsFee
		return
	}

	return
}

func (handler *Handler) validateAndNormalizeExpireTTL(expire string) (normalized time.Time, err error) {
	if expire == "" {
		err = ec.ErrInvalidExpireTTL
		return
	}

	normalized, err = time.Parse(time.RFC3339, expire)
	if err != nil {
		err = ec.ErrInvalidExpireTTL
		return
	}

	if normalized.Sub(time.Now()) > time.Hour*24*3 {
		err = ec.ErrInvalidExpireTTL
		return
	}

	return
}

func (handler *Handler) validateAndNormalizeReservedAmount(reserve string) (normalized *big.Int, err error) {
	if reserve == "" {
		err = ec.ErrInvalidFixedFee
		return
	}
	reserve = strings.ReplaceAll(reserve, ",", ".")

	f := &big.Float{}
	f, ok := f.SetString(reserve)
	if !ok {
		err = ec.ErrInvalidFixedFee
		return
	}

	f = f.Mul(f, precision)
	normalized = &big.Int{}
	normalized, _ = f.Int(normalized)
	return
}
