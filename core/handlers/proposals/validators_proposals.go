package proposals

import (
	"errors"
	"gitlab.com/jaxnet/hub/ead/core/epl"
	"gitlab.com/jaxnet/hub/ead/core/utils"
	"time"
)

var (
	ErrInvalidProposal = errors.New("proposal does not passed validation")
)

func (handler *Handler) validateProposal(proposal *epl.ExchangeAgentProposal) (isValid bool, err error) {

	if proposal.Expire.Before(time.Now()) {
		// todo: return validation error here
		return false, nil
	}

	if proposal.ReservedAmount.Cmp(zero) != 1 {
		// todo: return validation error here
		return false, nil
	}

	if proposal.Expire.Before(utils.UTCNow()) {
		// todo: return validation error here
		return false, nil
	}

	if handler.ealHandler.EAIsPresentInShard(proposal.Index.SourceShard, proposal.ExchangeAgentID) == false {
		// By the data of current EA, source shard is absent,
		// or no EA with such regID is registered there.
		// todo: return validation error here
		return false, nil
	}

	if handler.ealHandler.EAIsPresentInShard(proposal.Index.DestinationShard, proposal.ExchangeAgentID) == false {
		// By the data of current EA, destination shard is absent,
		// or no EA with such regID is registered there.
		// todo: return validation error here
		return false, nil
	}

	data, err := proposal.MarshalAllExceptSignature()
	if err != nil {
		return false, nil
	}

	if handler.ealHandler.EASigIsValid(
		proposal.Index.DestinationShard, proposal.ExchangeAgentID, proposal.Signature, data) == false {
		// todo: return validation error here
		return false, nil
	}

	currentProposals := handler.getProposalsListOrCreate(proposal.Index)
	currentProposal, isPresent := currentProposals.Proposals[proposal.ExchangeAgentID]
	if !isPresent {
		return true, nil
	}

	if currentProposal.SequenceAnchor > proposal.SequenceAnchor {
		// Current proposal is newer than received one.
		// todo: return validation error here
		return false, nil
	}

	// todo: spawn gorutine to kill this proposal when it becames outdated.
	return true, nil
}

func (handler *Handler) validateProposalGetRequest(proposalGetRequest *epl.ExchangeAgentProposalGetRequest) (isValid bool, err error) {
	// todo : add method in handler for checking AED presence independent from shardID

	//data, err := proposalGetRequest.MarshalAllExceptSignature()
	//if err != nil {
	//	return false, nil
	//}

	//if handler.ealHandler.EASigIsValid(
	//	proposal.Index.DestinationShard, proposal.ExchangeAgentID, proposal.Signature, data) == false {
	//	// todo: return validation error here
	//	return false, nil
	//}

	return true, nil
}
