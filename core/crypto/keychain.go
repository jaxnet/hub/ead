package crypto

import (
	"encoding/hex"
	"errors"
	"gitlab.com/jaxnet/core/shard.core/btcec"
	"gitlab.com/jaxnet/core/shard.core/btcutil"
	"gitlab.com/jaxnet/core/shard.core/cmd/tx-gatling/txmodels"
	"gitlab.com/jaxnet/core/shard.core/cmd/tx-gatling/txutils"
	"gitlab.com/jaxnet/core/shard.core/txscript"
	"gitlab.com/jaxnet/core/shard.core/types/btcjson"
	"gitlab.com/jaxnet/core/shard.core/types/chaincfg"
	"gitlab.com/jaxnet/core/shard.core/types/chainhash"
	"gitlab.com/jaxnet/core/shard.core/types/wire"
	"gitlab.com/jaxnet/hub/ead/core/blockchain/shards"
	"gitlab.com/jaxnet/hub/ead/core/settings"
	"golang.org/x/crypto/blake2b"
	"io/ioutil"
)

const (
	pKeyFilename = "pkey.key"
)

var (
	pKey    *btcec.PrivateKey
	pubKey  *btcec.PublicKey
	pKeyHex string
)

func InitKeyChain() (err error) {
	data, err := ioutil.ReadFile(pKeyFilename)
	if err != nil {
		return
	}

	pKeyHex = string(data)
	pkBytes, err := hex.DecodeString(pKeyHex)
	if err != nil {
		return
	}

	pKey, pubKey = btcec.PrivKeyFromBytes(btcec.S256(), pkBytes)
	return
}

func PubKey() *btcec.PublicKey {

	return pubKey
}

func Sign(data []byte) (sig *btcec.Signature, err error) {
	hash := CalculateHash(data)
	sig, err = pKey.Sign(hash[:])
	return
}

func Verify(data []byte, sig *btcec.Signature) bool {
	hash := CalculateHash(data)
	return sig.Verify(hash[:], pubKey)
}

func VerifyExternalPubKey(data []byte, sig *btcec.Signature, key *btcec.PublicKey) bool {
	hash := CalculateHash(data)
	return sig.Verify(hash[:], key)
}

func PubKeyMatchOurOwn(key *btcec.PublicKey) bool {
	return pubKey.IsEqual(key)
}

func CalculateHash(data []byte) (h Hash) {
	hash := blake2b.Sum256(data)
	copy(h[:], hash[:])
	return
}

// PoC Limitation: HD Wallet is recommended here.
// At the moment, this method returns the same address fro each one operation.
// This kind of activity could lead to some security issues.
// todo: [security] integrate HD walled here.
func Address() (address *btcutil.AddressPubKeyHash, err error) {
	kp, err := txutils.NewKeyData(pKeyHex, settings.NetParams)
	if err != nil {
		return
	}

	address = kp.AddressPubKey.AddressPubKeyHash()
	return
}

func MakeMultiSigLockScript(keys []string, nRequired int, refundDeferringPeriod int32, net *chaincfg.Params) (*MultiSigAddress, error) {
	keysPrecious := make([]*btcutil.AddressPubKey, len(keys))

	// The address list will made up either of addresses (pubkey hash), for
	// which we need to look up the keys in wallet, straight pubkeys, or a
	// mixture of the two.
	for i, pubKey := range keys {
		// try to parse as pubkey address
		rawPK, err := hex.DecodeString(pubKey)
		if err != nil {
			return nil, err
		}

		addr, err := btcutil.NewAddressPubKey(rawPK, net)
		if err != nil {
			return nil, err
		}

		keysPrecious[i] = addr
	}

	script, err := txscript.MultiSigLockScript(keysPrecious, nRequired, keysPrecious[0], refundDeferringPeriod)
	if err != nil {
		return nil, err
	}
	address, err := btcutil.NewAddressScriptHash(script, net)
	if err != nil {
		// above is a valid script, shouldn't happen.
		return nil, err
	}

	return &MultiSigAddress{
		Address:         address.EncodeAddress(),
		RedeemScript:    hex.EncodeToString(script),
		RawRedeemScript: script,
	}, nil
}

func TXManWithKeys(txMan *txutils.TxMan) (keysTxMan *txutils.TxMan, err error) {
	eadKP, err := txutils.NewKeyData(pKeyHex, settings.NetParams)
	if err != nil {
		return
	}

	keysTxMan = txMan.WithKeys(eadKP)
	return
}

func NewTxMan(shardConf *settings.ShardConfig, networkName string) (txMan *txutils.TxMan, err error) {
	txMan, err = txutils.NewTxMan(txutils.ManagerCfg{
		Net:     networkName,
		ShardID: uint32(shardConf.ID),
		RPC: txutils.NodeRPC{
			Host: shardConf.RPC.Net.Interface(),
			User: shardConf.RPC.Credentials.User,
			Pass: shardConf.RPC.Credentials.Pass,
		},
		PrivateKey: pKeyHex,
	})
	if err != nil {
		return
	}
	txMan = txMan.ForShard(uint32(shardConf.ID))
	return
}

func CreateCrossShardTx(
	sourceShardID shards.ID,
	destinationShardID shards.ID,
	sourceShardTxMan *txutils.TxMan,
	destinationShardTxMan *txutils.TxMan,
	clientUTXOIndexer txutils.NewUTXOProvider,
	sourceShardAmount btcutil.Amount,
	destinationShardAmount btcutil.Amount,
	sourceShardDestinationAddress string,
	destinationShardDestinationAddress string,
	sourceShardUTXO *txmodels.UTXO,
	destinationShardUTXO *txmodels.UTXO,
	multiSigRedeemScript string) (crossShardTx *txmodels.SwapTransaction, err error) {

	kp, err := txutils.NewKeyData(pKeyHex, settings.NetParams)
	if err != nil {
		return
	}

	address := kp.AddressPubKey.AddressPubKeyHash()

	networkFeeFunc := func(shardID uint32) (int64, int64, error) {
		var (
			fee *btcjson.ExtendedFeeFeeResult
			err error
		)
		if shardID == uint32(sourceShardID) {
			fee, err = sourceShardTxMan.RPC().GetExtendedFee()
		} else if shardID == uint32(destinationShardID) {
			fee, err = destinationShardTxMan.RPC().GetExtendedFee()
		} else {
			err = errors.New("Invalid shard ID for getting fees")
		}
		if err != nil {
			return 0, 0, err
		}
		return 0, int64(fee.Moderate.SatoshiPerB), nil
	}

	tx, err := txutils.NewTxBuilder(chaincfg.NetName(settings.NetParams.Name)).
		SwapTx().
		SetSenders(address.EncodeAddress()).
		SetUTXOProvider(clientUTXOIndexer).
		AddRedeemScripts(multiSigRedeemScript).
		SetChangeDestination(address.EncodeAddress()).
		SetDestinationWithUTXO(sourceShardDestinationAddress, int64(sourceShardAmount), txmodels.UTXORows{*sourceShardUTXO}).
		SetDestinationWithUTXO(destinationShardDestinationAddress, int64(destinationShardAmount), txmodels.UTXORows{*destinationShardUTXO}).
		IntoTx(networkFeeFunc, kp)
	if err != nil {
		return
	}

	crossShardTx, err = sourceShardTxMan.NewSwapTx(map[string]txmodels.UTXO{
		sourceShardDestinationAddress:      *sourceShardUTXO,
		destinationShardDestinationAddress: *destinationShardUTXO,
	}, false)
	if err != nil {
		return
	}

	crossShardTx.RawTX = tx
	crossShardTx.SignedTx = txutils.EncodeTx(tx)
	crossShardTx.TxHash = tx.TxHash().String()
	return
}

func CreateRefundingTx(txBuilder txutils.TxBuilder, fee int64) (refundingTx *wire.MsgTx, err error) {
	eadKP, err := txutils.NewKeyData(pKeyHex, settings.NetParams)
	if err != nil {
		return
	}
	refundingTx, err = txBuilder.IntoTx(func(shardID uint32) (int64, int64, error) { return fee, 0, nil }, eadKP)
	return
}

func SignDataForIndexer(data string) (pubKey, sig string, err error) {
	hash := chainhash.DoubleHashB([]byte(data))
	signature, err := pKey.Sign(hash)
	if err != nil {
		return
	}

	pubKey = hex.EncodeToString(PubKey().SerializeUncompressed())
	sig = hex.EncodeToString(signature.Serialize())
	return
}
