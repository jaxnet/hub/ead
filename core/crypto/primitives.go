package crypto

// todo: replace by chain hash from core.
type Hash = [32]byte

type MultiSigAddress struct {
	Address         string `json:"address"`
	RedeemScript    string `json:"redeemScript"`
	RawRedeemScript []byte `json:"-"`
}
