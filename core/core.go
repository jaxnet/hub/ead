package core

import (
	"fmt"
	"github.com/davecgh/go-spew/spew"
	"gitlab.com/jaxnet/hub/ead/core/blockchain"
	"gitlab.com/jaxnet/hub/ead/core/database"
	"gitlab.com/jaxnet/hub/ead/core/ec"
	"gitlab.com/jaxnet/hub/ead/core/handlers/agents"
	"gitlab.com/jaxnet/hub/ead/core/handlers/blocks"
	"gitlab.com/jaxnet/hub/ead/core/handlers/limits"
	"gitlab.com/jaxnet/hub/ead/core/handlers/proposals"
	"gitlab.com/jaxnet/hub/ead/core/handlers/transactions"
	"gitlab.com/jaxnet/hub/ead/core/interfaces/http"
	"gitlab.com/jaxnet/hub/ead/core/interfaces/tcp/clients"
	"gitlab.com/jaxnet/hub/ead/core/interfaces/tcp/exchange_agents"
	"gitlab.com/jaxnet/hub/ead/core/logger"
	"time"
)

type Core struct {
	blocksHandler       *blocks.Handler
	limitsHandler       *limits.Handler
	ealHandler          *agents.Handler
	eplHandler          *proposals.Handler
	txsHandler          *transactions.Handler
	easInterface        *exchange_agents.TCPInterface
	clientsInterface    *clients.TCPInterface
	publicHttpInterface *http.PublicHTTPInterface
}

func NewCore() (core *Core, err error) {
	logger.Log.Info().Msg("Initializing database")
	{
		err = database.InitConnectionsPool()
		ec.InterruptOnError(err)
	}
	logger.Log.Info().Msg("Database initialized successfully")

	logger.Log.Info().Msg("Initializing database schema")
	{
		err = database.EnsureSchema()
		ec.InterruptOnError(err)
	}
	logger.Log.Info().Msg("Database schema initialized successfully")

	limitsHandler, err := limits.New()
	ec.InterruptOnError(err)

	bcConnector, err := blockchain.NewConnector()
	ec.InterruptOnError(err)

	blocksHandler, err := blocks.New(bcConnector)
	ec.InterruptOnError(err)

	ealHandler, err := agents.NewEALHandler(bcConnector)
	ec.InterruptOnError(err)

	eplHandler, err := proposals.NewEPLHandler(ealHandler, limitsHandler)
	ec.InterruptOnError(err)

	txsHandler, err := transactions.New(limitsHandler, bcConnector, blocksHandler)
	ec.InterruptOnError(err)

	easInterface, err := exchange_agents.NewExchangeAgentsTCPInterface()
	ec.InterruptOnError(err)

	clientsInterface, err := clients.NewTCPInterface(eplHandler, txsHandler)
	ec.InterruptOnError(err)

	publicHttpInterface, err := http.InitHTTPPublicInterface(eplHandler, txsHandler)
	ec.InterruptOnError(err)

	core = &Core{
		blocksHandler:       blocksHandler,
		limitsHandler:       limitsHandler,
		ealHandler:          ealHandler,
		eplHandler:          eplHandler,
		txsHandler:          txsHandler,
		easInterface:        easInterface,
		clientsInterface:    clientsInterface,
		publicHttpInterface: publicHttpInterface,
	}

	logger.Log.Info().Msg("Core initialization finished")
	return
}

func (core *Core) RunUntilError() (err error) {
	err = http.InitHHTPInterface(core.limitsHandler)
	if err != nil {
		return
	}

	select {
	case err = <-core.runInternalEventsDispatching():
	case err = <-core.limitsHandler.Run():
	case err = <-core.ealHandler.Run():
	case err = <-core.eplHandler.Run():
	case err = <-core.txsHandler.Run():
	case err = <-core.clientsInterface.Run():
	case err = <-core.easInterface.Run():
	case err = <-core.blocksHandler.Run():
	case err = <-http.RunHTTPInterface():
	case err = <-core.publicHttpInterface.Run():
	}

	// toto : correct finishing all processes
	return
}

func (core *Core) runInternalEventsDispatching() (errors <-chan error) {
	errorsFlow := make(chan error)

	go func() {
		for {
			select {
			case limitsEvent := <-core.limitsHandler.Events.Subscribe():
				{
					switch limitsEvent.(type) {
					case *limits.EventFeeSet:
						core.handleFeeUpdate(limitsEvent.(*limits.EventFeeSet))

					case *limits.EventFeeRestored:
						core.handleFeeRestored(limitsEvent.(*limits.EventFeeRestored))

					default:
						panic(fmt.Sprint(
							"unexpected limits handler event occurred: ", spew.Sdump(limitsEvent)))
					}
				}

			case proposal := <-core.eplHandler.OnProposalChange():
				core.easInterface.BroadcastProposal(proposal)

			case ealChange := <-core.ealHandler.OnEALChanged():
				core.easInterface.SetAgentsAddresses(ealChange)

			case incomingProposal := <-core.easInterface.OnIncomingProposal():
				core.eplHandler.HandleNetworkReceivedProposal(incomingProposal)

			case incomingProposalGetRequest := <-core.easInterface.OnIncomingProposalGetRequest():
				core.eplHandler.HandleNetworkReceivedProposalsRequestFromEAD(incomingProposalGetRequest)

			case ownProposals := <-core.eplHandler.OnOwnProposalsRequest():
				core.easInterface.RespondOwnProposals(ownProposals)

			case ownProposals := <-core.easInterface.OnIncomingOwnProposal():
				core.eplHandler.HandleNetworkReceiveOwnProposalsFromEAD(ownProposals)
			}
		}
	}()

	return errorsFlow
}

func (core *Core) handleFeeUpdate(e *limits.EventFeeSet) {
	err := core.eplHandler.GenerateNextProposal(e.Shards, e.Fee.Fixed, e.Fee.Percents, e.Fee.Expired)
	if err != nil {
		err = fmt.Errorf("can't handle fees update event: can't generate next exchange proposal: %w", err)
		logger.Log.Warn().Msg(err.Error())
		return
	}
}

func (core *Core) handleFeeRestored(e *limits.EventFeeRestored) {
	// When fees configuration is restored from the database -
	// the exchange proposal must be actualized too.
	//
	// Currently, the logic is the same as when fees is updated by the admin.
	event := &limits.EventFeeSet{
		Shards: e.Shards,
		Fee:    e.Fee,
	}

	// todo: There is a data race present.
	//		 Limits handler restores fees on startup and emits corresponding event,
	//		 that triggers proposals update, but the proposals handler itself could be not initialised yet,
	//		 cause it starts and works in parallel to the limits handler and has dependencies to other components
	//		 (exchange agents handler) that could be not initialised yet too.
	//		 So from start to start, the proposals are not restored on the startup,
	//		 (that is a serious bug), just cause proposals handler does not finished its validation yet.
	//	     Possible solution here is to refactor proposals handler (and all dependant components too)
	//	     to the signals/slots model AND make them not reporting data (by internal getters)
	//	     until proper initialisation (principle initialisation first).
	//	     This could be achieved by moving initialisation logic BEFORE starting internal events loop.
	//	     As a result, in case if some component's initialisation would take more than expected -
	//	     all other would wait.
	//		 https://gitlab.com/jaxnet/hub/ead/-/issues/48
	time.Sleep(time.Second * 2)

	core.handleFeeUpdate(event)
}
