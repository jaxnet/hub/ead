package messages

import "encoding"

type Request interface {
	encoding.BinaryMarshaler
	encoding.BinaryUnmarshaler

	TypeID() []byte
}
