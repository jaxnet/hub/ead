package clients_messages

import (
	"gitlab.com/jaxnet/hub/ead/core/blockchain/shards"
	"gitlab.com/jaxnet/hub/ead/core/marshalling"
	"math/big"
)

type ProposalsRequest struct {
	Index  shards.Pair
	Amount *big.Int

	// PoC limitation: filtering parameters
	// Client could already have some proposal fetched from another EAs.
	// In this case it seems reasonable to add some additional filtering parameters,
	// that would allow to skip fetching of the proposals that are definitively worse than currently present.
	// (some kind of filtering on the EA's side using the info provided by the client).
}

func NewProposalsRequest(index shards.Pair, requiredAmount *big.Int) (message *ProposalsRequest, err error) {
	message = &ProposalsRequest{
		Index: index,

		// this trick is used to copy the value, instead of carrying the pointer.
		Amount: requiredAmount.Abs(requiredAmount),
	}
	return
}

func (message *ProposalsRequest) MarshalBinary() (data []byte, err error) {
	encoder := marshalling.NewEncoder()

	indexBinary, err := message.Index.MarshalBinary()
	if err != nil {
		return
	}

	err = encoder.PutVariadicDataWithByteHeader(indexBinary)
	if err != nil {
		return
	}

	amountBinary := message.Amount.Bytes()
	err = encoder.PutVariadicDataWithByteHeader(amountBinary)
	if err != nil {
		return
	}

	data = encoder.CollectDataAndReleaseBuffers()
	return
}

func (message *ProposalsRequest) UnmarshalBinary(data []byte) (err error) {
	decoder := marshalling.NewDecoder(data)

	indexBinary, err := decoder.GetDataSegmentWithByteHeader()
	if err != nil {
		return
	}

	err = message.Index.UnmarshalBinary(indexBinary)
	if err != nil {
		return
	}

	amountBinary, err := decoder.GetDataSegmentWithByteHeader()
	if err != nil {
		return
	}

	message.Amount = &big.Int{}
	message.Amount.SetBytes(amountBinary)

	return
}
