package clients_messages

const (
	ProtocolVersionFieldSize = 1

	FieldSize_MessageType   = 2
	FieldOffset_MessageType = 0

	FieldSize_MessageSize   = 2
	FieldOffset_MessageSize = FieldOffset_MessageType + FieldSize_MessageType
)

const (
	// 0..128 are reserved for system purpose.

	ReqProposals          = 128
	RespProposals         = 129
	ReqInitExchange       = 130
	RespInitExchange      = 131
	ReqClientFundsLockTX  = 132
	RespClientFundsLockTx = 133
	ReqEADFundsLockingTX  = 134
	RespEADFundsLockingTX = 135
	ReqCrossShardTx       = 136
	RespCrossShardTx      = 137
	ReqFetchCrossShardTx  = 138
	RespFetchCrossShardTx = 139
)

const (
	ErrorCodeNotEnoughFunds       = 1
	ErrCodeInternalError          = 253
	ErrorCodeTemporaryUnavailable = 254
)

const (
	DefaultProtocolVersion = 1
)
