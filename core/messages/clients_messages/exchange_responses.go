package clients_messages

import (
	"github.com/google/uuid"
	"gitlab.com/jaxnet/hub/ead/core/marshalling"
	"gitlab.com/jaxnet/hub/ead/core/messages"
)

const (
	// Common segment of responses
	OK                 = 0
	TransactionAborted = 1
	InProgress         = 2

	// Init Exchange
	InitExchangeCodeInsufficientFunds = 100

	// EAD Funds Lock Request
	EADFundsLockNoFundsLocked = 100

	FollowTheProtocol          = 252
	InvalidRequest             = 253
	NoCorrespondingTransaction = 254
	Error                      = 255
)

type ResponseInitExchange struct {
	messages.BaseSignedResponse

	Code   uint8
	TxUUID uuid.UUID

	// Amount of blocks that are required by the EAD to agree to proceed the operation.
	// These parameters defines the amount of blocks during which the multi sig. addresses in
	// source and destination shards would not bbe possible to unlock by time.
	//
	// For more information, please, see
	// https://miro.com/app/board/o9J_ktf5hng=/?moveToWidget=3074457351816994359&cot=12
	SourceShardMultiSigLockWindowBlocks      uint64
	DestinationShardMultiSigLockWindowBlocks uint64
}

func (r *ResponseInitExchange) TypeID() uint8 {
	return RespInitExchange
}

func (r *ResponseInitExchange) MarshalBinary() (data []byte, err error) {
	encoder := marshalling.NewEncoder()
	err = encoder.PutUint8(r.Code)
	if err != nil {
		return
	}

	err = encoder.MarshallFixedSizeDataSegment(r.TxUUID)
	if err != nil {
		return
	}

	err = encoder.PutUint64(r.SourceShardMultiSigLockWindowBlocks)
	if err != nil {
		return
	}

	err = encoder.PutUint64(r.DestinationShardMultiSigLockWindowBlocks)
	if err != nil {
		return
	}

	return r.SignAndAttachSignature(encoder)
}

func (r *ResponseInitExchange) UnmarshalBinary(data []byte) (err error) {
	body, err := r.ReadSignatureAndDecoupleData(data)
	if err != nil {
		return
	}

	decoder := marshalling.NewDecoder(body)
	r.Code, err = decoder.GetUint8()
	if err != nil {
		return
	}

	err = decoder.UnmarshalDataSegment(16, &r.TxUUID)
	if err != nil {
		return
	}

	r.SourceShardMultiSigLockWindowBlocks, err = decoder.GetUint64()
	if err != nil {
		return
	}

	r.DestinationShardMultiSigLockWindowBlocks, err = decoder.GetUint64()
	if err != nil {
		return
	}

	return
}

func (r *ResponseClientFundsLockTx) TypeID() uint8 {
	return RespClientFundsLockTx
}

type ResponseClientFundsLockTx struct {
	messages.BaseSignedResponse

	Code uint8
}

func (r *ResponseClientFundsLockTx) MarshalBinary() (data []byte, err error) {
	encoder := marshalling.NewEncoder()
	err = encoder.PutUint8(r.Code)
	if err != nil {
		return
	}

	return r.SignAndAttachSignature(encoder)
}

func (r *ResponseClientFundsLockTx) UnmarshalBinary(data []byte) (err error) {
	body, err := r.ReadSignatureAndDecoupleData(data)
	if err != nil {
		return
	}

	decoder := marshalling.NewDecoder(body)
	r.Code, err = decoder.GetUint8()
	if err != nil {
		return
	}

	return
}

type ResponseFetchCrossShardTx struct {
	messages.BaseSignedResponse

	Code         uint8
	CrossShardTx []byte
	LockTxHash   string
	LockTxOutNum uint64
}

func (r *ResponseFetchCrossShardTx) TypeID() uint8 {
	return RespFetchCrossShardTx
}

func (r *ResponseFetchCrossShardTx) MarshalBinary() (data []byte, err error) {
	encoder := marshalling.NewEncoder()

	err = encoder.PutUint8(r.Code)
	if err != nil {
		return
	}

	err = encoder.PutVariadicDataWith2BytesHeader(r.CrossShardTx)
	if err != nil {
		return
	}

	err = encoder.PutString(r.LockTxHash)
	if err != nil {
		return
	}

	err = encoder.PutUint64(r.LockTxOutNum)
	if err != nil {
		return
	}

	data = encoder.CollectDataAndReleaseBuffers()
	return
}

func (r *ResponseFetchCrossShardTx) UnmarshalBinary(data []byte) (err error) {
	decoder := marshalling.NewDecoder(data)

	r.Code, err = decoder.GetUint8()
	if err != nil {
		return
	}

	r.CrossShardTx, err = decoder.GetDataSegmentWith2BytesHeader()
	if err != nil {
		return
	}

	r.LockTxHash, err = decoder.GetString()
	if err != nil {
		return
	}

	r.LockTxOutNum, err = decoder.GetUint64()
	if err != nil {
		return
	}

	return
}
