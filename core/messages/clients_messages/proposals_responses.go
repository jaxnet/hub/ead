package clients_messages

import (
	"gitlab.com/jaxnet/hub/ead/core/ec"
	"gitlab.com/jaxnet/hub/ead/core/epl"
	"gitlab.com/jaxnet/hub/ead/core/marshalling"
)

type ProposalsResponse struct {
	Proposals []*epl.ExchangeAgentProposal
}

func NewProposalResponse(list *epl.ExchangeAgentsProposalsList) (response *ProposalsResponse) {
	ec.InterruptOnNilArgument(list)

	response = &ProposalsResponse{
		Proposals: make([]*epl.ExchangeAgentProposal, 0, len(list.Proposals)),
	}

	for _, proposal := range list.Proposals {
		response.Proposals = append(response.Proposals, proposal)
	}

	return
}

func (response *ProposalsResponse) MarshalBinary() (data []byte, err error) {
	encoder := marshalling.NewEncoder()

	err = encoder.PutUint32(uint32(len(response.Proposals)))
	if err != nil {
		return
	}

	for _, proposal := range response.Proposals {
		proposalBinary, err := proposal.MarshalBinary()
		if err != nil {
			return nil, err
		}

		err = encoder.PutVariadicDataWithByteHeader(proposalBinary)
		if err != nil {
			return nil, err
		}
	}

	data = encoder.CollectDataAndReleaseBuffers()
	return
}

func (response *ProposalsResponse) UnmarshalBinary(data []byte) (err error) {
	decoder := marshalling.NewDecoder(data)

	totalRecordsSize, err := decoder.GetUint32()
	if err != nil {
		return
	}

	response.Proposals = make([]*epl.ExchangeAgentProposal, totalRecordsSize)
	for i, _ := range response.Proposals {
		proposalBinary, err := decoder.GetDataSegmentWithByteHeader()
		if err != nil {
			return err
		}

		proposal := &epl.ExchangeAgentProposal{}
		err = proposal.UnmarshalBinary(proposalBinary)
		if err != nil {
			return err
		}

		response.Proposals[i] = proposal
	}

	return
}
