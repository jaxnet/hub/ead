package clients_messages

import (
	"errors"
	"github.com/google/uuid"
	"gitlab.com/jaxnet/core/shard.core/btcec"
	"gitlab.com/jaxnet/core/shard.core/btcutil"
	"gitlab.com/jaxnet/hub/ead/core/blockchain/shards"
	"gitlab.com/jaxnet/hub/ead/core/marshalling"
	"gitlab.com/jaxnet/hub/ead/core/settings"
	"math/big"
)

const (
	// See "Type" field of the RequestInitExchange
	// for the details about how this constants re used.
	RequestInitExchangeType2Sig = 0
	RequestInitExchangeType3Sig = 1
)

type RequestInitExchange struct {
	// Operation type defines schema how this particular operation would be processed by the client.
	// There are 2 options here:
	//
	// * Sender -> EAD -> Sender (2sig schema)
	// * Sender -> EAD -> Receiver (3sig schema)
	Type uint8

	Index shards.Pair

	// The amount of tokens to be transferred FROM the source shard (client -> EA).
	SourceAmount *big.Int

	// The amount of tokens to be transferred TO the destination shard (EA -> client).
	// This fields is expected to contain less amount of tokens, than source amount
	// (unless the EA is working with negative fees).
	//
	// Client specifies destination amount to define the fees of the operation.
	// It is expected, that client would get fees configuration from the fetched proposal,
	// apply it to the desired amount and will fulfil this field.
	//
	// This field is used to mitigate cases, when EA's fees has changed
	// right before client initializes the exchange operation.
	// EA would validate the request in any way,
	// so in case if defined fees would be less than expected -
	// operation would fail and it makes EA and client sure,
	// that the operation would be processed with predefined fees.
	DestinationAmount *big.Int

	// Address in destination shard where funds would be moved
	// as a result of success operation.
	DestinationShardAddress *btcutil.AddressPubKeyHash

	// In S->EA->S schema, EAD uses Sender's pub key to create cross-shard transaction.
	// (ReceiverPubKey is going to be nil in this case)
	//
	// In S->EA->R schema, EAD uses Sender's and Receiver's pub keys for the same reason.
	SenderPubKey   *btcec.PublicKey
	ReceiverPubKey *btcec.PublicKey
}

func (r *RequestInitExchange) MarshalBinary() (data []byte, err error) {
	encoder := marshalling.NewEncoder()

	err = encoder.PutUint8(r.Type)
	if err != nil {
		return
	}

	err = encoder.MarshallFixedSizeDataSegment(&r.Index)
	if err != nil {
		return
	}

	err = encoder.PutBigIntWithByteHeader(r.SourceAmount)
	if err != nil {
		return
	}

	err = encoder.PutBigIntWithByteHeader(r.DestinationAmount)
	if err != nil {
		return
	}

	// todo: address could be serialized as bytes, not as a hex
	err = encoder.PutString(r.DestinationShardAddress.EncodeAddress())
	if err != nil {
		return
	}

	senderPubKeyBinary := r.SenderPubKey.SerializeCompressed()
	err = encoder.PutVariadicDataWith2BytesHeader(senderPubKeyBinary)
	if err != nil {
		return
	}

	if r.Type == RequestInitExchangeType3Sig {
		// Receiver's Pub Key is optional and is going to be included into request
		// only in case if operation type is S->EA->R.

		receiverPubKeyBinary := r.ReceiverPubKey.SerializeCompressed()
		err = encoder.PutVariadicDataWith2BytesHeader(receiverPubKeyBinary)
		if err != nil {
			return
		}
	}

	data = encoder.CollectDataAndReleaseBuffers()
	return
}

func (r *RequestInitExchange) UnmarshalBinary(data []byte) (err error) {
	decoder := marshalling.NewDecoder(data)

	r.Type, err = decoder.GetUint8()
	if err != nil {
		return
	}

	err = decoder.UnmarshalDataSegment(8, &r.Index)
	if err != nil {
		return
	}

	r.SourceAmount, err = decoder.GetBigIntWithByteHeader()
	if err != nil {
		return
	}

	r.DestinationAmount, err = decoder.GetBigIntWithByteHeader()
	if err != nil {
		return
	}

	address, err := decoder.GetString()
	if err != nil {
		return
	}

	a, err := btcutil.DecodeAddress(address, settings.NetParams)
	if err != nil {
		return
	}

	var ok bool
	r.DestinationShardAddress, ok = a.(*btcutil.AddressPubKeyHash)
	if !ok {
		err = errors.New("invalid address type occurred")
	}

	sendersPubKeyBinary, err := decoder.GetDataSegmentWith2BytesHeader()
	if err != nil {
		return
	}

	r.SenderPubKey, err = btcec.ParsePubKey(sendersPubKeyBinary, btcec.S256())
	if err != nil {
		return
	}

	if r.Type == RequestInitExchangeType3Sig {
		// Receiver's Pub Key is optional and is going to be included into request
		// only in case if operation type is S->EA->R.

		receiversPubKeyBinary, err := decoder.GetDataSegmentWith2BytesHeader()
		if err != nil {
			return err
		}

		r.ReceiverPubKey, err = btcec.ParsePubKey(receiversPubKeyBinary, btcec.S256())
		if err != nil {
			return err
		}
	}

	return
}

func (r *RequestInitExchange) TypeID() []byte {
	return []byte{ReqInitExchange}
}

type RequestClientFundsLockTx struct {
	TxUUID               uuid.UUID
	MultiSigRedeemScript []byte
	TxHash               []byte
}

func (r *RequestClientFundsLockTx) MarshalBinary() (data []byte, err error) {
	encoder := marshalling.NewEncoder()

	err = encoder.MarshallFixedSizeDataSegment(&r.TxUUID)
	if err != nil {
		return
	}

	err = encoder.PutVariadicDataWith2BytesHeader(r.MultiSigRedeemScript)
	if err != nil {
		return
	}

	err = encoder.PutFixedSizeDataSegment(r.TxHash[:])
	if err != nil {
		return
	}

	data = encoder.CollectDataAndReleaseBuffers()
	return
}

func (r *RequestClientFundsLockTx) UnmarshalBinary(data []byte) (err error) {
	decoder := marshalling.NewDecoder(data)

	err = decoder.UnmarshalDataSegment(16, &r.TxUUID)
	if err != nil {
		return
	}

	r.MultiSigRedeemScript, err = decoder.GetDataSegmentWith2BytesHeader()
	if err != nil {
		return
	}

	hash, err := decoder.GetDataSegment(32)
	if err != nil {
		return
	}

	r.TxHash = make([]byte, 32)
	copy(r.TxHash[:], hash)
	return
}

func (r *RequestClientFundsLockTx) TypeID() []byte {
	return []byte{ReqClientFundsLockTX}
}

type RequestFetchCrossShardTx struct {
	TxUUID uuid.UUID
}

func (r *RequestFetchCrossShardTx) MarshalBinary() (data []byte, err error) {
	encoder := marshalling.NewEncoder()

	err = encoder.MarshallFixedSizeDataSegment(&r.TxUUID)
	if err != nil {
		return
	}

	data = encoder.CollectDataAndReleaseBuffers()
	return
}

func (r *RequestFetchCrossShardTx) UnmarshalBinary(data []byte) (err error) {
	decoder := marshalling.NewDecoder(data)

	err = decoder.UnmarshalDataSegment(16, &r.TxUUID)
	return
}

func (r *RequestFetchCrossShardTx) TypeID() []byte {
	return []byte{ReqFetchCrossShardTx}
}
