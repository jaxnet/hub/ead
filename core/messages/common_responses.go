package messages

import (
	"gitlab.com/jaxnet/core/shard.core/btcec"
	"gitlab.com/jaxnet/hub/ead/core/crypto"
	"gitlab.com/jaxnet/hub/ead/core/marshalling"
)

type BaseSignedResponse struct {
	Signature *btcec.Signature
}

func (r *BaseSignedResponse) SignAndAttachSignature(dataEnc *marshalling.Encoder) (content []byte, err error) {
	data := dataEnc.CollectDataAndReleaseBuffers()

	sig, err := r.signatureMarshalBinary(data)
	if err != nil {
		return
	}

	encoder := marshalling.NewEncoder()
	err = encoder.PutVariadicDataWith2BytesHeader(data)
	if err != nil {
		return
	}

	err = encoder.PutFixedSizeDataSegment(sig)
	if err != nil {
		return
	}

	content = encoder.CollectDataAndReleaseBuffers()
	return
}

func (r *BaseSignedResponse) ReadSignatureAndDecoupleData(binaryData []byte) (data []byte, err error) {
	decoder := marshalling.NewDecoder(binaryData)

	data, err = decoder.GetDataSegmentWith2BytesHeader()
	if err != nil {
		return
	}

	sigBinary, err := decoder.GetDataSegmentWith2BytesHeader()
	if err != nil {
		return
	}

	err = r.signatureUnmarshalBinary(sigBinary)
	if err != nil {
		return
	}

	return
}

func (r *BaseSignedResponse) signatureMarshalBinary(content []byte) (data []byte, err error) {
	r.Signature, err = crypto.Sign(content)
	binarySignature := r.Signature.Serialize()

	encoder := marshalling.NewEncoder()
	err = encoder.PutVariadicDataWith2BytesHeader(binarySignature)

	data = encoder.CollectDataAndReleaseBuffers()
	return
}

func (r *BaseSignedResponse) signatureUnmarshalBinary(data []byte) (err error) {
	r.Signature, err = btcec.ParseSignature(data, btcec.S256())
	return
}
