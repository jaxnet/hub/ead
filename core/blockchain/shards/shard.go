package shards

import (
	"fmt"
	"gitlab.com/jaxnet/hub/ead/core/marshalling"
)

type ID uint32

type Pair struct {
	SourceShard      ID
	DestinationShard ID
}

func NewPairIndex(source, destination ID) Pair {
	return Pair{
		SourceShard:      source,
		DestinationShard: destination,
	}
}

func (i *Pair) String() string {
	return fmt.Sprint("(", i.SourceShard, ", ", i.DestinationShard, ")")
}

func (i *Pair) MarshalBinary() (data []byte, err error) {
	encoder := marshalling.NewEncoder()

	err = encoder.PutUint32(uint32(i.SourceShard))
	if err != nil {
		return
	}

	err = encoder.PutUint32(uint32(i.DestinationShard))
	if err != nil {
		return
	}

	data = encoder.CollectDataAndReleaseBuffers()
	return
}

func (i *Pair) UnmarshalBinary(data []byte) (err error) {
	decoder := marshalling.NewDecoder(data)

	sourceShard, err := decoder.GetUint32()
	if err != nil {
		return
	}
	i.SourceShard = ID(sourceShard)

	destinationShard, err := decoder.GetUint32()
	if err != nil {
		return
	}
	i.DestinationShard = ID(destinationShard)

	return
}
