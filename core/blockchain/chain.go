package blockchain

import (
	"encoding/hex"
	"errors"
	"fmt"
	"gitlab.com/jaxnet/core/indexer/clients/utxos/cross_shard"
	"gitlab.com/jaxnet/core/shard.core/btcec"
	"gitlab.com/jaxnet/core/shard.core/btcutil"
	"gitlab.com/jaxnet/core/shard.core/cmd/tx-gatling/txmodels"
	"gitlab.com/jaxnet/core/shard.core/cmd/tx-gatling/txutils"
	"gitlab.com/jaxnet/core/shard.core/types/btcjson"
	"gitlab.com/jaxnet/core/shard.core/types/chaincfg"
	"gitlab.com/jaxnet/core/shard.core/types/chainhash"
	"gitlab.com/jaxnet/core/shard.core/types/wire"
	"gitlab.com/jaxnet/hub/ead/core/blockchain/discovery"
	"gitlab.com/jaxnet/hub/ead/core/blockchain/shards"
	"gitlab.com/jaxnet/hub/ead/core/crypto"
	txs "gitlab.com/jaxnet/hub/ead/core/handlers/transactions/transactions"
	"gitlab.com/jaxnet/hub/ead/core/logger"
	"gitlab.com/jaxnet/hub/ead/core/settings"
	"strconv"
	"strings"
	"time"
)

const (
	// todo: Move it to the settings.
	ShardBlockGenerationTimeout = time.Second * 20
)

type ShardConnector struct {
	ShardID                      shards.ID
	txMan                        *txutils.TxMan
	UTXOIndexerInterface         string
	AddressesTxsIndexerInterface string
}

func NewShardConnector(
	shardConf *settings.ShardConfig, networkName string) (connector *ShardConnector, err error) {
	txManager, err := crypto.NewTxMan(shardConf, networkName)

	connector = &ShardConnector{
		ShardID:                      shardConf.ID,
		txMan:                        txManager,
		UTXOIndexerInterface:         shardConf.UTXOIndexerInterface,
		AddressesTxsIndexerInterface: shardConf.AddressesTxsIndexerInterface,
	}
	return
}

func (c *ShardConnector) GetRawTransaction(hash *chainhash.Hash) (tx *btcutil.Tx, err error) {
	tx, err = c.txMan.RPC().GetRawTransaction(hash, false)
	return
}

func (c *ShardConnector) IsTxMined(hash *chainhash.Hash, confirmationsExpected int64, memPoolCheck bool) (isFound, isMined bool, err error) {
	// todo : replace with method related to tx instead of tx out
	out, err := c.txMan.RPC().GetTxOut(hash, 0, memPoolCheck, false)
	if err != nil {
		return
	}

	if out == nil {
		out, err = c.txMan.RPC().GetTxOut(hash, 2, memPoolCheck, false)
		if err != nil {
			return
		}
	}

	if out == nil {
		return
	}

	isFound = true
	isMined = out.Confirmations >= confirmationsExpected
	logger.Log.Debug().Str("tx-hash", hash.String()).Uint32("shard-id", uint32(c.ShardID)).
		Int64("confirmations", out.Confirmations).Msg("IsTxMined")
	return
}

// Prepare2SigFundsLockMultiSig creates 2/2 multi sig address for locking funds.
func (c *ShardConnector) Prepare2SigFundsLockMultiSig(senderPubKeyBinary []byte, refundDeferringPeriod int32) (
	multiSig *crypto.MultiSigAddress, multiSigAddress *btcutil.AddressScriptHash, err error) {

	signers := [][]byte{senderPubKeyBinary}
	return c.prepareFundsLockMultiSig(signers, refundDeferringPeriod)
}

// Prepare3SigFundsLockMultiSig creates 3/3 multi sig address for locking funds.
func (c *ShardConnector) Prepare3SigFundsLockMultiSig(senderPubKeyBinary, receiverPubKeyBinary []byte,
	refundDeferringPeriod int32) (
	multiSig *crypto.MultiSigAddress, multiSigAddress *btcutil.AddressScriptHash, err error) {

	signers := [][]byte{senderPubKeyBinary, receiverPubKeyBinary}
	return c.prepareFundsLockMultiSig(signers, refundDeferringPeriod)
}

// prepareFundsLockMultiSig low level method for creating the n/n multi sig configurations.
// Used for creating 2/2 and 3/3 multi sig addresses for 2sig and 3sig schemas correspondingly.
func (c *ShardConnector) prepareFundsLockMultiSig(signersPubKeyExceptEAD [][]byte, refundDeferringPeriod int32) (
	multiSig *crypto.MultiSigAddress, multiSigAddress *btcutil.AddressScriptHash, err error) {

	multiSig, err = c.PrepareMultiSig(signersPubKeyExceptEAD, refundDeferringPeriod)
	if err != nil {
		return
	}

	multiSigAddressRaw, err := btcutil.DecodeAddress(multiSig.Address, c.txMan.NetParams)
	if err != nil {
		return
	}

	tmp, ok := multiSigAddressRaw.(*btcutil.AddressScriptHash)
	if !ok {
		err = errors.New("invalid multi sig address generated")
		return
	}

	multiSigAddress = tmp
	return
}

func (c *ShardConnector) IssueCrossShardTx(tx *wire.MsgTx) (
	hash *chainhash.Hash, err error) {

	hash, err = c.txMan.RPC().SendRawTransaction(tx)
	return
}

// PrepareMultiSig creates n/n multi sig address.
// signersBinaryPubKeysExceptEAD must contain set of binary serialized public keys of all signers, EXCEPT EAD's one
// (EAD's one would be added automatically during the address creation).
func (c *ShardConnector) PrepareMultiSig(signersBinaryPubKeysExceptEAD [][]byte, refundDeferringPeriod int32) (
	multiSig *crypto.MultiSigAddress, err error) {

	signersPubKeys := make([]*btcec.PublicKey, 0, len(signersBinaryPubKeysExceptEAD))
	for _, binaryPubKey := range signersBinaryPubKeysExceptEAD {
		deserializedPubKey, err := btcec.ParsePubKey(binaryPubKey, btcec.S256())
		if err != nil {
			return nil, err
		}

		signersPubKeys = append(signersPubKeys, deserializedPubKey)
	}

	signersAddresses := make([]string, 0, len(signersBinaryPubKeysExceptEAD))
	eadPubKey := crypto.PubKey()
	eadAddress, err := btcutil.NewAddressPubKey(eadPubKey.SerializeUncompressed(), settings.NetParams)
	if err != nil {
		return
	}
	signersAddresses = append(signersAddresses, eadAddress.String())
	for _, pubKey := range signersPubKeys {
		address, err := btcutil.NewAddressPubKey(pubKey.SerializeUncompressed(), settings.NetParams)
		if err != nil {
			return nil, err
		}

		signersAddresses = append(signersAddresses, address.String())
	}

	multiSig, err = crypto.MakeMultiSigLockScript(signersAddresses, len(signersAddresses),
		refundDeferringPeriod, settings.NetParams)
	if err != nil {
		return
	}

	return
}

func (c *ShardConnector) CurrentBlockNumber() (blockNumber uint64, err error) {

	_, bestBlock, err := c.txMan.RPC().GetBestBlock()
	if err != nil {
		return
	}
	blockNumber = uint64(bestBlock)
	return
}

func (c *ShardConnector) BuildRefundingTx(multiSigAddress *btcutil.AddressScriptHash, multiSigRedeemScript []byte,
	utxo *wire.TxOut, lockTxHash string) (refundingTx *wire.MsgTx, err error) {
	fee, err := c.txMan.RPC().GetExtendedFee()
	if err != nil {
		return
	}
	moderateFee := txutils.EstimateFee(1, 1,
		int64(fee.Moderate.SatoshiPerB), true)

	eadAddress, err := crypto.Address()
	if err != nil {
		return
	}
	refundingTxBuilder := txutils.NewTxBuilder(chaincfg.NetName(c.txMan.NetParams.Name)).
		SetSenders(multiSigAddress.EncodeAddress()).
		AddRedeemScripts(hex.EncodeToString(multiSigRedeemScript)).
		SetDestinationWithUTXO(eadAddress.EncodeAddress(), utxo.Value-moderateFee, txmodels.UTXORows{{
			ShardID:  uint32(c.ShardID),
			Address:  multiSigAddress.String(),
			TxHash:   lockTxHash,
			Value:    utxo.Value,
			PKScript: hex.EncodeToString(utxo.PkScript),
		}})
	refundingTx, err = crypto.CreateRefundingTx(refundingTxBuilder, moderateFee)
	return
}

func (c *ShardConnector) IssuedRefundingTx(tx *wire.MsgTx) (hash *chainhash.Hash, err error) {

	hash, err = c.txMan.RPC().SendRawTransaction(tx)
	return
}

func (c *ShardConnector) UTXOIndexerHostAndPort() (hostAndPort string) {
	hostAndPort = strings.Replace(c.UTXOIndexerInterface, "http://", "", 1)
	return
}

type Connector struct {
	conf *settings.Settings

	ShardsConnectors map[shards.ID]*ShardConnector
}

func NewConnector() (connector *Connector, err error) {
	logger.Log.Info().Msg("Initializing blockchain connector")
	defer logger.Log.Info().Msg("Blockchain connector initialization finished")

	connector = &Connector{
		ShardsConnectors: make(map[shards.ID]*ShardConnector),
	}

	for shardID, shardConf := range settings.Conf.Blockchain.ShardsMap {
		shardConnector, err := NewShardConnector(shardConf, settings.Conf.Blockchain.NetworkName)
		if err != nil {
			return nil, err
		}

		connector.ShardsConnectors[shardID] = shardConnector
	}

	// Initialising connector to the beacon chain.
	shardConnector, err := NewShardConnector(&settings.Conf.Blockchain.Beacon, settings.Conf.Blockchain.NetworkName)
	connector.ShardsConnectors[0] = shardConnector

	return
}

// todo: add deduplication here.
func (connector *Connector) GetExchangeAgentsRegistrationRecords() (list []*discovery.DiscoveryRecord, err error) {
	txMan := connector.ShardsConnectors[0].txMan

	addresses, err := txMan.RPC().ListEADAddresses(nil, nil)
	if err != nil {
		return
	}

	logger.Log.Debug().Msg(fmt.Sprintf("EAD Addresses: %v", addresses))

	for _, agent := range addresses.Agents {
		rawPK, err := hex.DecodeString(agent.PublicKey)
		if err != nil {
			return nil, err
		}

		pubKeyAddr, err := btcutil.NewAddressPubKey(rawPK, settings.NetParams)
		if err != nil {
			return nil, err
		}

		discoveryRecord := discovery.DiscoveryRecord{
			SeqNumber: agent.ID,
			Addresses: convertEADAddressToDiscoveryAddress(agent.IPs),
			PubKey:    pubKeyAddr.PubKey(),
		}
		list = append(list, &discoveryRecord)
	}
	return
}

func (connector *Connector) PrepareCrossShardTX(tx *txs.Transaction) (crossChainTx *txmodels.SwapTransaction, err error) {

	sourceShardConnector, ok := connector.ShardsConnectors[tx.SourceShardID]
	if !ok {
		err = errors.New("There are no shard connector for source shard " + strconv.Itoa(int(tx.SourceShardID)))
		return
	}
	destinationShardConnector, ok := connector.ShardsConnectors[tx.DestinationShardID]
	if !ok {
		err = errors.New("There are no shard connector for destination shard " + strconv.Itoa(int(tx.DestinationShardID)))
		return
	}

	var (
		sourceShardMultiSigAddress      = tx.SourceShardMultiSigAddress.String()
		destinationShardMultiSigAddress = tx.DestinationShardMultiSigAddress.String()

		sourceShardUTXO, destinationShardUTXO txmodels.UTXO
	)

	hostAndPortsMap := map[uint32]string{
		uint32(tx.SourceShardID):      sourceShardConnector.UTXOIndexerHostAndPort(),
		uint32(tx.DestinationShardID): destinationShardConnector.UTXOIndexerHostAndPort(),
	}
	clientUTXOIndexer, err := cross_shard.New(hostAndPortsMap, false, crypto.SignDataForIndexer)
	if err != nil {
		return
	}

	clientLockFundsTxHash, err := chainhash.NewHash(tx.ClientFundLockTxHash)
	if err != nil {
		return
	}
	sourceShardUTXOs, err := clientUTXOIndexer.GetByHashes(uint32(tx.SourceShardID),
		sourceShardMultiSigAddress, clientLockFundsTxHash.String())
	if len(sourceShardUTXOs) == 0 {
		err = errors.New("no UTXO found for spending multi. sig address (client)")
		return
	}
	sourceShardUTXO = sourceShardUTXOs[0]
	hexSourceShardRedeemScript := hex.EncodeToString(tx.SourceShardMultiSigRedeemScript)
	sourceShardUTXO, err = txutils.SetRedeemScript(sourceShardUTXO, hexSourceShardRedeemScript, settings.NetParams)
	if err != nil {
		return
	}

	eadLockFundsTxHash, err := chainhash.NewHash(tx.EAFundsLockTxHash)
	if err != nil {
		return
	}
	destinationShardUTXOs, err := clientUTXOIndexer.GetByHashes(uint32(tx.DestinationShardID),
		destinationShardMultiSigAddress, eadLockFundsTxHash.String())
	if len(destinationShardUTXOs) == 0 {
		err = errors.New("no UTXO found for spending multi. sig address (EAD)")
		return
	}
	destinationShardUTXO = destinationShardUTXOs[0]
	hexDestinationShardRedeemScript := hex.EncodeToString(tx.DestinationShardMultiSigRedeemScript)
	destinationShardUTXO, err = txutils.SetRedeemScript(destinationShardUTXO,
		hexDestinationShardRedeemScript, settings.NetParams)
	if err != nil {
		return
	}

	crossChainTx, err = crypto.CreateCrossShardTx(
		tx.SourceShardID, tx.DestinationShardID, sourceShardConnector.txMan, destinationShardConnector.txMan,
		clientUTXOIndexer, tx.SourceAmount, tx.DestinationAmount,
		tx.SourceShardDestinationAddress.String(), tx.DestinationShardDestinationAddress.String(),
		&sourceShardUTXO, &destinationShardUTXO, hexSourceShardRedeemScript)

	if err != nil {
		return
	}

	logger.Log.Info().Str(
		"hash", crossChainTx.RawTX.TxHash().String()).Str(
		"signed-tx", crossChainTx.SignedTx).Str(
		"source", crossChainTx.Source).Msg(
		"Cross-Shard TX created")

	return
}

func convertEADAddressToDiscoveryAddress(eadAddresses []btcjson.EADAddress) (
	discoveryMapAddresses map[string][]shards.ID) {
	discoveryMapAddresses = make(map[string][]shards.ID)
	for _, eadAddress := range eadAddresses {
		clientPort := eadAddress.Port
		discoveryAddressKey := fmt.Sprintf("%s:%d", eadAddress.IP, clientPort)
		var discoveryAddressShards []shards.ID
		for _, shardID := range eadAddress.Shards {
			discoveryAddressShards = append(discoveryAddressShards, shards.ID(shardID))
		}
		discoveryMapAddresses[discoveryAddressKey] = discoveryAddressShards
	}
	return
}
