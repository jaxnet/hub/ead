package discovery

import (
	"gitlab.com/jaxnet/core/shard.core/btcec"
	"gitlab.com/jaxnet/hub/ead/core/blockchain/shards"
)

type DiscoveryRecord struct {
	SeqNumber uint64

	// The list of shards in which the Exchange Agent is operating.
	// This list must be specified by the EA for himself on the registration stage.
	//OperatingShards []shards.ID
	Addresses map[string][]shards.ID

	// todo: [security] IMPORTANT!
	// PubKey MUST NOT be stored in the blockchain as a separated data segment.
	// Instead it should be recovered from the signature of the transaction, that has created this record.
	// This could be done on the stage when this info is fetched from the blockchain.
	PubKey *btcec.PublicKey
}
