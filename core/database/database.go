package database

import (
	"context"
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/jaxnet/hub/ead/core/settings"
)

var (
	// Common connections pool for all components.
	// pgxpool is a concurrency-safe pool implementation,
	// so it is safe to use it as singleton once initialised.
	DB *pgxpool.Pool
)

func InitConnectionsPool() (err error) {
	DB, err = pgxpool.Connect(context.Background(), settings.Conf.Database.PSQLConnectionCredentials())
	return
}

// Query is a syntactic sugar which hides context from regular SQL queries and decreases mental complexity.
func Query(sql string, args ...interface{}) (rows pgx.Rows, err error) {
	rows, err = DB.Query(context.Background(), sql, args...)
	return
}

// Query is a syntactic sugar which hides context from regular SQL queries and decreases mental complexity.
func QueryRow(sql string, args ...interface{}) (row pgx.Row) {
	row = DB.QueryRow(context.Background(), sql, args...)
	return
}
