package database

import (
	"context"
	"fmt"
	"gitlab.com/jaxnet/core/shard.core/btcutil"
	"gitlab.com/jaxnet/hub/ead/core/blockchain/shards"
	"gitlab.com/jaxnet/hub/ead/core/ec"
	txs "gitlab.com/jaxnet/hub/ead/core/handlers/transactions/transactions"
	"time"
)

func FetchTransactions(whereClause string) (transactions map[uint64]*txs.Transaction, err error) {

	//
	// Optional fields handlers
	// Returns data in case if it present and corresponds the expected type,
	// otherwise - silently fail.
	//
	__optionalStringField := func(value interface{}) (result *string) {
		switch v := value.(type) {
		case string:
			result = &v
		}

		return
	}

	__optionalBytesField := func(value interface{}) (result []byte) {
		switch v := value.(type) {
		case []byte:
			result = v
		}

		return
	}

	__optionalTimeField := func(value interface{}) (result *time.Time) {
		switch v := value.(type) {
		case time.Time:
			result = &v
		}

		return
	}

	__optionalUint64Field := func(value interface{}) (result uint64) {
		switch v := value.(type) {
		case int64:
			result = uint64(v)
		}
		return
	}

	//
	// Required fields handlers
	// Returns data if it is present and corresponds the expected type.
	// In case of error - set up global error.
	//
	// To keep fields parsing code short - required fields handlers are not returning error.
	// This makes it possible to avoid error checking after each one call
	// and do fields population in table style (see further).
	//
	// In case if some error occurs - these methods are using common error instance.
	// In case if this instance is already set - method automatically exits without doing any actions.
	// This allows to keep fields parsing code clean and process error once after all fields has been parsed.
	//

	var (
		requiredFieldParsingError error
	)

	requiredInt64Field := func(value interface{}) (result int64) {
		if requiredFieldParsingError != nil {
			return
		}

		return value.(int64)
	}

	requiredUint64Field := func(value interface{}) (result uint64) {
		if requiredFieldParsingError != nil {
			return
		}

		return uint64(value.(int64))
	}

	requiredShardIDField := func(value interface{}) (result shards.ID) {
		if requiredFieldParsingError != nil {
			return
		}

		return shards.ID(value.(int32))
	}

	requiredInt16Field := func(value interface{}) (result int16) {
		if requiredFieldParsingError != nil {
			return
		}

		return value.(int16)
	}

	required16BytesField := func(value interface{}) (result [16]byte) {
		if requiredFieldParsingError != nil {
			return
		}

		return value.([16]byte)
	}

	requiredAmountField := func(value interface{}) (result btcutil.Amount) {
		if requiredFieldParsingError != nil {
			return
		}

		return btcutil.Amount(value.(int64))
	}

	requiredStringField := func(value interface{}) (result string) {
		if requiredFieldParsingError != nil {
			return
		}

		return value.(string)
	}

	query := fmt.Sprintf(
		"SELECT " +
			"id, " + // 0
			"uuid, " + // 1
			"source_amount, " + // 2
			"destination_amount, " + // 3
			"source_shard_id, " + // 4
			"destination_shard_id, " + // 5
			"source_shard_destination_address, " + // 6
			"destination_shard_destination_address, " + // 7
			"source_shard_multi_sig_address, " + // 8
			"destination_shard_multi_sig_address, " + // 9
			"source_shard_multi_sig_redeem_script, " + // 10
			"destination_shard_multi_sig_redeem_script, " + // 11
			"state, " + // 12
			"op_schema, " + // 13
			"error_description, " + // 14
			"updated, " + // 15
			"delayed_until, " + // 16
			"client_pub_key, " + // 17
			"receiver_pub_key, " + // 18
			"client_funds_locking_tx_hash, " + // 19
			"client_funds_locking_tx, " + // 20
			"ea_funds_locking_tx_hash, " + // 21
			"ea_funds_locking_tx_out_num, " + // 22
			"cross_shard_tx, " + // 23
			"source_shard_lock_window, " + // 24
			"source_shard_max_block_height, " + // 25
			"destination_shard_lock_window, " + // 26
			"destination_shard_max_block_height, " + // 27
			"max_block_height_for_cstx_signing " + // 28
			"FROM transactions ")

	if whereClause != "" {
		query += fmt.Sprint("WHERE ", whereClause)
	}

	rows, err := DB.Query(context.Background(), query)
	if err != nil {
		return
	}

	transactions = make(map[uint64]*txs.Transaction)
	for rows.Next() {
		values, err := rows.Values()
		if err != nil {
			return nil, err
		}

		var (
			txID                                 = requiredInt64Field(values[0])     // 0
			txUUID                               = required16BytesField(values[1])   // 1
			sourceShardAmount                    = requiredAmountField(values[2])    // 2
			destinationShardAmount               = requiredAmountField(values[3])    // 3
			sourceShardID                        = requiredShardIDField(values[4])   // 4
			destinationShardID                   = requiredShardIDField(values[5])   // 5
			sourceShardDestinationAddress        = requiredStringField(values[6])    // 6
			destinationShardDestinationAddress   = requiredStringField(values[7])    // 7
			sourceShardMultiSigAddress           = __optionalStringField(values[8])  // 8, 	null by default
			destinationShardMultiSigAddress      = __optionalStringField(values[9])  // 9, 	null by default
			sourceShardMultiSigRedeemScript      = __optionalBytesField(values[10])  // 10, null by default
			destinationShardMultiSigRedeemScript = __optionalBytesField(values[11])  // 11, null by default
			txState                              = requiredInt16Field(values[12])    // 12
			txSchema                             = requiredInt16Field(values[13])    // 13
			errorDescription                     = __optionalStringField(values[14]) // 14, null by default
			updated                              = __optionalTimeField(values[15])   // 15, null by default
			delayedUntil                         = __optionalTimeField(values[16])   // 16, null by default
			clientPubKey                         = __optionalBytesField(values[17])  // 17, null by default
			receiverPubKey                       = __optionalBytesField(values[18])  // 18, null by default

			// todo: [refactoring] This field is redundant.
			//		 Tx hash could be fetched from the TX itself (next field).
			clientFundsLockingTxHash = __optionalBytesField(values[19])  // 19, null by default
			clientFundsLockingTx     = __optionalBytesField(values[20])  // 20, null by default
			eaFundsLockingTxHash     = __optionalBytesField(values[21])  // 21, null by default
			eaFundsLickingTxOutNum   = __optionalUint64Field(values[22]) // 22, null by default
			crossShardTx             = __optionalBytesField(values[23])  // 23, null by default

			sourceShardLockWindow          = requiredUint64Field(values[24]) // 24,
			sourceShardMaxBlockHeight      = requiredUint64Field(values[25]) // 25
			destinationShardLockWindow     = requiredUint64Field(values[26]) // 26,
			destinationShardMaxBlockHeight = requiredUint64Field(values[27]) // 27,
			maxBlockHeightForCstxSigning   = requiredUint64Field(values[28]) // 28,
		)

		if requiredFieldParsingError != nil {
			err = requiredFieldParsingError
			return nil, err
		}

		tx, err := txs.FromDBData(
			txID,
			txUUID,
			sourceShardAmount,
			destinationShardAmount,
			sourceShardID,
			destinationShardID,
			sourceShardDestinationAddress,
			destinationShardDestinationAddress,
			sourceShardMultiSigAddress,
			destinationShardMultiSigAddress,
			sourceShardMultiSigRedeemScript,
			destinationShardMultiSigRedeemScript,
			txState,
			txSchema,
			errorDescription,
			updated,
			delayedUntil,
			clientPubKey,
			receiverPubKey,
			clientFundsLockingTxHash,
			clientFundsLockingTx,
			eaFundsLockingTxHash,
			eaFundsLickingTxOutNum,
			crossShardTx,
			sourceShardLockWindow,
			sourceShardMaxBlockHeight,
			destinationShardLockWindow,
			destinationShardMaxBlockHeight,
			maxBlockHeightForCstxSigning,
		)

		if err != nil {
			return nil, err
		}

		transactions[tx.ID] = tx
	}

	return
}

func FetchOneTransaction(whereClause string) (tx *txs.Transaction, err error) {
	transactions, err := FetchTransactions(whereClause)
	if err != nil {
		return
	}

	if len(transactions) == 0 {
		err = ec.ErrDBNoRecords
		return
	}

	if len(transactions) != 1 {
		format := "received `where` clause leads to more than one transaction: %w"
		err = fmt.Errorf(format, ec.ErrDBMoreRecordsThanExpected)
		return
	}

	for _, tx := range transactions {
		return tx, nil
	}

	return
}
