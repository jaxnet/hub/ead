begin transaction;

create table if not exists fees (
    id bigserial primary key,
    constraint id_gte_zero check (id >= 0),

    source_shard_id bigint,
    constraint source_shard_id_gte_zero check (source_shard_id >= 0), /* beacon chain is supported */

    destination_shard_id bigint,
    constraint destination_shard_id_gte_zero check (destination_shard_id >= 0), /* beacon chain is supported */
    constraint shards_are_not_equal check ( source_shard_id != destination_shard_id),

    fee_fixed bigint,
    constraint fee_fixed_gte_zero check (fee_fixed >= 0),

    fee_percents float8,
    constraint fee_percents_gte_zero check (fee_percents >= 0),
    constraint fee_percents_lt_hundred check (fee_percents < 100),

    fee_expire_ttl timestamp with time zone,

    updated timestamp with time zone default now()
);

create table if not exists reserves (
    id bigserial primary key,
    constraint id_gte_zero check (id >= 0),

    shard_id bigint,
    constraint shard_id_gte_zero check (shard_id >= 0), /* beacon chain is supported */

    reserve bigint,
    constraint reserve_gte_zero check (reserve >= 0),

    updated timestamp with time zone default now()
);

create table if not exists transactions (
    id bigserial primary key,
    constraint id_gte_zero check (id >= 0),

    uuid uuid not null,
    constraint unique_uuid unique (uuid),

    source_amount bigint not null,
    constraint source_amount_gt_zero
        check (source_amount > 0),

    destination_amount bigint not null,
    constraint destination_amount_gt_zero
        check (destination_amount > 0),
    constraint destination_amount_le_source_amount
        check (destination_amount <= source_amount),

    source_shard_id int not null,
    constraint source_shard_id_gte_zero
        check (source_shard_id >= 0),

    destination_shard_id int not null,
    constraint destination_shard_id_gte_zero
        check (destination_shard_id >= 0),
    constraint shards_are_different
        check (source_shard_id != destination_shard_id),

    source_shard_destination_address text,              /* EAD's address */
    destination_shard_destination_address text,         /* Sender's / Receiver's address */
    source_shard_multi_sig_address text null,           /* Sender -> EAD time locking address */
    destination_shard_multi_sig_address text null,      /* EAD -> Sender/Receiver time locking address */

    source_shard_multi_sig_redeem_script bytea null default null,
    destination_shard_multi_sig_redeem_script bytea null default null,

    state smallint not null default 0,
    constraint state_ge_zero check (state >= 0),

    op_schema smallint not null,
    constraint op_schema_ge_zero check (op_schema >= 0),

    error_description text null default null,
    updated timestamp with time zone default now(),
    delayed_until timestamp with time zone default null,

    client_pub_key bytea null default null,
    receiver_pub_key bytea null default null,

    client_funds_locking_tx_hash bytea null default null,
    client_funds_locking_tx bytea null default null,
    ea_funds_locking_tx_hash bytea null default null,
    ea_funds_locking_tx bytea null default null,
    ea_funds_locking_tx_out_num bigint,
    cross_shard_tx bytea null default null,

    source_shard_lock_window bigint,
    constraint source_shard_lock_window_ge_zero check (source_shard_lock_window >= 0),
    source_shard_max_block_height bigint,
    destination_shard_lock_window bigint,
    constraint destination_shard_lock_window_ge_zero check (destination_shard_lock_window >= 0),
    destination_shard_max_block_height bigint,
    max_block_height_for_cstx_signing bigint,
    constraint max_block_height_for_cstx_signing_ge_zero check (max_block_height_for_cstx_signing >= 0)
);

create table if not exists transactions_revisions (
      rev bigserial primary key,

      txid bigint,
      constraint id_gte_zero check (txid >= 0),

      uuid uuid not null,
      constraint txrev_unique_uuid unique (rev, txid, uuid),

      source_amount bigint not null,
      constraint txrev_source_amount_gt_zero
          check (source_amount > 0),

      destination_amount bigint not null,
      constraint txrev_destination_amount_gt_zero
          check (destination_amount > 0),

      constraint txrev_destination_amount_le_source_amount
          check (destination_amount <= source_amount),

      source_shard_id int not null,
      constraint source_shard_id_gte_zero
          check (source_shard_id >= 0),

      destination_shard_id int not null,
      constraint destination_shard_id_gte_zero
          check (destination_shard_id >= 0),
      constraint shards_are_different
          check (source_shard_id != destination_shard_id),

      source_shard_destination_address text,              /* EAD's address */
      destination_shard_destination_address text,         /* Sender's / Receiver's address */
      source_shard_multi_sig_address text null,           /* Sender -> EAD time locking address */
      destination_shard_multi_sig_address text null,      /* EAD -> Sender/Receiver time locking address */

      source_shard_multi_sig_redeem_script bytea null default null,
      destination_shard_multi_sig_redeem_script bytea null default null,

      state smallint not null default 0,
      constraint state_ge_zero check (state >= 0),

      op_schema smallint not null,
      constraint op_schema_ge_zero check (op_schema >= 0),

      error_description text null default null,
      updated timestamp with time zone default now(),
      delayed_until timestamp with time zone default null,

      client_pub_key bytea null default null,
      receiver_pub_key bytea null default null,

      client_funds_locking_tx_hash bytea null default null,
      client_funds_locking_tx bytea null default null,
      ea_funds_locking_tx bytea null default null,
      cross_shard_tx bytea null default null,

      created timestamp with time zone
);

create or replace function trigger_on_transaction_revision()
    returns trigger
    language plpgsql as $body$
begin
    insert into transactions_revisions
    (txid, uuid, source_amount, destination_amount,
     source_shard_id, destination_shard_id,
     source_shard_destination_address, destination_shard_destination_address,
     source_shard_multi_sig_address, destination_shard_multi_sig_address,
     source_shard_multi_sig_redeem_script, destination_shard_multi_sig_redeem_script,
     state, op_schema,
     error_description, delayed_until,
     client_pub_key, receiver_pub_key, client_funds_locking_tx_hash, client_funds_locking_tx,
     ea_funds_locking_tx,
     cross_shard_tx,
     created) --WARN: !!

    values (new.id, new.uuid, new.source_amount,
            new.destination_amount, new.source_shard_id, new.destination_shard_id,
            new.source_shard_destination_address, new.destination_shard_destination_address,
            new.source_shard_multi_sig_address, new.destination_shard_multi_sig_address,
            new.source_shard_multi_sig_redeem_script, new.destination_shard_multi_sig_redeem_script,
            new.state, new.op_schema,
            new.error_description, new.delayed_until,
            new.client_pub_key, new.receiver_pub_key, new.client_funds_locking_tx_hash, new.client_funds_locking_tx,
            new.ea_funds_locking_tx,
            new.cross_shard_tx,
            new.updated); --WARN: !!

    return new;
end; $body$;

drop trigger if exists transactions_revisions on transactions;
create trigger transactions_revisions
    before insert or delete or update on transactions
    for each row
execute procedure
    trigger_on_transaction_revision();

end;