package settings

import (
	"fmt"
	"gitlab.com/jaxnet/core/shard.core/types/chaincfg"
	"gitlab.com/jaxnet/hub/ead/core/blockchain/shards"
	"gopkg.in/yaml.v2"
	"io/ioutil"
)

var (
	Conf      *Settings
	NetParams *chaincfg.Params
)

type Interfaces struct {
	Http           NetworkConfig `yaml:"http"`
	HttpPublic     NetworkConfig `yaml:"http-public"`
	ExchangeAgents NetworkConfig `yaml:"exchange-agents"`
	Clients        NetworkConfig `yaml:"clients"`
}

type BlockchainConfig struct {
	NetworkName string `yaml:"network-name"`

	// Shards are stored as an array in conf.yaml,
	// It is much more suitable to operate them as a map of shards configs,
	// but in this case, config goes to be a bit ugly.
	// To handle it, slice structure is going to be copied into the map on settings initialisation.
	Shards []ShardConfig `yaml:"shards"`

	Beacon ShardConfig `yaml:"beacon"`

	// Due to the bug in current yaml-parser implementation,
	// there is no way to parse field from YAML-file to the private field, or field with different name.
	// So, there were a choice: to rename "shards" section in config file, or to use different name for map.
	ShardsMap map[shards.ID]*ShardConfig `yaml:"-"`
}

func (c *BlockchainConfig) normalize() {
	// Moving Shards configs from slice into the map.
	// See `Shards` field docs for the details.
	c.ShardsMap = make(map[shards.ID]*ShardConfig)
	for i, shardConf := range c.Shards {
		c.ShardsMap[shardConf.ID] = &c.Shards[i]
	}
}

type DatabaseConfig RPCConfig

func (c DatabaseConfig) PSQLConnectionCredentials() string {
	return fmt.Sprint("postgres://", c.Credentials.User, ":", c.Credentials.Pass, "@", c.Net.Address, ":", c.Net.Port, "/ead")
}

type PaymentsHandlerSettings struct {
	URL            string `yaml:"url"`
	MaxBlockHeight uint64 `yaml:"max_block_height"`
}

type Settings struct {
	Debug           bool                    `yaml:"debug"`
	Interfaces      Interfaces              `yaml:"interfaces"`
	Blockchain      BlockchainConfig        `yaml:"blockchain"`
	Database        DatabaseConfig          `yaml:"database"`
	PaymentsHandler PaymentsHandlerSettings `yaml:"payments_handler"`
}

func LoadSettings() (err error) {
	Conf = &Settings{}

	data, err := ioutil.ReadFile("conf.yaml")
	if err != nil {
		return
	}

	err = yaml.Unmarshal(data, Conf)
	if err != nil {
		return
	}

	Conf.normalize()

	err = Conf.validate()
	if err != nil {
		return
	}

	// todo: temporary implementation here
	Conf.assignNetworkConfAccordingToSpecifiedNetwork()
	return
}

func (s *Settings) assignNetworkConfAccordingToSpecifiedNetwork() {
	if s.Blockchain.NetworkName == "mainnet" {
		panic("Slow down, boy! It's not ready yet..")
	}

	if s.Blockchain.NetworkName != "fastnet" {
		panic("Hmm.. I know nothing about other test networks than 'testnet'(")
	}

	NetParams = &chaincfg.TestNet3Params
}

// normalize calls the same methods of internal structures,
// to processes loaded parameters and change their representation if it is needed.
func (s *Settings) normalize() {
	s.Blockchain.normalize()
}

func (s *Settings) validate() (err error) {
	defer func() {
		// Providing a bit more more context to the error.
		// Otherwise, the error would be a bit hard to assign to the settings.
		if err != nil {
			err = fmt.Errorf("settings validation error: %w", err)
		}
	}()

	for _, shardConf := range s.Blockchain.ShardsMap {
		err = shardConf.validate()
		if err != nil {
			return
		}
	}

	return
}
