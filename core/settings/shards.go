package settings

import (
	"fmt"
	"gitlab.com/jaxnet/hub/ead/core/blockchain/shards"
	"gitlab.com/jaxnet/hub/ead/core/ec"
	"gitlab.com/jaxnet/hub/ead/core/logger"
)

type ShardConfig struct {
	ID  shards.ID `yaml:"id"`
	RPC RPCConfig `yaml:"rpc"`

	// [security]
	// Amount of blocks during which EAD is able to sign cross-shard operation.
	//
	// See cross-shard transactions processing schema for more details:
	// https://miro.com/app/board/o9J_ktf5hng=/?moveToWidget=3074457351816994359&cot=12
	CrossShardOperationsBlocksRange int    `yaml:"cross-shard-operations-blocks-range"`
	UTXOIndexerInterface            string `yaml:"utxo-indexer-interface"`
	AddressesTxsIndexerInterface    string `yaml:"addresses-txs-indexer-interface"`
}

func (s *ShardConfig) validate() (err error) {
	if s.CrossShardOperationsBlocksRange < ec.CrossShardOperationsBlocksRangeMinCount {
		// Error: setting this parameter sow low could harm security.
		msg := fmt.Sprint(
			"cross shard operations blocks range is configured to be less ",
			"than minimal possible value, provided parameter: ", s.CrossShardOperationsBlocksRange, ", ",
			"minimal possible values is: ", ec.CrossShardOperationsBlocksRangeMinCount)

		err = fmt.Errorf(msg+"(%w)", ec.ErrValidationFailed)
		return
	}

	if s.CrossShardOperationsBlocksRange > ec.CrossShardOperationsBlocksRangeCountSoftCap {
		// Warning: potentially to long operation, but still OK.
		logger.Log.Warn().Int(
			"max-value", ec.CrossShardOperationsBlocksRangeCountSoftCap).Int(
			"provided-value", s.CrossShardOperationsBlocksRange).Msg(
			"Cross shard operations blocks range is configured to be bigger " +
				"than soft cap value, which is OK, but could lead to very long funds lock operations.")
	}

	if s.CrossShardOperationsBlocksRange > ec.CrossShardOperationsBlocksRangeCountHardCap {
		// Error: transport level limitations.
		// See implementation if the ResponseInitExchange{} for the details.
		msg := fmt.Sprint(
			"cross shard operations blocks range is configured to be bigger ",
			"than hard cap value, provided parameter: ", s.CrossShardOperationsBlocksRange, ", ",
			"hard cap values is: ", ec.CrossShardOperationsBlocksRangeCountHardCap)

		err = fmt.Errorf(msg+"(%w)", ec.ErrValidationFailed)
	}

	return
}
