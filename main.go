package main

import (
	"gitlab.com/jaxnet/hub/ead/core"
	"gitlab.com/jaxnet/hub/ead/core/crypto"
	"gitlab.com/jaxnet/hub/ead/core/ec"
	"gitlab.com/jaxnet/hub/ead/core/logger"
	"gitlab.com/jaxnet/hub/ead/core/settings"
)

func main() {
	// todo: [post PoC] Update all marshalling/unmarshalling to the new style using Encoder/Decoder
	// todo: [post PoC] Provide documentation for all methods.
	// todo: [post PoC] Provide (save in repo?) postman schema for HTTP requests.
	// todo: [post PoC] Provide schema of EAD communication with clients in public access.

	err := settings.LoadSettings()
	ec.InterruptOnError(err)

	logger.Init(settings.Conf.Debug)

	err = crypto.InitKeyChain()
	ec.InterruptOnError(err)

	c, err := core.NewCore()
	ec.InterruptOnError(err)

	err = c.RunUntilError()
	ec.InterruptOnError(err)
}
