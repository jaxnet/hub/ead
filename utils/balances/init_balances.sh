# Initialisation of Alice's account (shard 1, 10 JaxCoins)
rm config.yaml -f
cp shard_1_config.yaml config.yaml

#make sync_miner
make send_tx -f data/miner_utxo.csv

# Initialisation of Bob's account (shard 2, 10 JaxCoins)
rm config.yaml -f1
cp shard_2_config.yaml config.yaml

#make sync_miner
make send_tx -f data/miner_utxo.csv
