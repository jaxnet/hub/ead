
drop database ead;
create database ead;


update transactions set state=0 where id =23;
update transactions set client_pub_key = decode('04bcc591ca1c9bd40b90146b20396ac37090dfdb75a4f2742da982cda3148c1d27e65716eeed3294cc87f22399437b2bc431f687316111b8a90632b396209a3e94', 'hex')  where id = 3;

select receiver_pub_key from transactions where id = 16;

select id, error_description from transactions;

select coalesce(sum(source_amount), 0) from transactions where source_shard_id = 1 AND state >= 7;

select coalesce(sum(destination_amount), 0) from transactions where destination_shard_id = 1 AND state >= 7;

begin transaction;
drop table if exists transactions cascade;
create table if not exists transactions (
    id bigserial primary key,
    constraint id_gte_zero check (id >= 0),

    uuid uuid not null,
    constraint unique_uuid unique (uuid),

    source_amount bigint not null,
    constraint source_amount_gt_zero
        check (source_amount > 0),

    destination_amount bigint not null,
    constraint destination_amount_gt_zero
        check (destination_amount > 0),

    constraint destination_amount_le_source_amount
        check (destination_amount <= source_amount),

    source_shard_id int not null,
    constraint source_shard_id_gte_zero
        check (source_shard_id >= 0),

    destination_shard_id int not null,
    constraint destination_shard_id_gte_zero
        check (destination_shard_id >= 0),
    constraint shards_are_different
        check (source_shard_id != destination_shard_id),

    source_shard_destination_address text,
    destination_shard_destination_address text,

--     source_shard_multi_sig_address text,
    source_shard_multi_sig_redeem_script bytea null default null,

--     destination_shard_multi_sig_address text,
    destination_shard_multi_sig_redeem_script bytea null default null,


    state smallint not null default 0,
    constraint state_ge_zero check (state >= 0),

    error_description text null default null,

    updated timestamp with time zone default now(),
    delayed_until timestamp with time zone default null,
    deadline timestamp with time zone,

    client_pub_key bytea null default null,
    client_funds_locking_tx_hash bytea null default null,
    client_funds_locking_tx bytea null default null,
    ea_funds_locking_tx bytea null default null,
    cross_shard_tx bytea null default null,
    cross_shard_tx_source_shard_hash bytea null default null,
    cross_shard_tx_destination_shard_hash bytea null default null
);

drop table if exists transactions_revisions cascade;
create table if not exists transactions_revisions (
    rev bigserial primary key,

    txid bigint,
    constraint id_gte_zero check (txid >= 0),

    uuid uuid not null,
    constraint txrev_unique_uuid unique (rev, txid, uuid),

    source_amount bigint not null,
    constraint txrev_source_amount_gt_zero
        check (source_amount > 0),

    destination_amount bigint not null,
    constraint txrev_destination_amount_gt_zero
        check (destination_amount > 0),

    constraint txrev_destination_amount_le_source_amount
        check (destination_amount <= source_amount),

    source_shard_id int not null,
    constraint source_shard_id_gte_zero
        check (source_shard_id >= 0),

    destination_shard_id int not null,
    constraint destination_shard_id_gte_zero
        check (destination_shard_id >= 0),
    constraint shards_are_different
        check (source_shard_id != destination_shard_id),

--     source_shard_multi_sig_address text,
    source_shard_multi_sig_redeem_script bytea null default null,

--     destination_shard_multi_sig_address text,
    destination_shard_multi_sig_redeem_script bytea null default null,

    source_shard_destination_address text,
    destination_shard_destination_address text,

--     client_pub_key bytea,

    state smallint not null default 0,
    constraint state_ge_zero check (state >= 0),

    error_description text null default null,

    updated timestamp with time zone default now(),
    delayed_until timestamp with time zone default null,
    deadline timestamp with time zone,

    client_pub_key bytea null default null,
    client_funds_locking_tx_hash bytea null default null,
    client_funds_locking_tx bytea null default null,
    ea_funds_locking_tx bytea null default null,
    cross_shard_tx bytea null default null,
    cross_shard_tx_source_shard_hash bytea null default null,
    cross_shard_tx_destination_shard_hash bytea null default null,

    created timestamp with time zone
);

create or replace function trigger_on_transaction_revision()
    returns trigger
    language plpgsql as $body$
begin
    insert into transactions_revisions
        (txid, uuid, source_amount, destination_amount, source_shard_id, destination_shard_id,
         source_shard_multi_sig_redeem_script, destination_shard_multi_sig_redeem_script,
         source_shard_destination_address, destination_shard_destination_address,
         state, error_description, delayed_until, deadline,
         client_pub_key, client_funds_locking_tx_hash, client_funds_locking_tx,
         ea_funds_locking_tx, cross_shard_tx,
         cross_shard_tx_source_shard_hash, cross_shard_tx_destination_shard_hash,
         created) --WARN: !!

    values (new.id, new.uuid, new.source_amount, new.destination_amount, new.source_shard_id, new.destination_shard_id,
            new.source_shard_multi_sig_redeem_script,
            new.destination_shard_multi_sig_redeem_script,
            new.source_shard_destination_address, new.destination_shard_destination_address,
            new.state, new.error_description, new.delayed_until, new.deadline,
            new.client_pub_key, new.client_funds_locking_tx_hash, new.client_funds_locking_tx,
            new.ea_funds_locking_tx, new.cross_shard_tx,
            new.cross_shard_tx_source_shard_hash, new.cross_shard_tx_destination_shard_hash,
            new.updated); --WARN: !!

    return new;
end; $body$;


create trigger transactions_revisions
    before insert or delete or update on transactions
    for each row
execute procedure
    trigger_on_transaction_revision();

end;
--
--
--
-- select now();
--
select id, state, cross_shard_tx from transactions;
--
-- INSERT INTO transactions (uuid, source_amount, destination_amount, source_shard_id, destination_shard_id, source_shard_destination_address, destination_shard_destination_address) VALUES ('9cb47c2a-f109-11ea-8760-a81e84054adf', 100000000, 100000000, 1, 2, 'mwfVehxRZMWpiThvRDLX86jZ1DgC45qFxg', 'mrVnei2tNmPVQg9geVmJpm2oxyQo1Soxs2')
--
--
--
--
--
--
--
--
--
--
-- update transactions set
--     source_amount = 2
-- where id = 1;
--
--
-- insert into transactions
-- (uuid, source_amount, destination_amount, source_shard_id, destination_shard_id,
--  source_shard_multi_sig_address, source_shard_multi_sig_redeem_script,
--  destination_shard_multi_sig_address, destination_shard_multi_sig_redeem_script,
--  source_shard_destination_address, destination_shard_destination_address,
--  client_pub_key, state,
--  updated, deadline)
-- values (
--            '300003b3-758c-42e1-96aa-60957647a3ce', 10, 9,
--            '5feceb66ffc86f38d952786c6d696c79c2dbc239dd4e91b46729d73a27fb57e9',
--            '6b86b273ff34fce19d6b804eff5a3f5747ada4eaa22f1d49c01e52ddb7875b4b',
--            '3B1NNtaK474rhR3n6XJSncAD86EJFBdF5f', 'source_reedem', '33jfJxQHsabZNXBezCxRrxcUjtZo45hMqa', 'dest_redeem',
--            '33jfJxQHsabZNXBezCxRrxcUjtZo45hMqa', '33jfJxQHsabZNXBezCxRrxcUjtZo45hMqa', 'xxxx', 0, now(), now() + interval '1 day');
--
-- select * from transactions;
-- select * from transactions_revisions;
--
-- SELECT id, uuid, source_amount, destination_amount, source_shard_id, destination_shard_id, source_shard_destination_address, destination_shard_destination_address, source_shard_multi_sig_address, source_shard_multi_sig_redeem_script, destination_shard_multi_sig_address, destination_shard_multi_sig_redeem_script, client_pub_key, state, error_description, updated, delayed_until, deadline, ea_funds_locking_tx, cross_shard_tx FROM transactions WHERE state = 0 AND (now() < delayed_until OR delayed_until IS NULL) AND now() < deadline
--
-- select now();
--
-- INSERT INTO transactions (
--     uuid, source_amount, destination_amount,
--     source_shard_id, destination_shard_id,
--     source_shard_destination_address,
--     destination_shard_destination_address)
-- VALUES (
--            'a2fd204b-f107-11ea-b137-a81e84054adf',
--            100000000, 100000000, 1, 2,
--            'mwfVehxRZMWpiThvRDLX86jZ1DgC45qFxg',
--            'mrVnei2tNmPVQg9geVmJpm2oxyQo1Soxs2')
--
--
-- --
-- -- create table if not exists transactions_states (
-- --     id bigserial primary key,
-- --
-- --     tx bigint not null,
-- --     constraint fk_transactions
-- --        foreign key (tx)
-- --            references transactions(id),
-- --
-- --     state smallint not null,
-- --     constraint state_gte_zero
-- --         check (state >= 0),
-- --     constraint state_lt_last_state
-- --         check (state < 3),
-- --
-- --     updated timestamp with time zone not null default now()
-- -- );
--
-- -- drop table reserves cascade ;
-- -- create table if not exists reserves (
-- --   id bigserial primary key ,
-- --
-- --   shard_id text not null,
-- --   constraint shard_id_length
-- --       check (length(shard_id) = 64),
-- --
-- --   amount decimal not null,
-- --   constraint amount_gt_zero
-- --       check (amount > 0),
-- --
-- --   updated timestamp with time zone not null default now(),
-- --
-- --   constraint unique_shard_and_timestamp unique (shard_id, updated)
-- -- );
-- --
-- -- drop function if exists check_available_reserve_is_source_shard cascade;
-- -- create or replace function check_available_reserve_is_source_shard() returns trigger as
-- --     $$
-- --     declare
-- --         last_reserve_updated timestamp with time zone;
-- --         reserve_declared decimal;
-- --         reserves_count decimal;
-- --         reserve_available decimal;
-- --         reserve_used decimal;
-- --
-- --     begin
-- --         reserves_count := (select count(shard_id) from reserves where shard_id = new.source_shard_id);
-- --         if (reserves_count = 0) then
-- --             raise exception 'no reserve specified';
-- --         end if;
-- --
-- --         last_reserve_updated := (select updated from reserves where shard_id = new.source_shard_id order by updated desc limit 1);
-- --         reserve_declared := (select amount from reserves where shard_id = new.source_shard_id order by updated desc limit 1);
-- --
-- --         reserve_used := (select coalesce(sum(source_amount), 0) from transactions where id in (select tx from transactions_states where state = 0 and updated >= last_reserve_updated));
-- --         reserve_available = reserve_declared - reserve_used;
-- --         if (reserve_available <= 0) then
-- --             raise exception 'insufficient reserve funds';
-- --         end if;
-- --
-- --         if (reserve_available < new.source_amount) then
-- --             raise exception 'insufficient reserve funds';
-- --         end if;
-- --
-- --         return new;
-- --     end
-- --     $$ language plpgsql;
-- --
-- -- drop function if exists check_available_reserve_is_destination_shard() cascade;
-- -- create or replace function check_available_reserve_is_destination_shard() returns trigger as
-- -- $$
-- -- declare
-- --     last_reserve_updated timestamp with time zone;
-- --     reserve_declared decimal;
-- --     reserves_count decimal;
-- --     reserve_available decimal;
-- --     reserve_used decimal;
-- --
-- -- begin
-- --     reserves_count := (select count(shard_id) from reserves where shard_id = new.destination_shard_id);
-- --     if (reserves_count = 0) then
-- --         raise exception 'no reserve specified';
-- --     end if;
-- --
-- --     last_reserve_updated := (select updated from reserves where shard_id = new.destination_shard_id order by updated desc limit 1);
-- --     reserve_declared := (select amount from reserves where shard_id = new.destination_shard_id order by updated desc limit 1);
-- --
-- --     reserve_used := (select coalesce(sum(destination_amount), 0) from transactions where id in (select tx from transactions_states where state = 0 and updated >= last_reserve_updated));
-- --     reserve_available = reserve_declared - reserve_used;
-- --     if (reserve_available <= 0) then
-- --         raise exception 'insufficient reserve funds';
-- --     end if;
-- --
-- --     if (reserve_available < new.destination_amount) then
-- --         raise exception 'insufficient reserve funds';
-- --     end if;
-- --
-- --     return new;
-- -- end
-- -- $$ language plpgsql;
-- --
-- -- create trigger check_source_shard_available_reserve
-- --     before insert or update on transactions
-- --     for each row
-- --     execute procedure check_available_reserve_is_source_shard();
-- --     end;
-- --
-- -- create trigger check_destination_shard_available_reserve
-- --     before insert or update on transactions
-- --     for each row
-- --     execute procedure check_available_reserve_is_destination_shard();
-- -- end;
--
-- -- select * from transactions where id in (select tx from transactions_states where state = 0);
-- --
-- -- begin;
-- --     select * from reserves;
-- --
-- --     select * from transactions;
-- --     delete from transactions where true;
-- --
-- --     insert into transactions (
-- --       uuid, source_amount, destination_amount,
-- --       source_shard_id, destination_shard_id,
-- --       source_shard_source_address, source_shard_destination_address,
-- --       destination_shard_source_address, destination_shard_destination_address)
-- --     values (
-- --        '300003b3-758c-42e1-96aa-60957647a3ce', 1, 0.9, '1111111111111111111111111111111111111111111111111111111111111111', '1111111111111111111111111111111111111111111111111111111111111112', '1:11111111111111111111111111111111111111111111111111111111111111', '1:21111111111111111111111111111111111111111111111111111111111111', '2:11111111111111111111111111111111111111111111111111111111111111', '2:21111111111111111111111111111111111111111111111111111111111111') returning id;
-- --
-- --     insert into transactions_states (tx, state) values (49, 0);
-- --
-- --     insert into reserves (shard_id, amount) values ('1111111111111111111111111111111111111111111111111111111111111111', 2);
-- --
-- --
-- --
-- --
-- -- select * from reserves where shard_id = '1111111111111111111111111111111111111111111111111111111111111111' order by updated desc limit 1;
-- --     select coalesce(sum(source_amount), 0) from transactions where id in (select tx from transactions_states where state = 0);
-- --
-- -- commit;
-- --
-- -- rollback;
--
--
-- -- end
-- --!!
-- --     else
-- --         -- Subsequent edits of node
-- --         insert into transactions_revisions
-- --         (txid, uuid, source_amount, destination_amount, source_shard_id, destination_shard_id,
-- --          source_shard_multi_sig_address, source_shard_multi_sig_redeem_script,
-- --          destination_shard_multi_sig_address, destination_shard_multi_sig_redeem_script,
-- --          source_shard_destination_address, destination_shard_destination_address,
-- --          client_pub_key, state,
-- --          created) --!!
-- --
-- --         values (new.id, new.uuid, new.source_amount, new.destination_amount, new.source_shard_id, new.destination_shard_id,
-- --                 new.source_shard_multi_sig_address, new.source_shard_multi_sig_redeem_script,
-- --                 new.destination_shard_multi_sig_address, new.destination_shard_multi_sig_redeem_script,
-- --                 new.source_shard_destination_address, new.destination_shard_destination_address,
-- --                 new.client_pub_key, new.state,
-- --                 new.updated);  --!!
-- --     end if;
